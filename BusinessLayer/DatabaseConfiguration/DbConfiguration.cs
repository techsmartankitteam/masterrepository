﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataLayer.DatabaseConfiguration
{
   public static class DbConfiguration
    {
       private static string _connectionString { get; set; }
       public static String ConnectionString { get {
           return _connectionString;
       }
           set {
               _connectionString = value;
           }
       }

       public static void InitializeConnection(String connectionString)
       {

          ConnectionString=String.Format("metadata=res://*/EnventoryManager.csdl|res://*/EnventoryManager.ssdl|res://*/EnventoryManager.msl;provider=System.Data.SqlClient;provider connection string=\"{0}\"",connectionString)  ;
           
       }
       public static String GetConnection()
       { 
       return ConnectionString;
       }

    }
}

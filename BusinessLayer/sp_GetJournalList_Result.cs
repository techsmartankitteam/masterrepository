//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataLayer
{
    using System;
    
    public partial class sp_GetJournalList_Result
    {
        public long Company_Id { get; set; }
        public long Journal_VC_Id { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Particulars { get; set; }
        public Nullable<decimal> CR { get; set; }
        public Nullable<decimal> DR { get; set; }
    }
}

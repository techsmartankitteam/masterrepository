﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using ViewModel.Transactions;
using ViewModel.Users;

namespace DataLayer.Transactions
{
    public class SalesReturnDbLayer
    {
        INVENTORY_DBEntities _db;
        ViewModel.Users.LoginInfo _userLog;
        public SalesReturnDbLayer()
        {
            _db = new INVENTORY_DBEntities();
            _userLog = new LoginInfo
            {
                CompanyId = 1,
                ConnectionString = _db.Database.Connection.ConnectionString,
                FinancialId = 1,
                FinancialYear = "2015-2016",
                LoginId = 1,
                LoginName = "Ankit Singh",
                SecurityToken = Guid.NewGuid().ToString(),
                UserSession = Guid.NewGuid().ToString()
            };
           
        }
           [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
        public SalesReturnDbLayer(string ConnectionString)
        {
            _db = new INVENTORY_DBEntities();
            _db.Database.Connection.ConnectionString = ConnectionString;
            _userLog = new LoginInfo
            {
                CompanyId = 1,
                ConnectionString = _db.Database.Connection.ConnectionString,
                FinancialId = 1,
                FinancialYear = "2015-2016",
                LoginId = 1,
                LoginName = "Ankit Singh",
                SecurityToken = Guid.NewGuid().ToString(),
                UserSession = Guid.NewGuid().ToString()
            };


        }

        public SalesReturnDbLayer(ViewModel.Users.LoginInfo Log)
        {
            _db = new INVENTORY_DBEntities();
            _db.Database.Connection.ConnectionString=Log.ConnectionString;
            _userLog =Log;
        }
        public List<SalesReturnList> List(string search = "")
        {
            List<SalesReturnList> list = new List<SalesReturnList>();
            var ls = _db.Sales_Return.Where(x=>x.Is_Deleted==false && x.SalesRet_VC_No.Trim().StartsWith(search.Trim())).OrderByDescending(x => x.DATE).ToList();
            ls.ForEach(x => list.Add(new SalesReturnList
            {
                Id = x.Id,
                Return_Date = x.DATE.ToShortDateString(),
                VC_No = x.SalesRet_VC_No,
                Remarks = x.Remarks,
                Customer_Id = x.Customer_Id,
                  Amount=x.Total_Amount
            }));
            return list;
        }

      
        public bool Create(SalesReturnInfo ModelSales)
        {

            bool result = false;
            decimal Amount = 0;
            //using (TransactionScope ts = new TransactionScope())
            //{


            try
            {
                Amount = Convert.ToDecimal(ModelSales.ReturnDetails.Sum(x => x.Rate * x.Quantity));
                Sales_Return TblSalesReturn = new Sales_Return();
                TblSalesReturn.Sales_Id = ModelSales.Sales_Id;
                TblSalesReturn.Customer_Id = ModelSales.Customer_Id;
                TblSalesReturn.DATE = ModelSales.Return_Date;
                TblSalesReturn.Discount_Amount = ModelSales.Discount;
                TblSalesReturn.Fncl_Year_Id = Convert.ToInt32( _userLog.FinancialId);
                TblSalesReturn.Is_Active = true;
                TblSalesReturn.Is_Deleted = false;
                TblSalesReturn.Modified_By =_userLog.LoginId;
                TblSalesReturn.Modified_Date = DateTime.Now;
                TblSalesReturn.Created_By = _userLog.LoginId;
                TblSalesReturn.Created_Date = DateTime.Now;
                TblSalesReturn.SalesRet_VC_No = ModelSales.VC_No;
                TblSalesReturn.Total_Amount = Amount;
                TblSalesReturn.Remarks = ModelSales.Remarks;

                var res = Save(TblSalesReturn);
                if (res > 0)
                {
                    result = CreateSalesRetTra(ModelSales.ReturnDetails, TblSalesReturn.Id, "CREATE");
                }


            }
            catch
            {

                result = false;
            }
            //    ts.Complete();
            //    ts.Dispose();
            //}
            return result;
        }
        public bool Update(SalesReturnInfo ModelSales)
        {

            bool result = false;
            decimal Amount = 0;
            //using (TransactionScope ts = new TransactionScope())
            //{


            try
            {
                Amount = Convert.ToDecimal(ModelSales.ReturnDetails.Sum(x => x.Rate * x.Quantity));
                Sales_Return TblSalesReturn = _db.Sales_Return.Find(ModelSales.Id);
                TblSalesReturn.Sales_Id = ModelSales.Sales_Id;
                TblSalesReturn.Customer_Id = ModelSales.Customer_Id;
                TblSalesReturn.DATE = ModelSales.Return_Date;
                TblSalesReturn.Discount_Amount = ModelSales.Discount;
                TblSalesReturn.Modified_By = _userLog.LoginId;
                TblSalesReturn.Modified_Date = DateTime.Now;
                TblSalesReturn.SalesRet_VC_No = ModelSales.VC_No;
                TblSalesReturn.Total_Amount = Amount;
                TblSalesReturn.Remarks = ModelSales.Remarks;

                var res = Save(TblSalesReturn);
                if (res > 0)
                {
                    result = CreateSalesRetTra(ModelSales.ReturnDetails, TblSalesReturn.Id, "UPDATE");
                }


            }
            catch
            {

                result = false;
            }
            //    ts.Complete();
            //    ts.Dispose();
            //}
            return result;
        }
        public List<SalesReturnInfo_Tra> ReturnDetais(long Id)
        {
            List<SalesReturnInfo_Tra> lst = new List<SalesReturnInfo_Tra>();
            if (Id > 0)
            {

                var list = _db.Sales_Return_Tra.Where(x => x.SalesRet_Id == Id && x.Is_Deleted==false).ToList();
                var fnl = (from l in list
                           join i in _db.Stock_Item on l.Item_Id equals i.Id
                           join u in _db.UnitMasters on i.Unit_Id equals u.Id
                           select new
                           {
                               Id = l.Id,
                               Item_Id = l.Item_Id,
                               Item_Name = i.Name,
                               SalesRet_Id = l.SalesRet_Id,
                               Quantity = l.Qty,
                               Rate = l.Rate,
                               Sale_Serial_No = l.Sale_Serial_No,
                               Unit_Name = u.Name,
                               No_of_Decimal = u.No_of_Decimal ?? 0
                           }).ToList();

                fnl.ForEach(x => lst.Add(new SalesReturnInfo_Tra
                {
                    Id = x.Id,
                    Item_Id = x.Item_Id,
                    Item_Name = x.Item_Name,
                    Quantity = x.Quantity,
                    Rate = x.Rate,
                    Sale_Serial_No = x.Sale_Serial_No,
                    SalesRet_Id = x.SalesRet_Id,
                    Total_Amount = x.Rate * x.Quantity,
                    No_of_Decimal = x.No_of_Decimal,
                    Unit_Name = x.Unit_Name,

                }));
            }
            return lst;
        }
        public SalesReturnInfo Find(long Id)
        {
            SalesReturnInfo modelsalesRet = new SalesReturnInfo();

            try
            {
                Sales_Return tblSalesRet = new Sales_Return();
                tblSalesRet = Id > 0 ? _db.Sales_Return.Find(Id) : Id < 0 ? _db.Sales_Return.OrderByDescending(x => x.DATE).FirstOrDefault(x=>x.Is_Deleted==false) : null;
                if (tblSalesRet != null)
                {
                    modelsalesRet.Id = tblSalesRet.Id;
                    modelsalesRet.Is_RejectionNote = false;
                    modelsalesRet.Discount = tblSalesRet.Discount_Amount ?? 0.0m;
                    modelsalesRet.Amount = tblSalesRet.Total_Amount;
                    modelsalesRet.Customer_Id = tblSalesRet.Customer_Id;
                    modelsalesRet.Remarks = tblSalesRet.Remarks;
                    modelsalesRet.Return_Date = tblSalesRet.DATE;
                    modelsalesRet.Sales_Id = tblSalesRet.Sales_Id;
                    modelsalesRet.VC_No = tblSalesRet.SalesRet_VC_No;
                    if (modelsalesRet.Customer_Id > 0)
                    {
                        Ledger_Master lgm = _db.Ledger_Master.Find(tblSalesRet.Customer_Id);
                        if (lgm != null)
                        {
                            modelsalesRet.Customer_Name = lgm.Name;
                            modelsalesRet.Contact_No = lgm.Contact_No;
                            modelsalesRet.Email = lgm.Email_Id;
                        }
                    }
                }

            }
            catch
            {

                return null;
            }
            return modelsalesRet;
        }
        public String GEN_VC_No()
        {
            string VC_No = "";

            int countRows = _db.Sales_Return.Count();
            if (countRows != 0)
                VC_No = _db.Sales_Return.OrderBy(x => x.Id).Skip(countRows - 1).FirstOrDefault().SalesRet_VC_No;
            if (!string.IsNullOrWhiteSpace(VC_No))
            {
                VC_No = Regex.Replace(VC_No, @"\d+(?=\D*$)",
                   m => (Convert.ToInt64(m.Value) + 1).ToString().PadLeft(5, '0'));


            }
            else
            {
                VC_No = "1".PadLeft(5, '0');
            }


            return VC_No;
        }
        public bool CheckSerialNo(string SerialNo, int Sales_Id, int Item_Id)
        {
            bool result = false;
            try
            {
                result = _db.Sales_Master_Tra.Any(x => x.Sale_Serial_No == SerialNo && x.Sales_Id == Sales_Id && x.Item_Id == Item_Id && x.Is_Deleted == false);
            }
            catch
            {

                result = false;
            }
            return result;
        }
        public bool Delete(long Id)
        {
            bool result = false;
            try
            {
                if (Id > 0)
                {
                    Sales_Return tblSaleRet = _db.Sales_Return.Find(Id);
                    if (tblSaleRet != null)
                    {
                        tblSaleRet.Is_Deleted = true;
                        tblSaleRet.Modified_By = _userLog.LoginId;
                        tblSaleRet.Modified_Date = DateTime.Now;
                        _db.Sales_Return_Tra.Where(x => x.SalesRet_Id == tblSaleRet.Id).ToList().ForEach(x => x.Is_Deleted = true);
                        _db.SaveChanges();
                        result = true;
                    }
                }
            }
            catch
            {

                result = false;
            }
            return result;
        }
        private long Save(Sales_Return TblSalesReturn)
        {
            bool result = false;
            try
            {

                bool res = _db.Sales_Return.Any(m => (0 == TblSalesReturn.Id || m.Id != TblSalesReturn.Id) && (m.SalesRet_VC_No.ToUpper().Trim().Equals(TblSalesReturn.SalesRet_VC_No.ToUpper().Trim())));
                if (result == false)
                {
                    if (TblSalesReturn.Id == 0)
                    {
                        TblSalesReturn.Id = _db.Sales_Return.Count() > 0 ? _db.Sales_Return.Max(x => x.Id) + 1 : 1;
                        _db.Sales_Return.Add(TblSalesReturn);
                    }
                    _db.SaveChanges();
                    result = true;

                }
            }
            catch
            {

                return 0;
            }

            return result == true ? TblSalesReturn.Id : 0;
        }


        public bool CreateSalesRetTra(List<SalesReturnInfo_Tra> ModelSalesRetTra, long SalesRet_Id, string Tag)
        {
            bool result = false;
            try
            {
                // List<Sales_Return_Tra> TblPurMasterTra = new List<Sales_Return_Tra>();
                Sales_Return_Tra tbltra;

                if (ModelSalesRetTra != null)
                {

                    System.Data.DataTable tbl = new System.Data.DataTable("Product");
                    System.Data.DataColumn[] columns = new System.Data.DataColumn[6];
                    columns[0] = new System.Data.DataColumn("Id", typeof(long));
                    columns[1] = new System.Data.DataColumn("SalesRet_Id", typeof(long));
                    columns[2] = new System.Data.DataColumn("Item_Id", typeof(long));
                    columns[3] = new System.Data.DataColumn("Qty", typeof(decimal));
                    columns[4] = new System.Data.DataColumn("Rate", typeof(decimal));
                    columns[5] = new System.Data.DataColumn("Sale_Serial_No", typeof(string));

                    tbl.Columns.AddRange(columns);

                    foreach (var item in ModelSalesRetTra)
                    {
                        var row = tbl.NewRow();
                        row["Id"] = item.Id;
                        row["SalesRet_Id"] = SalesRet_Id;
                        row["Item_Id"] = item.Item_Id;
                        row["Qty"] = item.Quantity ?? 0;
                        row["Rate"] = item.Rate ?? 0.00m;
                        row["Sale_Serial_No"] = item.Sale_Serial_No;
                        tbl.Rows.Add(row);
                    }




                    DatabaseConfiguration.DbConfiguration.InitializeConnection(_userLog.ConnectionString);

                    var builder = new EntityConnectionStringBuilder(DatabaseConfiguration.DbConfiguration.ConnectionString);
                    var regularConnectionString = builder.ProviderConnectionString;
                    using (SqlConnection connection = new SqlConnection(regularConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("", connection);

                        command.CommandText = "Pr_SalesReturnTra";
                        command.Parameters.Clear();
                        command.CommandType = CommandType.StoredProcedure;
                        // command.Parameters.AddWithValue("@someIdForParameter", someId);
                        command.Parameters.AddWithValue("@datatable", tbl).SqlDbType = SqlDbType.Structured;
                        command.Parameters.AddWithValue("@SalesRet_Id", SalesRet_Id).SqlDbType = SqlDbType.BigInt;
                        command.Parameters.AddWithValue("@Tag", Tag).SqlDbType = SqlDbType.NVarChar;
                        command.ExecuteNonQuery();

                        connection.Close();

                        //  bool result = false;

                        return result;
                    };
                    result = true;

                }
            }
            catch
            {

                result = false;
            }
            return result;
        }

        public List<ViewModel.Common.DDLBind> GetBillList(long Customer_Id)
        {
            return _db.Sales_Master.Where(x => x.Customer_Id == Customer_Id && x.Is_Deleted == false).Select(x => new ViewModel.Common.DDLBind
            {
                Id = x.Id,
                Name = x.Sales_VC_No
            }).ToList();
        }

        /// <summary>
        /// Added by anurag
        /// </summary>
        /// <param name="Purchase_Id"></param>
        /// <returns></returns>
        public List<ViewModel.Common.DDLBind> GetStockItemForSupplier(long Sales_Id)
        {
            return _db.Sales_Master_Tra.Where(x => x.Sales_Id == Sales_Id && x.Is_Deleted == false).Join(_db.Stock_Item, x => x.Item_Id, y => y.Id, (x, y) => y).GroupBy(z => new { z.Id, z.Name }).Select(z => new ViewModel.Common.DDLBind
            {
                Id = z.Key.Id,
                Name = z.Key.Name
            }).OrderBy(z => z.Name).ToList();
        }

        /// <summary>
        /// Added by anurag
        /// </summary>
        /// <param name="SerialNo"></param>
        /// <param name="Item_Id"></param>
        /// <returns></returns>
        public bool CheckIsReturnedSerialNo(string SerialNo, int Item_Id, int Sales_Id)
        {
            bool result = false;
            try
            {
                result = _db.Sales_Master_Tra.Any(x => x.Sale_Serial_No == SerialNo && x.Sales_Id == Sales_Id && x.Item_Id == Item_Id && x.Is_Deleted == false && x.Is_Return == true);
            }
            catch
            {

                result = false;
            }
            return result;
        }

        public string CheckValidQty(int Customer_Id, int Sales_Id, int Item_Id)
        {
            bool result = false;
            string result1 = "";
            try
            {
                //result = _db.Purchase_Master_Tra.Any(x => x.Pur_Id == Purchase_Id && x.Item_Id == Item_Id && x.Is_Deleted == false && x.Is_Return == true);

                int BookedQty = _db.Sales_Master_Tra.Where(x => x.Sales_Id == Sales_Id && x.Item_Id == Item_Id && x.Is_Deleted == false && x.Is_Return == false).Count();
                int ReturnedQty = _db.Sales_Return.Join(_db.Sales_Return_Tra, x => x.Id, y => y.SalesRet_Id, (x, y) => new { x, y }).Where(t => t.x.Sales_Id == Sales_Id && t.x.Is_Deleted == false && t.y.Item_Id == Item_Id).Count();

                int RestQty = BookedQty - ReturnedQty;
                //if (RestQty > 0)
                //{
                //    result = false;
                //}
                //else if (RestQty == 0 || RestQty < 0)
                //{
                //    result = true;
                //}
                result1 = BookedQty + "~" + RestQty;
            }
            catch
            {

                result = false;
            }
            return result1;
        }

        public string GetItemRate(int Item_Id, int Sales_Id)
        {
            string result = "";
            try
            {
                var Rate = _db.Sales_Master_Tra.FirstOrDefault(x => x.Item_Id == Item_Id && x.Sales_Id == Sales_Id && x.Is_Deleted == false && x.Is_Return == false);
                if (Rate != null)
                {
                    result = Convert.ToString(Rate.Rate) + "~" + Convert.ToString(Rate.Id);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }


}

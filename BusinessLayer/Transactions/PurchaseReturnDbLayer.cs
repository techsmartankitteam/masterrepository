﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using ViewModel.Transactions;

using System.Data.Entity.Core.Objects;
using ViewModel.Users;
namespace DataLayer.Transactions
{
    public class PurchaseReturnDbLayer
    {
        INVENTORY_DBEntities _db;
        ViewModel.Users.LoginInfo _userLog;
        public PurchaseReturnDbLayer()
        {
            _db = new INVENTORY_DBEntities();
            _userLog = new LoginInfo
            {
                CompanyId = 1,
                ConnectionString = _db.Database.Connection.ConnectionString,
                FinancialId = 1,
                FinancialYear = "2015-2016",
                LoginId = 1,
                LoginName = "Ankit Singh",
                SecurityToken = Guid.NewGuid().ToString(),
                UserSession = Guid.NewGuid().ToString()
            };
        }
        [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
        public PurchaseReturnDbLayer(string ConnectionString)
        {
            _db = new INVENTORY_DBEntities();
            _db.Database.Connection.ConnectionString = ConnectionString;
            _userLog = new LoginInfo
            {
                CompanyId = 1,
                ConnectionString = _db.Database.Connection.ConnectionString,
                FinancialId = 1,
                FinancialYear = "2015-2016",
                LoginId = 1,
                LoginName = "Ankit Singh",
                SecurityToken = Guid.NewGuid().ToString(),
                UserSession = Guid.NewGuid().ToString()
            };

        }
        public PurchaseReturnDbLayer(ViewModel.Users.LoginInfo Log)
        {
            _db = new INVENTORY_DBEntities();
            _db.Database.Connection.ConnectionString = Log.ConnectionString;
            this._userLog = Log;

        }
        public List<PurchaseReturnList> List(string search = "")
        {
            List<PurchaseReturnList> list = new List<PurchaseReturnList>();

            var lst = _db.Purchase_Return.Where(x=>x.Is_Deleted==false && x.PurRet_VC_No.Trim().StartsWith(search.Trim()) ).OrderByDescending(x => x.DATE).ToList();
            lst.ForEach(x => list.Add(new PurchaseReturnList
            {
                Id = x.Id,
                Return_Date = x.DATE.ToShortDateString(),
                Remarks = x.Remarks,
                VC_No = x.PurRet_VC_No,
                Amount=x.Total_Amount
            }));
            return list;
        }
        public bool Create(PurchaseReturnInfo ModelPurRet)
        {
            bool result = false;
            decimal Amount = 0;
            //using (TransactionScope ts = new TransactionScope())
            //{

            try
            {
                // var g = TotalAmount(ModelPurRet);
                Amount = Convert.ToDecimal(ModelPurRet.ReturnDetails.Sum(x => x.Rate * x.Quantity));
                if (ModelPurRet != null && ModelPurRet.Id == 0)
                {
                    Purchase_Return tblPurRet = new Purchase_Return();
                    tblPurRet.DATE = ModelPurRet.Return_Date;
                    tblPurRet.Discount_Amount = ModelPurRet.Discount;
                    tblPurRet.Supplier_Id = ModelPurRet.Supplier_Id;
                    tblPurRet.Created_Date = DateTime.Now;
                    tblPurRet.Created_By = _userLog.LoginId;
                    tblPurRet.Modified_By = _userLog.LoginId;
                    tblPurRet.Modified_Date = DateTime.Now;
                    tblPurRet.Purchase_Id = ModelPurRet.Purchase_Id;
                    tblPurRet.Remarks = ModelPurRet.Remarks;
                    tblPurRet.PurRet_VC_No = ModelPurRet.VC_No;
                    tblPurRet.Total_Amount = Amount;


                    var r = Save(tblPurRet);
                    if (r > 0)
                    {
                        CreatePurchaseRetTra(ModelPurRet.ReturnDetails, tblPurRet.Id, "CREATE");
                    }

                }
            }
            catch
            {

                result = false;
            }
            //    ts.Complete();
            //    ts.Dispose();
            //}
            return result;
        }
        public PurchaseReturnInfo Find(long Id)
        {
            PurchaseReturnInfo modelPurRet = new PurchaseReturnInfo();
            Purchase_Return tblPurRet = new Purchase_Return();
            tblPurRet = Id > 0 ? _db.Purchase_Return.Find(Id) : Id < 0 ? _db.Purchase_Return.OrderByDescending(x => x.DATE).FirstOrDefault(x=>x.Is_Deleted==false) : null;
            if (tblPurRet != null)
            {
                //Purchase_Return tblPurRet = _db.Purchase_Return.Find(Id);

                modelPurRet.Id = tblPurRet.Id;
                modelPurRet.Purchase_Id = tblPurRet.Purchase_Id;
                modelPurRet.Remarks = tblPurRet.Remarks;
                modelPurRet.Return_Date = tblPurRet.DATE;
                modelPurRet.Supplier_Id = tblPurRet.Supplier_Id;
                modelPurRet.VC_No = tblPurRet.PurRet_VC_No;
                modelPurRet.Amount = tblPurRet.Total_Amount;
                modelPurRet.Discount = tblPurRet.Discount_Amount ?? 0.00m;
                if (tblPurRet.Supplier_Id > 0)
                {
                    Ledger_Master lgm = _db.Ledger_Master.Find(tblPurRet.Supplier_Id);
                    if (lgm != null)
                    {
                        modelPurRet.Contact_No = lgm.Contact_No;
                        modelPurRet.Email = lgm.Email_Id;
                        modelPurRet.Supplier_Name = lgm.Name;
                    }
                }

            }
            return modelPurRet;
        }
        public bool Update(PurchaseReturnInfo ModelPurRet)
        {
            bool result = false;
            decimal Amount = 0;
            //using (TransactionScope ts = new TransactionScope())
            //{

            try
            {
                Amount = Convert.ToDecimal(ModelPurRet.ReturnDetails.Sum(x => x.Rate * x.Quantity));

                if (ModelPurRet != null && ModelPurRet.Id != 0)
                {
                    Purchase_Return tblPurRet = _db.Purchase_Return.Find(ModelPurRet.Id);
                    tblPurRet.DATE = ModelPurRet.Return_Date;
                    tblPurRet.Discount_Amount = ModelPurRet.Discount;
                    tblPurRet.Supplier_Id = ModelPurRet.Supplier_Id;
                    tblPurRet.Modified_By = _userLog.LoginId;
                    tblPurRet.Modified_Date = DateTime.Now;
                    tblPurRet.Purchase_Id = ModelPurRet.Purchase_Id;
                    tblPurRet.Remarks = ModelPurRet.Remarks;
                    tblPurRet.Total_Amount = Amount;

                    Save(tblPurRet);
                    var r = Save(tblPurRet);
                    if (r > 0)
                    {
                        CreatePurchaseRetTra(ModelPurRet.ReturnDetails, tblPurRet.Id, "UPDATE");
                    }
                }
            }
            catch
            {

                result = false;
            }
            //    ts.Complete();
            //    ts.Dispose();
            //}
            return result;
        }
        public List<PurchaseReturnInfo_Tra> ReturnDetails(long Id)
        {
            List<PurchaseReturnInfo_Tra> list = new List<PurchaseReturnInfo_Tra>();
            var lst = _db.Purchase_Return_Tra.Where(x => x.PurRet_Id == Id && x.Is_Deleted==false).ToList();
            var fnl = (from l in lst
                       join i in _db.Stock_Item on l.Item_Id equals i.Id
                       join u in _db.UnitMasters on i.Unit_Id equals u.Id
                       select new
                       {
                           Id = l.Id,
                           Item_Id = l.Item_Id,
                           Item_Name = i.Name,
                           PurRet_Id = l.PurRet_Id,
                           Quantity = l.Qty,
                           Rate = l.Rate,
                           Sale_Serial_No = l.Sale_Serial_No,
                           Unit_Name = u.Name,
                           No_of_Decimal = u.No_of_Decimal ?? 0
                       }).ToList();
            fnl.ForEach(x => list.Add(new PurchaseReturnInfo_Tra
            {
                Id = x.Id,
                Item_Id = x.Item_Id,
                Item_Name = x.Item_Name,
                PurRet_Id = x.PurRet_Id,
                Quantity = x.Quantity ?? 0.00m,
                Rate = x.Rate ?? 0.00m,
                Amount = x.Quantity * x.Rate,
                No_of_Decimal = x.No_of_Decimal,
                Unit_Name = x.Unit_Name,
                Sale_Serial_No = x.Sale_Serial_No
            }));
            return list;
        }
        public bool Delete(long Id)
        {
            bool result = false;
            try
            {
                if (Id > 0)
                {
                    Purchase_Return tblpurRet = _db.Purchase_Return.Find(Id);
                    if (tblpurRet != null)
                    {
                        tblpurRet.Is_Deleted = true;
                        tblpurRet.Modified_By = _userLog.LoginId;
                        tblpurRet.Modified_Date = DateTime.Now;
                        _db.SaveChanges();

                    }
                    result = true;
                }
            }
            catch
            {

                result = false;
            }
            return result;
        }
        public bool CheckSerialNo(string SerialNo, int Purchase_Id)
        {
            bool result = false;
            try
            {
                result = _db.Purchase_Master_Tra.Any(x => x.Purchase_Serial_No == SerialNo && x.Is_Deleted == false && x.Pur_Id == Purchase_Id);
            }
            catch
            {

                result = false;
            }
            return result;
        }


        /// <summary>
        /// Added by anurag
        /// </summary>
        /// <param name="SerialNo"></param>
        /// <param name="Item_Id"></param>
        /// <returns></returns>
        public bool CheckIsReturnedSerialNo(string SerialNo, int Item_Id, int Purchase_Id)
        {
            bool result = false;
            try
            {
                result = _db.Purchase_Master_Tra.Any(x => x.Purchase_Serial_No == SerialNo && x.Item_Id == Item_Id && x.Pur_Id == Purchase_Id && x.Is_Deleted == false && x.Is_Return == true);
            }
            catch
            {

                result = false;
            }
            return result;
        }

        public string CheckValidQty(int Supplier_Id, int Purchase_Id, int Item_Id)
        {
            bool result = false;
            string result1 = "";
            try
            {
                //result = _db.Purchase_Master_Tra.Any(x => x.Pur_Id == Purchase_Id && x.Item_Id == Item_Id && x.Is_Deleted == false && x.Is_Return == true);

                decimal BookedQty = _db.Purchase_Master_Tra.Where(x => x.Pur_Id == Purchase_Id && x.Item_Id == Item_Id && x.Is_Deleted == false && x.Is_Return == false).Sum(z => z.Qty) ?? 0.0m;
                decimal ReturnedQty = _db.Purchase_Return.Join(_db.Purchase_Return_Tra, x => x.Id, y => y.PurRet_Id, (x, y) => new { x, y }).Where(t => t.x.Purchase_Id == Purchase_Id && t.x.Is_Deleted == false && t.y.Item_Id == Item_Id).Sum(z => z.y.Qty) ?? 0.0m;

                decimal RestQty = BookedQty - ReturnedQty;
                //if (RestQty > 0)
                //{
                //    result = false;
                //}
                //else if (RestQty == 0 || RestQty < 0)
                //{
                //    result = true;
                //}
                result1 = BookedQty + "~" + RestQty;
            }
            catch
            {

                result = false;
            }
            return result1;
        }
        public String GEN_VC_No()
        {
            string VC_No = "";

            int countRows = _db.Purchase_Return.Count();
            if (countRows != 0)
                VC_No = _db.Purchase_Return.OrderBy(x => x.Id).Skip(countRows - 1).FirstOrDefault().PurRet_VC_No;
            if (!string.IsNullOrWhiteSpace(VC_No))
            {
                VC_No = Regex.Replace(VC_No, @"\d+(?=\D*$)",
                   m => (Convert.ToInt64(m.Value) + 1).ToString().PadLeft(5, '0'));


            }
            else
            {
                VC_No = "1".PadLeft(5, '0');
            }


            return VC_No;
        }
        public bool CreatePurchaseRetTra(List<PurchaseReturnInfo_Tra> ModelPurRetTra, long PurRet_Id, string Tag)
        {
            bool result = false;
            try
            {
                // List<Sales_Return_Tra> TblPurMasterTra = new List<Sales_Return_Tra>();
                Sales_Return_Tra tbltra;

                if (ModelPurRetTra != null)
                {

                    System.Data.DataTable tbl = new System.Data.DataTable("Product");
                    System.Data.DataColumn[] columns = new System.Data.DataColumn[6];
                    columns[0] = new System.Data.DataColumn("Id", typeof(long));
                    columns[1] = new System.Data.DataColumn("SalesRet_Id", typeof(long));
                    columns[2] = new System.Data.DataColumn("Item_Id", typeof(long));
                    columns[3] = new System.Data.DataColumn("Qty", typeof(decimal));
                    columns[4] = new System.Data.DataColumn("Rate", typeof(decimal));
                    columns[5] = new System.Data.DataColumn("Sale_Serial_No", typeof(string));

                    tbl.Columns.AddRange(columns);

                    foreach (var item in ModelPurRetTra)
                    {
                        var row = tbl.NewRow();
                        row["Id"] = item.Id;
                        row["SalesRet_Id"] = PurRet_Id;
                        row["Item_Id"] = item.Item_Id;
                        row["Qty"] = item.Quantity;
                        row["Rate"] = item.Rate ?? 0.00m;
                        row["Sale_Serial_No"] = item.Sale_Serial_No;
                        tbl.Rows.Add(row);
                    }


                        DatabaseConfiguration.DbConfiguration.InitializeConnection(_userLog.ConnectionString);

                    var builder = new EntityConnectionStringBuilder(DatabaseConfiguration.DbConfiguration.ConnectionString);
                    var regularConnectionString = builder.ProviderConnectionString;
                    using (SqlConnection connection = new SqlConnection(regularConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("", connection);

                        command.CommandText = "Pr_PurchaseReturnTra";
                        command.Parameters.Clear();
                        command.CommandType = CommandType.StoredProcedure;
                        // command.Parameters.AddWithValue("@someIdForParameter", someId);
                        command.Parameters.AddWithValue("@datatable", tbl).SqlDbType = SqlDbType.Structured;
                        command.Parameters.AddWithValue("@PurRet_Id", PurRet_Id).SqlDbType = SqlDbType.BigInt;
                        command.Parameters.AddWithValue("@Tag", Tag).SqlDbType = SqlDbType.NVarChar;
                        command.ExecuteNonQuery();

                        connection.Close();

                        //  bool result = false;

                        return result;
                    };
                    result = true;

                }
            }
            catch
            {

                result = false;
            }
            return result;
        }
        private long Save(Purchase_Return TblPurRet)
        {
            bool result = false;
            try
            {
                bool res = _db.Purchase_Return.Any(m => (0 == TblPurRet.Id || m.Id != TblPurRet.Id) && (m.PurRet_VC_No.ToUpper().Trim().Equals(TblPurRet.PurRet_VC_No.ToUpper().Trim())));
                if (result == false)
                {
                    if (TblPurRet.Id == 0)
                    {
                        TblPurRet.Id = _db.Purchase_Return.Count() > 0 ? _db.Purchase_Return.Max(x => x.Id) + 1 : 1;
                        _db.Purchase_Return.Add(TblPurRet);
                    }
                    _db.SaveChanges();
                    result = true;

                }
            }
            catch
            {

                return 0;
            }

            return result == true ? TblPurRet.Id : 0;
        }

        public List<ViewModel.Common.DDLBind> GetBillList(long Supplier_Id)
        {
            return _db.Purchase_Master.Where(x => x.Supplier_Id == Supplier_Id && x.Is_Deleted == false).Select(x => new ViewModel.Common.DDLBind
            {
                Id = x.Id,
                Name = x.Pur_VC_No
            }).ToList();
        }


        /// <summary>
        /// Added by anurag
        /// </summary>
        /// <param name="Purchase_Id"></param>
        /// <returns></returns>
        public List<ViewModel.Common.DDLBind> GetStockItemForSupplier(long Purchase_Id)
        {
            return _db.Purchase_Master_Tra.Where(x => x.Pur_Id == Purchase_Id && x.Is_Deleted == false).Join(_db.Stock_Item, x => x.Item_Id, y => y.Id, (x, y) => y).GroupBy(z => new { z.Id, z.Name }).Select(z => new ViewModel.Common.DDLBind
            {
                Id = z.Key.Id,
                Name = z.Key.Name
            }).OrderBy(z => z.Name).ToList();
        }


        /// <summary>
        /// Added by anurag dated-29-06-2016
        /// </summary>
        /// <param name="SerialNo"></param>
        /// <returns></returns>
        public string GetItemRate(int Item_Id, int Purchase_Id)
        {
            string result = "";
            try
            {
                var Rate = _db.Purchase_Master_Tra.FirstOrDefault(x => x.Item_Id == Item_Id && x.Pur_Id == Purchase_Id && x.Is_Deleted == false && x.Is_Return == false);
                if (Rate != null)
                {
                    result = Convert.ToString(Rate.Rate) + "~" + Convert.ToString(Rate.Id);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using ViewModel.Ledger;
using ViewModel.Users;

namespace DataLayer.Transactions
{
    public class SalesDbLayer
    {
        INVENTORY_DBEntities _db;
        ViewModel.Users.LoginInfo _userLog;
        
        public SalesDbLayer()
        {
            _db = new INVENTORY_DBEntities();
            _userLog = new LoginInfo
            {
                CompanyId = 1,
                ConnectionString = _db.Database.Connection.ConnectionString,
                FinancialId = 1,
                FinancialYear = "2015-2016",
                LoginId = 1,
                LoginName = "Ankit Singh",
                SecurityToken = Guid.NewGuid().ToString(),
                UserSession = Guid.NewGuid().ToString()
            };
        }
          [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
        public SalesDbLayer(String ConnectionString)
        {
            _db = new INVENTORY_DBEntities();
            _db.Database.Connection.ConnectionString = ConnectionString;
            _userLog = new LoginInfo
            {
                CompanyId = 1,
                ConnectionString = _db.Database.Connection.ConnectionString,
                FinancialId = 1,
                FinancialYear = "2015-2016",
                LoginId = 1,
                LoginName = "Ankit Singh",
                SecurityToken = Guid.NewGuid().ToString(),
                UserSession = Guid.NewGuid().ToString()
            };
        }
        public SalesDbLayer(ViewModel.Users.LoginInfo Log)
        {
                        _db = new INVENTORY_DBEntities();
            _db.Database.Connection.ConnectionString = Log.ConnectionString;
            _userLog =Log;
        }

        public List<PurchaseList> List(String Terms="")
        {

            var lst = _db.Pr_SalesList(_userLog.CompanyId).Where(m=>m.Chalan_No.Trim().StartsWith(Terms.Trim())).OrderByDescending(x => x.DATE).Select(x => new PurchaseList
            {
                Id = x.Id,
                Name = x.Name,
                Alias_Name = x.Alias_Name,
                Address = x.Address,
                Challan_No = x.Chalan_No,
                Email_Id = x.Email_Id,
                Purchase_Date = x.DATE.ToShortDateString(),
                Amount = x.Total_Amount,
                Code = x.Code,
                Contact_No = x.Contact_No,
                Discount = x.Discount_Amount,
                Is_Challan_Gen = x.Is_Bill_Gen
            }).ToList();
            return lst;
        }
        public List<Challan_Details> ViewDetails(long Id)
        {
            List<Challan_Details> lstchldetal = new List<Challan_Details>();
            if (Id != 0)
            {
                //var tblPurmaster = _db.Sales_Master.Find(Id);
                //var lst = _db.Pr_Challan_Details(Id, "SALES").ToList();


                //lst.ForEach(x => lstchldetal.Add(new Challan_Details
                //{
                //    Id = x.Id,
                //    Supplier_Name = x.Supplier_Name,
                //    Item_Name = x.Item_Name,
                //    Challan_Number = x.Challan_No,
                //    Purchase_Date = x.Purchase_Date.ToShortDateString(),
                //    Qty = x.Qty,
                //    Rate = x.Rate,
                //    Remarks = "",

                //    Total_Amount = x.Qty * x.Rate,
                //    Grand_Total = x.Total_Amount,
                //    Unit_Name = x.Per
                //}));
            }
            return lstchldetal;
        }
        public List<ViewModel.Ledger.Purchase_Tra> ItemDetails(long salesId)
        {
            List<ViewModel.Ledger.Purchase_Tra> modellist = new List<Purchase_Tra>();
            

            (from tra in _db.Sales_Master_Tra
             join St in _db.Stock_Item on tra.Item_Id equals St.Id into i
             from item in i.DefaultIfEmpty()
             where tra.Sales_Id == salesId
             select new
             {
                 tra.Id,
                 
                 tra.Item_Id,
                 tra.Qty,
                 tra.Rate,
                 tra.Tax_Id,
                 item.Name,
                 tra.Sale_Serial_No
             }).AsParallel().AsEnumerable().ToList().ForEach((x) => modellist.Add(new Purchase_Tra
             {
                 Id = x.Id,
                 
                 GoDownId = 0,
                 Item_Id = x.Item_Id,
                 Qty = x.Qty ?? 0.0m,
                 Rate = x.Rate ?? 0.0m,
                 
                 Total_Amount = x.Qty * x.Rate ?? 0.0m,
                 Unit_Id = 1,
                 Unit_Name = UnitNameByItemId(x.Item_Id),
                 ItemName = x.Name,
                 SerialNo = x.Sale_Serial_No
             }));

            

            return modellist;
        }
        public SalesOrderItem GetSalesOrdertemList(long Delivery_Note_Id = 0)
        {
            SalesOrderItem orderItem = new SalesOrderItem();
            Delivery_Note tblReceipt = _db.Delivery_Note.FirstOrDefault(x => x.Is_Sales == false && x.Is_Deleted == false && x.Id == Delivery_Note_Id);
            if (tblReceipt != null)
            {
                var DeliverdDetails = _db.Delivery_Note_Tra.Where(x => x.Delivery_Note_Id == Delivery_Note_Id).ToList();
                var fnllist = (from rl in DeliverdDetails
                               join st in _db.Stock_Item on rl.Item_Id equals st.Id
                               join ut in _db.UnitMasters on st.Unit_Id equals ut.Id
                               select new { Item_Id = rl.Item_Id, Item_Name = st.Name, 
                                   No_of_Decimal = ut.No_of_Decimal, 
                                   Unit_Id = st.Unit_Id, 
                                   Unit_Name = ut.Name, 
                                   Receipt_Note_Id = rl.Delivery_Note_Id, 
                                   Quantity = rl.Delivered_Quantity,
                                   Rate = rl.Rate,
                                   ProductCode = rl.Sales_Serial_No }).ToList();
                fnllist.ForEach(x => orderItem.listSalesOrderItem.Add(new SalesOrderItem
                {
                    ItemId = x.Item_Id,
                    ItemName = x.Item_Name,
                    orderId = Delivery_Note_Id,
                    Quantity = x.Quantity ?? 0,
                    Rate = x.Rate ?? 0,
                    ProductCode = x.ProductCode,
                    SerialNo = x.ProductCode,
                    UnitId = x.Unit_Id,
                    UnitName = x.Unit_Name,
                    No_of_Decimal = x.No_of_Decimal ?? 0,

                }));
                orderItem.CustomerId = tblReceipt.Customer_Id;
                orderItem.TotalAMount = tblReceipt.Amount ?? 0;
            }
            return orderItem;
        }
        public List<ViewModel.Ledger.Sales_Tra> ViewItemDetails(long salesId)
        {
            List<Sales_Tra> modelList = new List<Sales_Tra>();
            var fnllst =
     (from tra in _db.Sales_Master_Tra.Where(x => x.Sales_Id == salesId && x.Is_Deleted==false )
      join St in _db.Stock_Item on tra.Item_Id equals St.Id
      join ut in _db.UnitMasters on St.Unit_Id equals ut.Id
      select new
      {
          Id = tra.Id,
          Item_Id = tra.Item_Id,
          Qty = tra.Qty,
          Rate = tra.Rate,
          SerialNo = tra.Sale_Serial_No,
          Item_Name = St.Name,
          Unit_Name = ut.Name,
          Unit_Id = St.Unit_Id,
          UnitPlace = ut.No_of_Decimal
      }).ToList();

            fnllst.ForEach(x => modelList.Add(new Sales_Tra
            {
                Id = x.Id,
                Item_Id = x.Item_Id,
                Unit_Id = x.Unit_Id,
                Unit_Name = x.Unit_Name,
                UnitPlace = x.UnitPlace ?? 0,
                ItemName = x.Item_Name,
                Qty = x.Qty ?? 0.00m,
                Rate = x.Rate ?? 0.00m,
                Amount = x.Qty * x.Rate ?? 0.00m
            }));
            
            return modelList;
        }
        private string UnitNameByItemId(long ItemId)
        {
            string name = "";
            if (ItemId != 0)
            {
                var item = _db.Stock_Item.SingleOrDefault(x => x.Id == ItemId);
                name = _db.UnitMasters.SingleOrDefault(x => x.Id == item.Unit_Id).Name;
            }
            return name;
        }
        public bool Create(ViewModel.Ledger.SalesMaster ModelSalesMaster)
        {
            bool result = false;

            //using (TransactionScope ts = new TransactionScope()) 
            //{  
            try
            {
                
                Sales_Master TblSalesMaster = new Sales_Master();
                
                TblSalesMaster.Fncl_Year_Id = ModelSalesMaster.Finance_Id != 0 ? ModelSalesMaster.Finance_Id : _userLog.CompanyId;
                TblSalesMaster.Company_Id = _userLog.CompanyId;
                //  TblSalesMaster.Branch_Id = 1;
                TblSalesMaster.Sales_VC_No = ModelSalesMaster.Challan_Number;
                TblSalesMaster.Is_Active = true;
                TblSalesMaster.Is_Deleted = false;
                TblSalesMaster.Modified_By = _userLog.LoginId;
                TblSalesMaster.Refered_Id = ModelSalesMaster.Refered_Id != 0 ? ModelSalesMaster.Refered_Id : 0;
                TblSalesMaster.Refered_Type = ModelSalesMaster.Refered_Type;
                TblSalesMaster.Is_Refered = ModelSalesMaster.Is_Refered;
                TblSalesMaster.Created_By =_userLog.LoginId;
                TblSalesMaster.Created_Date = DateTime.Now;
                
                TblSalesMaster.Modified_Date = DateTime.Now;
                TblSalesMaster.DATE = ModelSalesMaster.Purchase_Date;
                TblSalesMaster.Remarks = ModelSalesMaster.Remarks;
                TblSalesMaster.Customer_Id = ModelSalesMaster.Customer_Id;
                TblSalesMaster.Total_Amount = ModelSalesMaster.Grand_Total;
                TblSalesMaster.Discount_Amount = ModelSalesMaster.Discount != 0 ? ModelSalesMaster.Discount : 0.0m;
                TblSalesMaster.Tax_Amount = ModelSalesMaster.Tax_Amount;
                
                var rs = Save(TblSalesMaster);
                if (rs != 0)
                {
                    //result = CreatePurchaseTra(ModelPurMaster.sales_tra.listpurchase_tra.ToList(), TblSalesMaster.Sales_VC_No, 0);
                    // result = Stock_MasterUpdate(ModelPurMaster.sales_tra.listpurchase_tra.Where(x => x.Status == true).ToList());
                    result = UpdateSales_TraAndStock_master(ModelSalesMaster.sales_tra.listpurchase_tra, rs);
                    result = CreateSalesTaxDetail(ModelSalesMaster.sales_tra.taxdetails.listTaxDetails, rs);
                }
            }
            catch
            {

                result = false;
            }
            //    ts.Complete();
            //    ts.Dispose();
            //}
            return result;
        }

        private long Save(Sales_Master TblSalesMaster)
        {
            bool result = false;
            try
            {
                bool res = _db.Sales_Master.Any(m => m.Company_Id == _userLog.CompanyId && (0 == TblSalesMaster.Id || m.Id != TblSalesMaster.Id) && (m.Sales_VC_No.ToUpper().Trim().Equals(TblSalesMaster.Sales_VC_No.ToUpper().Trim())));
                if (res == false)
                {
                    if (TblSalesMaster.Id == 0)
                    {
                        TblSalesMaster.Id = _db.Sales_Master.Count() > 0 ? _db.Sales_Master.Max(x => x.Id) + 1 : 1;
                        _db.Sales_Master.Add(TblSalesMaster);
                    }
                    _db.SaveChanges();
                    result = true;

                }
            }
            catch
            {
                return 0;
            }
            return result == true ? TblSalesMaster.Id : 0;
        }
       
        public bool Update(ViewModel.Ledger.SalesMaster ModelSalesMaster)
        {
            bool result = false;
            //using (TransactionScope ts = new TransactionScope())
            //{
                try
                {
                    Sales_Master TblSalesMaster = _db.Sales_Master.Find(ModelSalesMaster.Id);
                    TblSalesMaster.Fncl_Year_Id = ModelSalesMaster.Finance_Id != 0 ? ModelSalesMaster.Finance_Id : _userLog.FinancialId;

                    TblSalesMaster.Sales_VC_No = ModelSalesMaster.Challan_Number;

                    TblSalesMaster.Modified_By = _userLog.LoginId;
                    TblSalesMaster.Refered_Id = ModelSalesMaster.Refered_Id != 0 ? ModelSalesMaster.Refered_Id : 0;

                    TblSalesMaster.Refered_Type = ModelSalesMaster.Refered_Type;
                    TblSalesMaster.Is_Refered = ModelSalesMaster.Is_Refered;
                    TblSalesMaster.Modified_Date = DateTime.Now;
                    TblSalesMaster.DATE = ModelSalesMaster.Purchase_Date;
                    TblSalesMaster.Remarks = ModelSalesMaster.Remarks;
                    TblSalesMaster.Customer_Id = ModelSalesMaster.Customer_Id;
                    TblSalesMaster.Total_Amount = ModelSalesMaster.Grand_Total;
                    TblSalesMaster.Discount_Amount = ModelSalesMaster.Discount != 0 ? ModelSalesMaster.Discount : 0.0m;
                    TblSalesMaster.Tax_Amount = ModelSalesMaster.Tax_Amount;
                    
                    var rs = Save(TblSalesMaster);
                    if (rs != 0)
                    {
                        
                        result = UpdateSales_TraAndStock_master(ModelSalesMaster.sales_tra.listpurchase_tra, rs);
                        result = CreateSalesTaxDetail(ModelSalesMaster.sales_tra.taxdetails.listTaxDetails, rs);
                    }
                }
                catch
                {

                    result = false;
                }
            //    ts.Complete();
            //    ts.Dispose();

            //}

            return result;
        }
        [Obsolete("Use SP",true)]
        public bool CreatePurchaseTra(List<ViewModel.Ledger.Purchase_Tra> ModelSalesTra, long Sales_Id, int Id)
        {
            bool result = false;
            try
            {
                // List<Purchase_Master_Tra> TblPurMasterTra = new List<Purchase_Master_Tra>();
                List<Sales_Master_Tra> TblPurMasterTra = new List<Sales_Master_Tra>();
                Sales_Master_Tra tbltra;
                if (ModelSalesTra != null && Sales_Id != 0)
                {
                    string IDs = "";
                    foreach (var item in ModelSalesTra)
                    {
                        if (!IDs.Contains(item.Item_Id.ToString()))
                        {
                            IDs += item.Item_Id + ",";
                        }
                    }
                    IDs.Remove(IDs.LastIndexOf(','));

                    if (Id > 0)
                    {
                        var salestralist = _db.Sales_Master_Tra.Where(x => x.Sales_Id == Sales_Id).ToList();
                        List<Purchase_Master_Tra> listtra = new List<Purchase_Master_Tra>();
                        Purchase_Master_Tra p_tra = new Purchase_Master_Tra();
                        foreach (var item in salestralist)
                        {
                            var PurchaseTra = _db.Purchase_Master_Tra.Where(x => x.Sale_Serial_No == item.Sale_Serial_No).FirstOrDefault();

                            _db.UpdateQtyAndIsActive_PurchaseTra(PurchaseTra.Id, PurchaseTra.Qty + 1, PurchaseTra.Is_Active, PurchaseTra.Is_Deleted);
                        }
                        //_db.Purchase_Master_Tra.AddRange(listtra);// I am at this line ...
                        //_db.SaveChanges();
                    }
                    // var lst = _db.Purchase_Master_Tra.Where(x => IDs.Contains(x.Item_Id.ToString()) && x.Is_Active == true).ToList();
                    List<ViewModel.Ledger.Purchase_Tra> list = new List<Purchase_Tra>();
                    
                    foreach (var x in _db.GetdatafromPurchaseTra())
                    {
                        list.Add(new Purchase_Tra { Id = x.Id, Item_Id = x.Item_Id, SerialNo = x.Sale_Serial_No, IsActive = x.Is_Active, IsDeleted = x.Is_Deleted });
                    }


                    _db.Pr_DeleteStock_Item(1, "SALES");
                    
                    int i = 0;
                    int itm_Id = 0;
                    foreach (var item in ModelSalesTra)
                    {
                        tbltra = new Sales_Master_Tra();

                        tbltra.Id = item.Id;
                        tbltra.Item_Id = item.Item_Id;
                        tbltra.Qty = item.Qty;
                        tbltra.Rate = item.Rate;
                        tbltra.Tax_Id = 0;
                        // tbltra.Sales_VC_No = Challan_No;
                        tbltra.GoDown_Id = item.GoDownId;

                        tbltra.Sale_Serial_No = item.SerialNo != null ? item.SerialNo : list.Where(x => x.IsActive == true).FirstOrDefault().SerialNo; //lst.Where(x => x.Item_Id == item.Item_Id && x.Is_Active == true).FirstOrDefault().Sale_Serial_No;

                        tbltra.Created_By =_userLog.LoginId;
                        tbltra.Created_Date = DateTime.Now;
                        tbltra.Is_Active = true;
                        tbltra.Is_Deleted = false;
                        tbltra.Modified_By =_userLog.LoginId;
                        tbltra.Modified_Date = DateTime.Now;
                        
                        TblPurMasterTra.Add(tbltra);
                        i++;

                        if (item.SerialNo == "")
                        {
                            var lst = list.FirstOrDefault(x => x.SerialNo == tbltra.Sale_Serial_No);
                            if (lst.Qty > 1)
                            {
                                lst.Qty -= 1;
                            }
                            else
                            {
                                lst.Qty -= 1;
                                lst.IsActive = false;
                                lst.IsDeleted = true;
                            }
                        }
                    }
                    _db.Sales_Master_Tra.AddRange(TblPurMasterTra);
                    _db.SaveChanges();


                    foreach (var item in list)
                    {
                        _db.UpdateQtyAndIsActive_PurchaseTra(item.Id, item.Qty, item.IsActive, item.IsDeleted);
                    }
                }
            }
            catch
            {

                result = false;
            }
            return result;
        }
       [Obsolete("Use SP",true)]
        public bool CreatePurchaseTra2(List<ViewModel.Ledger.Purchase_Tra> ModelSalesTra, long Sales_Id, int Id)
        {
            bool result = false;
            try
            {
                // List<Purchase_Master_Tra> TblPurMasterTra = new List<Purchase_Master_Tra>();
                List<Sales_Master_Tra> TblPurMasterTra = new List<Sales_Master_Tra>();
                Sales_Master_Tra tbltra;
                if (ModelSalesTra != null && Sales_Id != 0)
                {
                    string IDs = "";
                    foreach (var item in ModelSalesTra)
                    {
                        if (!IDs.Contains(item.Item_Id.ToString()))
                        {
                            IDs += item.Item_Id + ",";
                        }
                    }
                    IDs.Remove(IDs.LastIndexOf(','));

                    if (Id > 0)
                    {
                        var salestralist = _db.Sales_Master_Tra.Where(x => x.Sales_Id == Sales_Id).ToList();
                        // List<Purchase_Master_Tra> listtra = new List<Purchase_Master_Tra>();
                        Purchase_Master_Tra p_tra = new Purchase_Master_Tra();
                        foreach (var item in salestralist)
                        {
                            var PurchaseTra = _db.Purchase_Master_Tra.Where(x => x.Sale_Serial_No == item.Sale_Serial_No).FirstOrDefault();
                            p_tra.Id = PurchaseTra.Id;
                            p_tra.Is_Active = true;
                            p_tra.Is_Deleted = false;
                            p_tra.Qty += 1;

                            _db.SaveChanges();
                        }
                        //_db.Purchase_Master_Tra.AddRange(listtra);// I am at this line ...
                        //_db.SaveChanges();
                    }
                    var lst = _db.Purchase_Master_Tra.Where(x => IDs.Contains(x.Item_Id.ToString()) && x.Is_Active == true).ToList();
                    _db.Pr_DeleteStock_Item(1, "SALES");
                    //List<Purchase_Master_Tra> list =new List<Purchase_Master_Tra>() ;
                    int i = 0;
                    //  int itm_Id = 0;
                    foreach (var item in ModelSalesTra)
                    {
                        tbltra = new Sales_Master_Tra();

                        tbltra.Id = item.Id;
                        tbltra.Item_Id = item.Item_Id;
                        tbltra.Qty = item.Qty;
                        tbltra.Rate = item.Rate;
                        tbltra.Tax_Id = 0;
                        //  tbltra.Sales_VC_No = Challan_No;
                        tbltra.GoDown_Id = item.GoDownId;

                        tbltra.Sale_Serial_No = lst.Where(x => x.Item_Id == item.Item_Id && x.Is_Active == true).FirstOrDefault().Sale_Serial_No;
                        foreach (var item1 in lst.Where(x => x.Sale_Serial_No == tbltra.Sale_Serial_No && x.Is_Active == true))
                        {
                            if (item1.Qty == 1 || item1.Qty == 0)
                            {
                                //lst.Remove(lst.Single(s => s.Id == item1.Id));
                                //lst.Where(w => w.Sale_Serial_No == tbltra.Sale_Serial_No).FirstOrDefault(k =>  k.Is_Active = false  );
                                //lst.Where(w => w.Sale_Serial_No == tbltra.Sale_Serial_No).FirstOrDefault(s => s.Qty=Convert.ToDecimal(item1.Qty));
                                var dd = lst.Where(w => w.Sale_Serial_No == tbltra.Sale_Serial_No).FirstOrDefault();

                                if (dd != null)
                                {
                                    dd.Qty -= 1;
                                    dd.Is_Active = false;

                                }
                            }
                            else
                            {
                                item1.Qty -= 1;

                                if (item1.Qty == 0)
                                {
                                    lst.Where(w => w.Sale_Serial_No == tbltra.Sale_Serial_No).FirstOrDefault(k => k.Is_Active = false);
                                    lst.Where(w => w.Sale_Serial_No == tbltra.Sale_Serial_No).Select(s => { s.Qty = s.Qty - 1; return s; });
                                }
                                //lst.Where(w=> w.Id == item1.Id).ToList().ForEach(k => k.Qty = (k.Qty- 1));
                                lst.Where(w => w.Sale_Serial_No == tbltra.Sale_Serial_No).ToList().ForEach(k => k.Qty = (k.Qty - 1));
                            }
                            break;
                        }


                        tbltra.Created_By =_userLog.LoginId;
                        tbltra.Created_Date = DateTime.Now;
                        tbltra.Is_Active = true;
                        tbltra.Is_Deleted = false;
                        tbltra.Modified_By = _userLog.LoginId;
                        tbltra.Modified_Date = DateTime.Now;
                        
                        TblPurMasterTra.Add(tbltra);
                        i++;
                    }
                    _db.Sales_Master_Tra.AddRange(TblPurMasterTra);
                    _db.SaveChanges();

                    foreach (var item in lst)
                    {
                        if (item.Is_Active == false)
                        {
                            var Purchase_tra = _db.Purchase_Master_Tra.Where(x => x.Id == item.Id).FirstOrDefault();
                            Purchase_tra.Is_Active = false;
                            Purchase_tra.Is_Deleted = true;
                        }
                        else
                        {
                            var Purchase_tra = _db.Purchase_Master_Tra.Where(x => x.Id == item.Id).FirstOrDefault();
                            Purchase_tra.Qty = item.Qty;

                        }

                        _db.SaveChanges();
                    }
                }
            }
            catch
            {

                result = false;
            }
            return result;
        }
        public ViewModel.Ledger.SalesMaster Find(long Id = 0)
        {
            ViewModel.Ledger.SalesMaster ModelSalesMaster = new SalesMaster();
            Sales_Master tblSalesMaster = Id > 0 ? _db.Sales_Master.Find(Id) :Id<0? _db.Sales_Master.OrderByDescending(x=>x.DATE).FirstOrDefault(x=>x.Is_Deleted==false && x.Company_Id==_userLog.CompanyId):null;
            if (tblSalesMaster !=null)
            {

           
                ModelSalesMaster.Id = tblSalesMaster.Id;
                ModelSalesMaster.Challan_Number = tblSalesMaster.Sales_VC_No;
                ModelSalesMaster.Tax_Amount = tblSalesMaster.Tax_Amount??0;
                
                ModelSalesMaster.Discount = tblSalesMaster.Discount_Amount ?? 0.00m;
                ModelSalesMaster.Finance_Id = tblSalesMaster.Fncl_Year_Id;
                ModelSalesMaster.Grand_Total = tblSalesMaster.Total_Amount ?? 0.00m;
                ModelSalesMaster.Is_Refered = tblSalesMaster.Is_Refered;
                ModelSalesMaster.Refered_Id = tblSalesMaster.Refered_Id ?? 0;
                ModelSalesMaster.Refered_Type = tblSalesMaster.Refered_Type;
                ModelSalesMaster.Remarks = tblSalesMaster.Remarks;
                ModelSalesMaster.Customer_Id = tblSalesMaster.Customer_Id;
                ModelSalesMaster.Purchase_Date = tblSalesMaster.DATE;
             
                if(ModelSalesMaster.Customer_Id>0)
                {
                    Ledger_Master lg = _db.Ledger_Master.FirstOrDefault(x => x.Id == tblSalesMaster.Customer_Id);
                    ModelSalesMaster.Customer_Name = lg.Name;
                    ModelSalesMaster.Email_Id = lg.Email_Id ?? "";
                    ModelSalesMaster.Contact_No = lg.Contact_No ?? "";
                    
                }
            }
            return ModelSalesMaster;
        }
        [Obsolete("Use SP",true)]
        public bool CreateStockMaster(List<ViewModel.Ledger.Sales_Tra> ModelSalTra)
        {
            List<Stock_Master> lstStockMaster = new List<Stock_Master>();
            
            Stock_Master tblStockMaster;
            bool result = false;
            bool existsItem = false;
            foreach (var item in ModelSalTra)
            {
                existsItem = _db.Stock_Master.Any(m => m.Item_Id == item.Item_Id);
                if (existsItem == true)
                {
                    tblStockMaster = _db.Stock_Master.SingleOrDefault(x => x.Item_Id == item.Item_Id);
                    tblStockMaster.Qty = tblStockMaster.Qty - item.Qty;
                    // tblStockMaster.Rate = item.Rate;
                }


            }
            //  _db.Stock_Master.AddRange(lstStockMaster);
            _db.SaveChanges();
            result = true;
            return result;
        }
        
        private bool UpdateSales_TraAndStock_master(List<ViewModel.Ledger.Purchase_Tra> ModelSalTra, long Sale_Id)
        {
            System.Data.DataTable tbl = new System.Data.DataTable("Product");
            System.Data.DataColumn[] columns = new System.Data.DataColumn[14];
            columns[0] = new System.Data.DataColumn("Item_Id", typeof(int));
            columns[1] = new System.Data.DataColumn("Qty", typeof(decimal));
            columns[2] = new System.Data.DataColumn("Rate", typeof(decimal));
            columns[3] = new System.Data.DataColumn("Sales_Id", typeof(long));
            //columns[3] = new System.Data.DataColumn("Challan_No", typeof(string));
            columns[4] = new System.Data.DataColumn("Godown_Id", typeof(int));
            columns[5] = new System.Data.DataColumn("SerialNo", typeof(string));
            columns[6] = new System.Data.DataColumn("Created_By", typeof(int));
            columns[7] = new System.Data.DataColumn("Created_Date", typeof(DateTime));
            columns[8] = new System.Data.DataColumn("Modified_By", typeof(int));
            columns[9] = new System.Data.DataColumn("Modified_Date", typeof(DateTime));
            columns[10] = new System.Data.DataColumn("Is_Active", typeof(bool));
            columns[11] = new System.Data.DataColumn("Is_Deleted", typeof(bool));

            tbl.Columns.AddRange(columns);

            foreach (var item in ModelSalTra)
            {
                var row = tbl.NewRow();

                row["Item_Id"] = item.Item_Id;
                row["Qty"] = item.Qty;
                row["Rate"] = item.Rate;
                row["Sales_Id"] = Sale_Id;
                //row["Challan_No"] = "";
                row["Godown_Id"] = item.GoDownId;
                row["SerialNo"] = item.SerialNo;
                row["Created_By"] = _userLog.LoginId;
                row["Created_Date"] = DateTime.Now;
                row["Modified_By"] = _userLog.LoginId;
                row["Modified_Date"] = DateTime.Now;
                row["Is_Active"] = true;
                row["Is_Deleted"] = false;

                tbl.Rows.Add(row);
            }

            DatabaseConfiguration.DbConfiguration.InitializeConnection(_userLog.ConnectionString);

            var builder = new EntityConnectionStringBuilder(DatabaseConfiguration.DbConfiguration.ConnectionString);
            var regularConnectionString = builder.ProviderConnectionString;
            using (SqlConnection connection = new SqlConnection(regularConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("", connection);

                // command.CommandText = "Pr_UpdateStockMaster"; 
                //command.CommandText = "Pr_UpdateStockMaster";
                command.CommandText = "Update_stock_and_Tra";
                command.Parameters.Clear();
                command.CommandType = CommandType.StoredProcedure;
                

                command.Parameters.AddWithValue("@tblsales_tra", tbl).SqlDbType = SqlDbType.Structured;
                command.Parameters.AddWithValue("@tblStockMaster", null).SqlDbType = SqlDbType.Structured;
                command.Parameters.AddWithValue("@pur_Id", Sale_Id).SqlDbType = SqlDbType.BigInt;
                command.Parameters.AddWithValue("@Tag", "Sales").SqlDbType.ToString();
                command.ExecuteNonQuery();

                connection.Close();

                bool result = false;

                return result;
            };
        }
        public bool CreateSalesTaxDetail(List<ViewModel.Common.TaxDetails> taxdetails, long SalesId)
        {
            List<Purchase_Master_TaxDetails> listtax = new List<Purchase_Master_TaxDetails>();

            if (taxdetails != null)
            {
                _db.Pr_DeleteStock_Item(SalesId, "SALESTAXDETAILS");
                _db.Sales_Master_TaxDetails.AddRange(taxdetails.Select(x => new Sales_Master_TaxDetails
                {
                    Id = x.TId,
                    // Company_Id = CompanyId,
                    Sales_Id = SalesId,
                    Sales_VC_No = "",
                    Tax_Id = Convert.ToInt32(x.Id.Split('_')[0]),
                    Tax_Rate = Convert.ToDecimal(x.Rate_Type),
                    Tax_Amount = Convert.ToDecimal(x.Value),
                    Created_By = _userLog.LoginId,
                    Created_Date = DateTime.Now,
                    Modified_By = _userLog.LoginId,
                    Modified_Date = DateTime.Now,
                    Is_Active = true,
                    Is_Deleted = false
                }));

              
            }

            // _db.Purchase_Master_TaxDetails.AddRange(listtax);
            _db.SaveChanges();
            return true;
        }
        public String GEN_ChallanNo()
        {
            string Challan_No = "";

            int countRows = _db.Sales_Master.Where(x => x.Company_Id == 1).Count();
            if (countRows != 0)
                Challan_No = _db.Sales_Master.OrderBy(x => x.Id).Skip(countRows - 1).FirstOrDefault().Sales_VC_No;
            if (!string.IsNullOrWhiteSpace(Challan_No))
            {
                Challan_No = Regex.Replace(Challan_No, @"\d+(?=\D*$)",
                        m => (Convert.ToInt64(m.Value) + 1).ToString().PadLeft(5, '0'));
            }
            else
            {
                Challan_No = "1".PadLeft(5, '0');
            }


            return Challan_No;
        }




        public bool Gen_ChallanOfSales(int Id)
        {
            bool result = false;
            try
            {
                var tblSalMaster = _db.Sales_Master.Find(Id);
                tblSalMaster.Is_Bill_Gen = true;
                tblSalMaster.Modified_Date = DateTime.Now;
                tblSalMaster.Modified_By =  _userLog.LoginId;
                _db.SaveChanges();
                result = true;
                if (result == true)
                {
                    Billing_Info tblBilling = new Billing_Info();
                    tblBilling.Is_Active = true;
                    tblBilling.Is_Deleted = false;
                    tblBilling.Modified_By =  _userLog.LoginId;

                    tblBilling.Modified_Date = DateTime.Now;
                    tblBilling.Created_By =  _userLog.LoginId;
                    tblBilling.Created_Date = DateTime.Now;
                    tblBilling.Discount_Amount = tblSalMaster.Discount_Amount;
                    tblBilling.Company_Id = tblSalMaster.Company_Id;
                    tblBilling.Billing_Amount = tblSalMaster.Total_Amount;
                    //  tblBilling.Chalan_No = tblSalMaster.Sales_VC_No;
                    tblBilling.Remarks = "After gen challan";
                    tblBilling.Tax_Amount = 0;
                    tblBilling.Transport = 0;
                    tblBilling.Amount = (tblBilling.Billing_Amount + tblBilling.Tax_Amount + tblBilling.Transport) - tblBilling.Discount_Amount;
                    tblBilling.Billing_Date = tblSalMaster.DATE;
                    tblBilling.User_Id = tblSalMaster.Customer_Id;
                    tblBilling.Tag_Type = "CUSTOMER";
                    _db.Billing_Info.Add(tblBilling);
                    _db.SaveChanges();

                }
            }
            catch
            {
                return result;

            }
            return result;
        }

        
        public bool PurchageReturn(List<ViewModel.Ledger.Challan_Details> ModelPurMaster)
        {
            bool result = false;
            try
            {
                Sales_Return TblPurReturn = new Sales_Return();
                //  TblPurMaster.Id = ModelPurMaster.Id;
                TblPurReturn.Fncl_Year_Id = 1;
                //TblPurReturn = 1;

                
                TblPurReturn.Is_Active = true;
                TblPurReturn.Is_Deleted = false;
                TblPurReturn.Modified_By = 1;
                
                TblPurReturn.Created_By = 1;
                TblPurReturn.Created_Date = DateTime.Now;
                TblPurReturn.Modified_By = 1;
                TblPurReturn.Modified_Date = DateTime.Now;
                TblPurReturn.DATE = Convert.ToDateTime(ModelPurMaster.FirstOrDefault().Purchase_Date);
                TblPurReturn.Remarks = ModelPurMaster.FirstOrDefault().Remarks;
                //TblPurReturn.pur = ModelPurMaster.FirstOrDefault().SupplierId;
                TblPurReturn.Total_Amount = ModelPurMaster.FirstOrDefault().Grand_Total;
                TblPurReturn.Discount_Amount = ModelPurMaster.FirstOrDefault().Discount != 0 ? ModelPurMaster.FirstOrDefault().Discount : 0.0m;
                TblPurReturn.Is_Deleted = false;
                TblPurReturn.Is_Active = true;
                TblPurReturn.Created_By = 1;
                TblPurReturn.Modified_By = 1;

                TblPurReturn.Created_Date = System.DateTime.Now;
                TblPurReturn.Modified_Date = System.DateTime.Now;
                var status = false;
                _db.Sales_Return.Add(TblPurReturn);
                _db.SaveChanges();
                status = true;

                var rs = false;
                // var rs = Save(TblPurReturn);
                if (status == true)
                {
                    rs = true;
                    result = ReturnPurchaseTra(ModelPurMaster, TblPurReturn.Id);
                }
                if (result == true)
                {
                    result = UpdatetockMaster(ModelPurMaster);
                }
                result = rs;
            }
            catch
            {

                result = false;
            }
            return result;
        }

        
        public bool ReturnPurchaseTra(List<ViewModel.Ledger.Challan_Details> ModelPurMasterTra, long Pur_Id)
        {
            bool result = false;
            try
            {
                List<Sales_Return_Tra> TblPurMasterTra = new List<Sales_Return_Tra>();
                Sales_Return_Tra tbltra;
                if (ModelPurMasterTra != null && Pur_Id != 0)
                {
                    _db.Pr_DeleteStock_Item(Pur_Id, "");
                    foreach (var item in ModelPurMasterTra)
                    {
                        if (item.ReturnQuantity != 0)
                        {
                            tbltra = new Sales_Return_Tra();

                            tbltra.Id = item.Id;
                            tbltra.Item_Id = item.ItemId;
                            tbltra.Qty = item.ReturnQuantity;
                            tbltra.Rate = item.Rate;
                            // tbltra.Tax_Id = item.Tax;
                            // tbltra.Chalan_No = Challan_No;

                            tbltra.Created_By =  _userLog.LoginId;
                            tbltra.Created_Date = DateTime.Now;
                            tbltra.Is_Active = true;
                            tbltra.Is_Deleted = false;
                            tbltra.Modified_By = 1;
                            tbltra.Modified_Date = DateTime.Now;


                            tbltra.Modified_By =  _userLog.LoginId;
                            TblPurMasterTra.Add(tbltra);
                        }
                    }
                    _db.Sales_Return_Tra.AddRange(TblPurMasterTra);
                    _db.SaveChanges();
                    result = true;
                }
            }
            catch
            {

                result = false;
            }
            return result;
        }
        
        public bool UpdatetockMaster(List<ViewModel.Ledger.Challan_Details> ModelPurMaster)
        {
            for (int i = 0; i < ModelPurMaster.Count(); i++)
            {
                if (ModelPurMaster[i].ReturnQuantity != 0)
                {
                    long id = ModelPurMaster[i].ItemId;
                    var findData = _db.Stock_Master.SingleOrDefault(m => m.Item_Id == id);
                    findData.Qty = findData.Qty + ModelPurMaster[i].ReturnQuantity;

                    _db.SaveChanges();
                }
            }

            return true;
        }
        public List<ViewModel.Common.TaxDetails> GetTaxList(long Sales_Id)
        {
            var list = _db.Sales_Master_TaxDetails.Where(x => x.Sales_Id == Sales_Id).ToList();
            List<ViewModel.Common.TaxDetails> listobj = new List<ViewModel.Common.TaxDetails>();
            list.ForEach((x) => listobj.Add(new ViewModel.Common.TaxDetails
            {
                TId = x.Id,
                Id = _db.Tax_Deduction_Master.Where(p => p.Id == x.Tax_Id && p.Is_Active == true).Select(t => t.Id + "_" + t.Type + "_" + t.Rate_Type + "_" + t.Rate).First().ToString(),
                Rate_Type = Convert.ToString(x.Tax_Rate),
                Value = Convert.ToString(x.Tax_Amount),

            }));
            return listobj;
        }
        public bool Delete(long Id)
        {
            bool result = false;
            try
            {
                if (Id != 0)
                {
                    Sales_Master tblSales = _db.Sales_Master.Find(Id);
                    if (tblSales != null)
                    {
                        tblSales.Modified_By =  _userLog.LoginId;
                        tblSales.Modified_Date = DateTime.Now;
                        tblSales.Is_Deleted = true;
                        _db.Sales_Master_Tra.Where(x => x.Sales_Id == tblSales.Id).ToList().ForEach(x => x.Is_Deleted = true);
                        _db.SaveChanges();
                    }
                    result = true;
                }
            }
            catch
            {

                result = false;
            }


            return result;
        }
        //public int DeleteSalesMaster(long ID)
        //{
            
        //    var data = _db.Sales_Master.Find(ID);
        //    data.Is_Active = false;
        //    data.Is_Deleted = true;
        //    return _db.SaveChanges();
        //}
        
        public List<string> GetSerial_No(int Id, int Qty, string SrNo)
        {
            var lst = _db.Purchase_Master_Tra.Where(x => x.Item_Id == Id && (SrNo == "" || !SrNo.Contains(x.Sale_Serial_No)) && x.Is_Active == true).OrderBy(x => x.Sale_Serial_No).Select(x => x.Sale_Serial_No).ToList();
            string temp;

            dynamic p1 = null;
            dynamic p2 = null;
            //for (int i = 0; i < lst.Count(); i++)
            //{
            //    for (int j = i + 1; j < lst.Count(); j++)
            //    {
            //        p1 = Regex.Replace(lst[i], @"\d+(?=\D*$)", m => (Convert.ToInt64(m.Value)).ToString());
            //        //p2 = Convert.ToInt32(lst[i].Split('-')[1]);
            //        p2 = Regex.Replace(lst[j], @"\d+(?=\D*$)", m => (Convert.ToInt64(m.Value)).ToString());
            //        if (p1>p2)
            //        {
            //            temp = lst[i];
            //            lst[i] = lst[j];
            //            lst[j] = temp;
            //        }
            //    }

            //}

            return lst.Take(Qty).ToList();
        }
    }
}

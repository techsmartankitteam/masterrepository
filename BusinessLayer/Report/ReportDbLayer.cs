﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViewModel.Common;
using ViewModel.CommonReport;
using ViewModel.Users;
namespace DataLayer.Report
{
    public class ReportDbLayer
    {
        #region  Property
        INVENTORY_DBEntities _db;
        private ViewModel.Users.LoginInfo _userLog { get; set; }
        #endregion
        #region Constructor
        public ReportDbLayer()
        {
            _db = new INVENTORY_DBEntities();
            _userLog = new LoginInfo
            {
                CompanyId = 1,
                ConnectionString = "",
                FinancialId = 1,
                FinancialYear = "2015-2016",
                LoginId = 1,
                LoginName = "Ankit Singh",
                SecurityToken = Guid.NewGuid().ToString(),
                UserSession = Guid.NewGuid().ToString()
            };

        }
        //  [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
        //public ReportDbLayer(String ConnectionString)
        //{  
        //    _db = new INVENTORY_DBEntities();
        //      _db.Database.Connection.ConnectionString = ConnectionString;
        //      _userLog = new LoginInfo
        //      {
        //          CompanyId = 1,
        //          ConnectionString = ConnectionString,
        //          FinancialId = 1,
        //          FinancialYear = "2015-2016",
        //          LoginId = 1,
        //          LoginName = "Ankit Singh",
        //          SecurityToken = Guid.NewGuid().ToString(),
        //          UserSession = Guid.NewGuid().ToString()
        //      };
        //}
        public ReportDbLayer(ViewModel.Users.LoginInfo Log)
        {
            this._userLog = Log;
            _db = new INVENTORY_DBEntities();
            _db.Database.Connection.ConnectionString = Log.ConnectionString;

        }
        #endregion

        public List<DayBookInfo> DayBookList(DateTime From, DateTime To, string SearchText)
        {
            List<DayBookInfo> DayBookList = new List<DayBookInfo>();
            try
            {
                var lst = _db.GetFinancialData(_userLog.CompanyId).ToList();
                lst.ForEach(x => DayBookList.Add(new DayBookInfo
                {
                    VC_Id = x.VC_Id,
                    Amount = x.Amount,
                    CR = x.CR,
                    DR = x.DR,
                    EntryOn = x.EntryOn,
                    Name = x.Name,
                    VC_NO = x.VC_NO.ToString(),
                    Module=x.Module


                }));
            }
            catch
            {

                DayBookList = null;
            }


            return DayBookList;
        }
        public List<InventoryBookInfo> InventoryBookList(DateTime From, DateTime To,string SearchText)
        {
            List<InventoryBookInfo> InventoryList = new List<InventoryBookInfo>();
            
            try
            {
                var lst = _db.Pr_Stock_Voucher_Details(_userLog.CompanyId, From, To).ToList();
                lst.ForEach(x => InventoryList.Add(new InventoryBookInfo
                {
                    Item_Id=x.Item_Id,
                    Item_Name=x.Item_Name,
                    Date_On=x.Date_on,
                    Decimal_Place=x.Decimal_Place,
                    Rate=x.Rate,
                    ClosingQty=x.ClosingQty,
                    OpenQty=x.OpenQty,
                    Unit_Name=x.Unit_Name
                }));

            }
            catch 
            {
                InventoryList = new List<InventoryBookInfo>();
                
            }
            return InventoryList;
        }
        public List<OutStandingInfo> PayabalesBill(DateTime From, DateTime To, string SearchText)
        {
            List<OutStandingInfo> OutStandingBill = new List<OutStandingInfo>();

            try
            {
                var lst = _db.Pr_OutStanding_Bill(_userLog.CompanyId, _userLog.FinancialId, "PAYBALES").ToList();
                lst.ForEach(x => OutStandingBill.Add(new OutStandingInfo
                {
                    VC_Id=x.VC_Id,
                    VC_No=x.VC_NO,
                    VC_Date=x.VC_Date,
                    Bill_Amount=x.Total_Amount,
                    Ledger_Id=x.Ledger_Id,
                    Ledger_Name=x.Ledger_Name,
                    Paid_Amount=x.Paid_Amount

                }));

            }
            catch
            {
                OutStandingBill = null;

            }
            return OutStandingBill;
        }
        public List<OutStandingInfo> ReceivablesBill(DateTime From, DateTime To, string SearchText)
        {
            List<OutStandingInfo> OutStandingBill = new List<OutStandingInfo>();

            try
            {
                var lst = _db.Pr_OutStanding_Bill(_userLog.CompanyId, _userLog.FinancialId, "RECEIVABALES").ToList();
                lst.ForEach(x => OutStandingBill.Add(new OutStandingInfo
                {
                    VC_Id = x.VC_Id,
                    VC_No = x.VC_NO,
                    VC_Date = x.VC_Date,
                    Bill_Amount = x.Total_Amount,
                    Ledger_Id = x.Ledger_Id,
                    Ledger_Name = x.Ledger_Name,
                    Paid_Amount = x.Paid_Amount

                }));

            }
            catch
            {
                OutStandingBill = null;

            }
            return OutStandingBill;
        }

        public List<OrdersInfo> OrdersList(string Tag,DateTime From, DateTime To, string SearchText)
        {
            List<OrdersInfo> OrdersList = new List<OrdersInfo>();

            try
            {
                var lst = _db.Pr_Rpt_Order_List(_userLog.CompanyId, Tag).ToList();
                //var lst = _db.Pr_OutStanding_Bill(1, 1, "RECEIVABALES").ToList();
                lst.ForEach(x => OrdersList.Add(new OrdersInfo
                {
                    VC_Id = x.VC_Id,
                    VC_No = x.VC_No,
                    VC_Date = x.VC_Date,
                    Amount=x.Amount,
                    Decimal_Place=x.Decimal_Place,
                     Item_Id=x.Item_Id,
                     Item_Name=x.Item_Name,
                     Ledger_Id=x.Ledger_Id,
                     Ledger_Name=x.Ledger_Name,
                     Qty=x.Qty,
                     Rate=x.Rate,
                     Refered_Type=x.Refered_Type,
                     Sales_Person_Id=x.Sales_Person_Id,
                     Status=x.Status,
                     Unit_Name=x.Unit_Name

                    

                }));

            }
            catch
            {
                OrdersList = null;

            }
            return OrdersList;
        }
        public List<AccountsBookInfo> AccountsBookList(string Tag, DateTime From, DateTime To, string SearchText)
        {
            List<AccountsBookInfo> AccountsBookList = new List<AccountsBookInfo>();
            
            try
            {
                var lst = _db.Pr_Rpt_Sales_Purchase(_userLog.CompanyId, Tag).ToList();
                //var lst = _db.Pr_OutStanding_Bill(1, 1, "RECEIVABALES").ToList();
                lst.ForEach(x => AccountsBookList.Add(new AccountsBookInfo
                {
                    VC_Id = x.VC_Id,
                    VC_No = x.VC_No,
                    VC_Date = x.VC_Date,
                    Amount = x.Amount,
                    Decimal_Place = x.Decimal_Place,
                    Item_Id = x.Item_Id,
                    Item_Name = x.Item_Name,
                    Ledger_Id = x.Ledger_Id,
                    Ledger_Name = x.Ledger_Name,
                    Qty = x.Qty,
                    Rate = x.Rate,
                    Refered_Type = x.Refered_Type,
                    
                    Serial_No=x.Serial_No,
                    Is_Auto_Serial=x.Is_Auto_Serial,
                    Is_Return=x.Is_Return,
                    Unit_Name = x.Unit_Name



                }));

            }
            catch
            {
                AccountsBookList = new List<AccountsBookInfo>();

            }
            return AccountsBookList;
        }
    }
}

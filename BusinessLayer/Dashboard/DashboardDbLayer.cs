﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViewModel.Dashboard;
using DataLayer.Transactions;
using ViewModel.Users;
namespace DataLayer.Dashboard
{
    public class DashboardDbLayer
    {
              /// <summary>
        /// This layer is used to display data on dashboard panel
        /// </summary>
        /// 
        #region    Property
        private ViewModel.Users.LoginInfo _userLog { get; set; }
        INVENTORY_DBEntities _db;
        
           #endregion
        #region Constructor
        public DashboardDbLayer()
        {
              _db = new INVENTORY_DBEntities();
              _userLog = new LoginInfo
              {
                  CompanyId = 1,
                  ConnectionString = "",
                  FinancialId = 1,
                  FinancialYear = "2015-2016",
                  LoginId = 1,
                  LoginName = "Ankit Singh",
                  SecurityToken = Guid.NewGuid().ToString(),
                  UserSession = Guid.NewGuid().ToString()
              };
        
            
        }
       [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
        public DashboardDbLayer(String ConnectionString)
        {  
            
            _db = new INVENTORY_DBEntities();
            _db.Database.Connection.ConnectionString = ConnectionString;
            _userLog = new LoginInfo
            {
                CompanyId = 1,
                ConnectionString = ConnectionString,
                FinancialId = 1,
                FinancialYear = "2015-2016",
                LoginId = 1,
                LoginName = "Ankit Singh",
                SecurityToken = Guid.NewGuid().ToString(),
                UserSession = Guid.NewGuid().ToString()
            };
        
        }
        public DashboardDbLayer(ViewModel.Users.LoginInfo Log)
        {
            this._userLog = Log;
            _db = new INVENTORY_DBEntities();
            _db.Database.Connection.ConnectionString =Log.ConnectionString;

        }


         #endregion


        public List<PieGraphInfo> PieGraph()
        {
            List<PieGraphInfo> list = new List<PieGraphInfo>();
            return _db.Pr_DashOrderQuats(_userLog.CompanyId).Select(_data => new PieGraphInfo { Name = _data.Name, Total = _data.Total }).ToList();
           // return list;
        }

        public List<DashboardInfo> SelesPurchaseGraph()
        {
            string end = DateTime.Now.ToString("yyyy-MM-dd");
           return _db.SP_DashSalePurc(Convert.ToInt32(_userLog.CompanyId), DateTime.Now.AddMonths(-12), Convert.ToDateTime(end)).Select(_data => new DashboardInfo { Months = _data.Months, TotalPurchase = _data.TotalPurchase, TotalSale = _data.TotalSale }).ToList();
        }

        //Get Top 10 Sales order list
        public IEnumerable<ViewModel.Dashboard.SalerOrderinfo> SalesOrderList(int No_of_Records=10)
        {
            return _db.Pr_Sales_Order_List(_userLog.CompanyId)
            .Select(_data => 
            new ViewModel.Dashboard.SalerOrderinfo { 
            Customer_Name = _data.Customer_Name, Contact_No = _data.Contact_No, Order_No = _data.Order_No,
             Order_Date = _data.Order_Date.ToShortDateString(), Request_Delivery_Date = _data.Request_Delivery_Date.ToShortDateString(), Status = _data.Status, Amount = _data.Amount
              });
        }


        //Get top 10 Purchase Order
        public IEnumerable<Purchaseinfo> PurchaseOrderList(int No_of_Records=10)
        {
            return _db.Pr_Purchase_Order_List(_userLog.CompanyId).
            Take(No_of_Records)
            .Select(_data => 
            new ViewModel.Dashboard.Purchaseinfo { 
            Supplier_Name = _data.Customer_Name, Contact_No = _data.Contact_No, Order_No = _data.Order_No, Order_Date = _data.Order_Date, Request_Delivery_Date = _data.Request_Delivery_Date, Status = _data.Status, Amount = _data.Amount 
            });
            
        }

        //Manage Stock List
        public IEnumerable<ManageStockInfo> ManageStock()
        {
            return _db.SP_ManageStock(Convert.ToInt32( _userLog.CompanyId)).Take(10).Select(m => new ManageStockInfo { Id = m.Id, Name = m.Name, Item_Code = m.Item_Code, Min_Qty = m.Min_Qty, Total_Qty = m.Total_Qty, Selling_Rate = m.Selling_Rate, Purchase_Rate = m.Purchase_Rate, ManageStock = m.ManageStock, Status = m.Status }).ToList();
           
        }

    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataLayer
{
    using System;
    
    public partial class Pr_OutStanding_Bill_Result
    {
        public long VC_Id { get; set; }
        public string VC_NO { get; set; }
        public System.DateTime VC_Date { get; set; }
        public long Ledger_Id { get; set; }
        public string Ledger_Name { get; set; }
        public decimal Total_Amount { get; set; }
        public decimal Paid_Amount { get; set; }
    }
}

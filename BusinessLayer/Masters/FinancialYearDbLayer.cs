﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using HDMEntity;
using ViewModel.Common;
using ViewModel.Users;
namespace DataLayer.Masters
{
 public   class FinancialYearDbLayer
    {
     INVENTORY_DBEntities _db;
     private LoginInfo _userLog { get; set; }
     public FinancialYearDbLayer()
     {
         _db = new INVENTORY_DBEntities();
         _userLog = new LoginInfo
         {
             CompanyId = 1,
             ConnectionString = _db.Database.Connection.ConnectionString,
             FinancialId = 1,
             FinancialYear = "2015-2016",
             LoginId = 1,
             LoginName = "Ankit Singh",
             SecurityToken = Guid.NewGuid().ToString(),
             UserSession = Guid.NewGuid().ToString()
         };
     
     }
     [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
     public FinancialYearDbLayer(String ConnectionString)
     {   
         _db = new INVENTORY_DBEntities();
         _db.Database.Connection.ConnectionString = ConnectionString;
         _userLog = new LoginInfo
         {
             CompanyId = 1,
             ConnectionString =ConnectionString,
             FinancialId = 1,
             FinancialYear = "2015-2016",
             LoginId = 1,
             LoginName = "Ankit Singh",
             SecurityToken = Guid.NewGuid().ToString(),
             UserSession = Guid.NewGuid().ToString()
         };
     }
     public FinancialYearDbLayer(LoginInfo _log)
        {
            _db = new INVENTORY_DBEntities();
            _db.Database.Connection.ConnectionString = _log.ConnectionString;
            _userLog = _log;
        }
     public List<ViewModel.Common.FinancialYear> List(String Terms="")
     {
         //FinancialYear modelFincial = new FinancialYear();
         if (Terms.Trim().Length > 0)
             return _db.FinancialYears.Where(x => x.Company_Id == _userLog.CompanyId && x.Is_Deleted == false && x.Name.Trim().ToUpper().StartsWith(Terms.Trim().ToUpper())).Select(x => new
             ViewModel.Common.FinancialYear { Id = x.Id, Name = x.Name, Remarks = x.Remarks, FromPeriod = x.From_Date.ToString(), ToPeriod = x.To_Date.ToString() }).ToList();
         else
             return _db.FinancialYears.Where(x => x.Company_Id == _userLog.CompanyId && x.Is_Deleted == false).Select(x => new
            ViewModel.Common.FinancialYear { Id = x.Id, Name = x.Name, Remarks = x.Remarks, FromPeriod = x.From_Date.ToString(), ToPeriod = x.To_Date.ToString() }).ToList();
         
     }
     public bool Create(ViewModel.Common.FinancialYear modelFincial)
     {
         bool result = false;
         FinancialYear tblFincial = new FinancialYear();


         //  tblunit.Id = modelunit.Id;
         tblFincial.Name = modelFincial.Name;
         tblFincial.Remarks = modelFincial.Remarks;
         tblFincial.To_Date = Convert.ToDateTime(modelFincial.ToPeriod);
         tblFincial.From_Date = Convert.ToDateTime(modelFincial.FromPeriod);
         tblFincial.Is_Active = true;
         tblFincial.Is_Deleted = false;
         tblFincial.Company_Id = _userLog.CompanyId;
         tblFincial.Created_By = _userLog.LoginId;
         tblFincial.Created_Date = DateTime.Now;
         tblFincial.Modified_Date = DateTime.Now;
         tblFincial.Modified_By = _userLog.LoginId;
         result = Save(tblFincial);
         return result;

     }
     public bool Update(ViewModel.Common.FinancialYear modelFincial)
     {
         bool result = false;

         if (modelFincial.Id != 0)
         {
             FinancialYear tblFincial = _db.FinancialYears.Find(modelFincial.Id);
             tblFincial.Id = modelFincial.Id;
             tblFincial.Name = modelFincial.Name;
             tblFincial.Remarks = modelFincial.Remarks;
             tblFincial.To_Date = Convert.ToDateTime(modelFincial.ToPeriod);
             tblFincial.From_Date = Convert.ToDateTime(modelFincial.FromPeriod);
             //tblunit.Is_Active = true;
             //tblunit.Is_Deleted = false;
             //tblunit.Company_Id = 1;
             //  tblunit.Created_By = 1;
             // tblunit.Created_Date = DateTime.Now;
             tblFincial.Modified_Date = DateTime.Now;
             tblFincial.Modified_By = _userLog.LoginId;
             result = Save(tblFincial);
         }
         return result;

     }
     public List<DDLBind> DDLBind()
     {
         var lst = _db.FinancialYears.Where(m=>m.Company_Id==_userLog.CompanyId).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();
         return lst;
     }
     private bool Save(FinancialYear tblFincial)
     {
         bool result = false;
         try
         {
             bool res = _db.UnitMasters.Any(m => m.Company_Id == _userLog.CompanyId && (0 == tblFincial.Id || m.Id != tblFincial.Id) && (m.Name.Trim().Equals(tblFincial.Name.ToUpper().Trim())));
             if (res == false)
             {
                 if (tblFincial.Id == 0)
                 {
                     tblFincial.Id = _db.FinancialYears.AsNoTracking().Count()!=0 ? _db.FinancialYears.Max(x => x.Id) + 1 : 1;
                     _db.FinancialYears.Add(tblFincial);
                 }
                 _db.SaveChanges();
                 result = true;
             }
             return result;
         }
         catch
         {
             return result;
         }
     }
     public ViewModel.Common.FinancialYear Find(int Id)
     {
         ViewModel.Common.FinancialYear modelFincial = new ViewModel.Common.FinancialYear();
         if(Id!=0)
         {
             FinancialYear tblFincial = _db.FinancialYears.Find(Id);
             modelFincial.Id = tblFincial.Id;
              modelFincial.Name=tblFincial.Name;
              modelFincial.Remarks = tblFincial.Remarks;
              modelFincial.ToPeriod = tblFincial.To_Date.ToString("yyyy-MM-dd");
              modelFincial.FromPeriod = tblFincial.From_Date.ToString("yyyy-MM-dd");
            // return tblFincial
         }
         return modelFincial;
     }
     public bool Delete(int Id)
     {
         if (Id != 0)
         {
             FinancialYear tblFincial = _db.FinancialYears.Find(Id);
             tblFincial.Is_Deleted = true;
             tblFincial.Modified_By = _userLog.LoginId;
             tblFincial.Modified_Date = DateTime.Now;
             _db.SaveChanges();
             return true;
         }
         return false;
     }
     public bool IsNameExists(int Id, string Name)
     {
         return _db.FinancialYears.Any(m => m.Company_Id == _userLog.CompanyId && (0 == Id || m.Id != Id) && (m.Name.Trim().ToUpper().Equals(Name.ToUpper().Trim())));
     }
    }
}

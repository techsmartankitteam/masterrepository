﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using HDMEntity;
using ViewModel.Common;
using ViewModel.Users;

namespace DataLayer.Masters
{
  public  class UnitsDblayer
    {
      INVENTORY_DBEntities _db;
      private LoginInfo _userLog { get; set; }
      log4net.ILog log ;
      public UnitsDblayer()
      {
          _db = new INVENTORY_DBEntities();
          _userLog = new LoginInfo
          {
              CompanyId = 1,
              ConnectionString = _db.Database.Connection.ConnectionString,
              FinancialId = 1,
              FinancialYear = "2015-2016",
              LoginId = 1,
              LoginName = "Ankit Singh",
              SecurityToken = Guid.NewGuid().ToString(),
              UserSession = Guid.NewGuid().ToString()
          };
          log = log4net.LogManager.GetLogger(typeof(UnitsDblayer));
       
      }
       [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
      public UnitsDblayer(String ConnectionString)
      {
          _db = new INVENTORY_DBEntities();
          _db.Database.Connection.ConnectionString = ConnectionString;
          _userLog = new LoginInfo
          {
              CompanyId = 1,
              ConnectionString = ConnectionString,
              FinancialId = 1,
              FinancialYear = "2015-2016",
              LoginId = 1,
              LoginName = "Ankit Singh",
              SecurityToken = Guid.NewGuid().ToString(),
              UserSession = Guid.NewGuid().ToString()
          };
          log = log4net.LogManager.GetLogger(typeof(UnitsDblayer));
       
      }
      public UnitsDblayer(LoginInfo _log)
        {
            _db = new INVENTORY_DBEntities();
            _db.Database.Connection.ConnectionString = _log.ConnectionString;
            _userLog = _log;
            log = log4net.LogManager.GetLogger(typeof(UnitsDblayer));
       
        }
      public List<ViewModel.Common.Unit> List(string Search="")
      {
      try
      { 
          List<Unit> list = new List<Unit>();
          var data=_db.UnitMasters.Where(x=>x.Group_Id==0 && x.Is_Deleted==false && x.Company_Id==_userLog.CompanyId).ToList();
          data.ForEach(x => list.Add(new Unit { Id = x.Id, Name = x.Name, Remarks = x.Remarks, AliasName = x.Alias_Name, Unit_Volume = x.Unit_Volume, No_of_Decimal = x.No_of_Decimal ?? 0 }));
          //dat
          //    .Select(x => new Unit { Id = x.Id, Name = x.Name, Remarks = x.Remarks, AliasName = x.Alias_Name, Unit_Volume = x.Unit_Volume,No_of_Decimal=x.No_of_Decimal??0 }).ToList();
       
          if(!String.IsNullOrWhiteSpace(Search))
          {
              list = list.Where(m => m.Name.ToUpper().Trim().StartsWith(Search.ToUpper().Trim())).ToList();
          }
          return list;
          }
          catch (Exception ex)
          {
          log.Error("Namespace DataLayer.Masters.UnitDbLayer, Method \"List\" have error occur : Error message #  "+ex.Message);
             return null;
          }
      }
      public bool Create(ViewModel.Common.Unit modelunit)
      {

          
         
          bool result = false;
          try
          { 
          
          
          UnitMaster tblunit=new UnitMaster();
          

        //  tblunit.Id = modelunit.Id;
          tblunit.Name = modelunit.Name.Trim();
          tblunit.Remarks = modelunit.Remarks;
          tblunit.Alias_Name = modelunit.AliasName;
          tblunit.Unit_Volume = "0";
          tblunit.No_of_Decimal = modelunit.No_of_Decimal ;
          tblunit.Is_Active = true;
          tblunit.Is_Deleted = false;
          tblunit.Company_Id = _userLog.CompanyId;
          tblunit.Created_By = _userLog.LoginId;
          tblunit.Ratio1 = 1;
          tblunit.Ratio2 = 1;
          tblunit.Created_Date = DateTime.Now;
          tblunit.Modified_Date = DateTime.Now;
          tblunit.Modified_By = _userLog.LoginId;
          result = Save(tblunit);
          if (result == true && modelunit.SubUnitList.Count > 0 && modelunit.AddSubUnit == true)
          {
             
              result = CreateSubUnit(modelunit.SubUnitList, tblunit.Id);
          }
          return result;
          }
          catch(Exception ex)
          {
              log.Error("Namespace DataLayer.Masters.UnitDbLayer, Method \"Create\" have error occur : Error message #  " + ex.Message);
              return result;
          }

      }

      public bool CreateSubUnit(List<SubUnit> _sub,long UnitId=0)
      {
          bool result = false;

          try { 
          List<UnitMaster> lstunit=new List<UnitMaster>();
          UnitMaster tblunit ;
        
          long maxid = _db.UnitMasters.Count() != 0 ? _db.UnitMasters.Max(x => x.Id) + 1 : 1;
          if (_sub!=null &&  UnitId!=0 )
          { 
          foreach (var item in _sub)
          {
              tblunit = new UnitMaster();


              tblunit.Id = maxid;
              tblunit.Group_Id = UnitId;
              tblunit.Name = item.Name??string.Empty;
              tblunit.Remarks = "Subunit";
              tblunit.Alias_Name = item.Alias_Name;
              tblunit.Unit_Volume = "0";
              tblunit.No_of_Decimal = 0;
              tblunit.Ratio1 = Convert.ToDecimal(item.Ratio1) > 0 ? Convert.ToDecimal(item.Ratio1) : 1;
              tblunit.Ratio2 = Convert.ToDecimal(item.Ratio2) > 0 ? Convert.ToDecimal(item.Ratio2) : 1;
          tblunit.Is_Active = true;
          tblunit.Is_Deleted = false;
          tblunit.Company_Id = _userLog.CompanyId;
          tblunit.Created_By = _userLog.LoginId;
          tblunit.Created_Date = DateTime.Now;
          tblunit.Modified_Date = DateTime.Now;
          tblunit.Modified_By = _userLog.LoginId;
              lstunit.Add(tblunit);
              maxid++;
          }
          _db.UnitMasters.AddRange(lstunit);
          _db.SaveChanges();
          result = true;
          }
          return result;
          }
          catch (Exception ex)
          {
              log.Error("Namespace DataLayer.Masters.UnitDbLayer, Method \"Create Sub unit\" have error occur : Error message #  " + ex.Message, ex);
              return result;
          }

      }
            public bool Update(ViewModel.Common.Unit modelunit)
         {
          bool result = false;
          try { 
          if (modelunit.Id != 0)
          {
              UnitMaster tblunit = _db.UnitMasters.Find(modelunit.Id);


              tblunit.Name = modelunit.Name;
              tblunit.Remarks = modelunit.Remarks;
              tblunit.Alias_Name = modelunit.AliasName;
              tblunit.Unit_Volume = modelunit.Unit_Volume;
              tblunit.No_of_Decimal = modelunit.No_of_Decimal;
             
              tblunit.Modified_Date = DateTime.Now;
              tblunit.Modified_By = _userLog.LoginId;
              result = Save(tblunit);
              _db.Pr_DeleteCommonTblRcrd(tblunit.Id, "UNIT");
              if (result == true && modelunit.SubUnitList.Count > 0)
              {
                 
                  result = CreateSubUnit(modelunit.SubUnitList, tblunit.Id);
              }
          }
          return result;
          }
          catch (Exception ex)
          {
              log.Error("Namespace DataLayer.Masters.UnitDbLayer, Method \"Update\" have error occur : Error message #  " + ex.Message);
              return result;
          }

      }
      public List<DDLBind> DDLBind()
      {
      try
      { 
          var lst = _db.UnitMasters.Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();
          return lst;
      }
      catch (Exception ex)
      {
          log.Error("Namespace DataLayer.Masters.UnitDbLayer, Method \"DDLBind\" have error occur : Error message #  " + ex.Message);
          return null;
      }
      }
      public List<SubUnit> FindSubUnit(long Id)
      {
          try { 
          List<SubUnit> modelsubunit = new List<SubUnit>();
         
          if (Id != 0)
          {
              var lst = _db.UnitMasters.Where(x => x.Group_Id == Id && x.Is_Deleted==false && x.Company_Id==_userLog.CompanyId).ToList();
              lst.ForEach(x => modelsubunit.Add(new SubUnit
              {
                 Id=x.Id,
              Alias_Name=x.Alias_Name,
              Name=x.Name,
              Ratio1= String.Format("{0:#}",x.Ratio1)??"1",
                 Ratio2 = String.Format("{0:#}", x.Ratio2) ?? "1",
              }));
          }
          return modelsubunit;
          }
          catch (Exception ex)
          {
              log.Error("Namespace DataLayer.Masters.UnitDbLayer, Method \"FindSubUnit\" have error occur : Error message #  " + ex.Message);
              return null;
          }
      }
      public ViewModel.Common.Unit Find(int Id)
      {
          try { 

          ViewModel.Common.Unit modelunit = new Unit();
          if(Id!=0)
          {
             
              UnitMaster tblunit = _db.UnitMasters.Find(Id);
              if (tblunit != null)
              {
                  modelunit.Id = tblunit.Id;
                  modelunit.Name = tblunit.Name;
                  modelunit.Remarks = tblunit.Remarks;
                  modelunit.Unit_Volume = tblunit.Remarks;
                  modelunit.AliasName = tblunit.Alias_Name;
                  modelunit.No_of_Decimal = tblunit.No_of_Decimal ?? 0;
                  int cnt = _db.UnitMasters.Count(x => x.Company_Id==1 && x.Is_Deleted==false && x.Group_Id == Id);
                  modelunit.AddSubUnit = cnt > 0 ? true : false;
              }
              
          }
         return modelunit;
          }
          catch (Exception ex)
          {
              log.Error("Namespace DataLayer.Masters.UnitDbLayer, Method \"Find\" have error occur : Error message #  " + ex.Message);
              return null;
          }
      }
      public bool Delete(int Id)
      {
          try { 
          if (Id != 0)
          {
              UnitMaster tblunit = _db.UnitMasters.Find(Id);
              tblunit.Is_Deleted = true;
              tblunit.Modified_By = _userLog.LoginId;
              tblunit.Modified_Date = DateTime.Now;
              _db.SaveChanges();
              return true;
          }
          return false;
          }
          catch (Exception ex)
          {
              log.Error("Namespace DataLayer.Masters.UnitDbLayer, Method \"Delete\" have error occur : Error message #  " + ex.Message);
              return false;
          }
      }
      private bool Save(UnitMaster units)
      {
          bool result = false;
          try
          {
              bool res = _db.UnitMasters.AsNoTracking().Any(m => m.Company_Id == _userLog.CompanyId && (0 == units.Id || m.Id != units.Id) && (m.Name.Trim().Equals(units.Name.ToUpper().Trim())));
              if (res == false )

              {
                  if (units.Id == 0)
                  {
                      units.Id=_db.UnitMasters.AsNoTracking().Count()!=0?_db.UnitMasters.AsNoTracking().Max(x=>x.Id)+1:1;
                      _db.UnitMasters.Add(units);
                  }
                  _db.SaveChanges();
                  result = true;
              }
              return result;
          }
         
          catch(Exception ex) {
              log.Error("Namespace DataLayer.Masters.UnitDbLayer, Method \"Save\" have error occur : Error message #  " + ex.Message);
          return result;
           }
      }
      public bool IsNameExists(int Id, string Name)
      {
      try
      { 
          return _db.UnitMasters.AsNoTracking().Any(m => m.Company_Id == _userLog.CompanyId && (0 == Id || m.Id != Id) && (m.Name.Trim().ToUpper().Equals(Name.ToUpper().Trim())) && m.Is_Deleted==false);
      }
      catch (Exception ex)
      {
          log.Error("Namespace DataLayer.Masters.UnitDbLayer, Method \"IsNameExists\" have error occur : Error message #  " + ex.Message);
          return true;
      }
      }

      
    }
}

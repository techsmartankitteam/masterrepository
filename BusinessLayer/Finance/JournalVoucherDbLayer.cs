﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ViewModel.Common;
using ViewModel.Users;
using ViewModel.Finance;
using DataLayer.Masters;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Data;

namespace DataLayer.Finance
{
    public class JournalVoucherDbLayer : FinancialYearDbLayer
    {
        INVENTORY_DBEntities _db;
        public long Journal_VC_No;
        private LoginInfo _userLog { get; set; }
        public JournalVoucherDbLayer()
            : base()
        {
            _db = new INVENTORY_DBEntities();
            _userLog = new LoginInfo
            {
                CompanyId = 1,
                ConnectionString = _db.Database.Connection.ConnectionString,
                FinancialId = 1,
                FinancialYear = "2015-2016",
                LoginId = 1,
                LoginName = "Ankit Singh",
                SecurityToken = Guid.NewGuid().ToString(),
                UserSession = Guid.NewGuid().ToString()
            };

        }
        public JournalVoucherDbLayer(LoginInfo _log)
            : base(_log)
        {
            _db = new INVENTORY_DBEntities();
            _db.Database.Connection.ConnectionString = _log.ConnectionString;
            _userLog = _log;
        }

        public bool Create(JournalVoucherInfo ModelJournalVC)
        {

            bool result = false;
            try
            {
                if (ModelJournalVC != null)
                {
                    JournalEntry tblJournal = new JournalEntry();
                    tblJournal.Date = ModelJournalVC.Date.Value;
                    tblJournal.Remarks = ModelJournalVC.Remarks;
                    tblJournal.ValueDate = ModelJournalVC.ValueDate.Value;
                    tblJournal.Journal_VC_No = ModelJournalVC.Journal_VC_No;
                    tblJournal.Modified_By = _userLog.LoginId;
                    tblJournal.Modified_Date = DateTime.Now;
                    tblJournal.Created_By = _userLog.LoginId;
                    tblJournal.Created_Date = DateTime.Now;
                    tblJournal.Company_Id = _userLog.CompanyId;
                    tblJournal.Is_Active = true;
                    tblJournal.Is_Active = false;
                    result = Save(tblJournal);

                    if (result == true)
                    {
                        Update_Journal_Tra(ModelJournalVC, Journal_VC_No);
                    }

                }

            }
            catch
            {

                result = false;
            }
            return result;

        }
        public bool Update(JournalVoucherInfo ModelJournalVC)
        {

            bool result = false;
            try
            {
                if (ModelJournalVC != null)
                {
                    JournalEntry tblJournal = _db.JournalEntries.Find(ModelJournalVC.Id);
                    if (tblJournal != null)
                    {
                        tblJournal.Date = ModelJournalVC.Date.Value;
                        tblJournal.Remarks = ModelJournalVC.Remarks;
                        tblJournal.ValueDate = ModelJournalVC.ValueDate.Value;
                        tblJournal.Journal_VC_No = ModelJournalVC.Journal_VC_No;
                        tblJournal.Modified_By = _userLog.LoginId;
                        tblJournal.Modified_Date = DateTime.Now;

                        result = Save(tblJournal);

                        if (result == true)
                        {
                            Update_Journal_Tra(ModelJournalVC, Journal_VC_No);
                        }
                    }

                }

            }
            catch
            {

                result = false;
            }
            return result;

        }
        public String GEN_JournalVC_No()
        {
            string Challan_No = "";

            int countRows = _db.JournalEntries.Where(x => x.Company_Id == _userLog.CompanyId).Count();
            if (countRows != 0)
                Challan_No = _db.JournalEntries.OrderBy(x => x.Id).Skip(countRows - 1).FirstOrDefault().Journal_VC_No;
            if (!string.IsNullOrWhiteSpace(Challan_No))
            {
                Challan_No = Regex.Replace(Challan_No, @"\d+(?=\D*$)",
                   m => (Convert.ToInt64(m.Value) + 1).ToString().PadLeft(5, '0'));
            }
            else
            {
                Challan_No = "1".PadLeft(5, '0');
            }
            return Challan_No;
        }

        private bool Save(JournalEntry tblJournalVC)
        {
            bool result = false;
            try
            {
                var res = false;
                res = _db.JournalEntries.Any(m => m.Company_Id == _userLog.CompanyId && (0 == tblJournalVC.Id || m.Id != tblJournalVC.Id) && (m.Journal_VC_No.Trim().ToUpper().Equals(tblJournalVC.Journal_VC_No.ToUpper().Trim())));
                if (res == false)
                {
                    if (tblJournalVC.Id == 0)
                    {
                        tblJournalVC.Id = _db.JournalEntries.AsNoTracking().Count() != 0 ? _db.JournalEntries.AsNoTracking().Max(x => x.Id) + 1 : 1;

                        _db.JournalEntries.Add(tblJournalVC);
                    }
                    _db.SaveChanges();
                    result = true;
                    Journal_VC_No = tblJournalVC.Id;
                }
                return result;
            }
            catch
            {
                return result;
            }
        }

        public List<CrDrList> List(string search = "")
        {
            List<CrDrList> list = new List<CrDrList>();
            var lst = _db.sp_GetJournalList().Where(m=>m.Journal_VC_Id.ToString().Trim().StartsWith(search.Trim())).ToList();
            //var lst = (from JE in _db.JournalEntries
            //           join JET in _db.JournalEntryTras on JE.Id equals JET.Journal_VC_Id
            //           group JET.Journal_VC_Id by
            //           new
            //           {
            //               JET.Journal_VC_Id,
            //               JE.Id,
            //               JET.Amount
            //           } into grouped
            //           select new
            //           {
            //               Journal_id = grouped.Key.Id,
            //               Journal_VC = grouped.Key.Journal_VC_Id,
            //               Amount = grouped.Sum(x => grouped.Key.Amount)
            //           }).ToList();
            //var lst = (from JE in _db.JournalEntries
            //           join JET in _db.JournalEntryTras on JE.Id equals JET.Journal_VC_Id into g
            //           select new
            //           {
            //               JE.Id,
            //               Amount = g.Sum(x => (decimal?)x.Amount),
            //               Type = g.Select(x => x.Behaviour).FirstOrDefault()
            //           }).ToList();

            lst.ForEach(x => list.Add(new CrDrList
            {
                Id = x.Journal_VC_Id,
                Journal_VC_Id = x.Journal_VC_Id,
                Date = Convert.ToDateTime(x.Date).ToShortDateString(),
                CRAmount = x.CR ?? 0.0m,
                DRAmount = x.DR ?? 0.0m,
                Particulars = x.Particulars
            }));
            return list;
        }

        private bool Update_Journal_Tra(ViewModel.Finance.JournalVoucherInfo ModelCrDrTra, long Journal_VC_Id)
        {
            System.Data.DataTable tbl = new System.Data.DataTable("Product");
            System.Data.DataColumn[] columns = new System.Data.DataColumn[7];
            columns[0] = new System.Data.DataColumn("Company_Id", typeof(long));
            columns[1] = new System.Data.DataColumn("Journal_VC_Id", typeof(long));
            columns[2] = new System.Data.DataColumn("Statementref", typeof(string));
            columns[3] = new System.Data.DataColumn("CrLedgerId", typeof(long));
            columns[4] = new System.Data.DataColumn("DrLedgerId", typeof(long));
            columns[5] = new System.Data.DataColumn("Amount", typeof(decimal));
            columns[6] = new System.Data.DataColumn("Behaviour", typeof(string));

            tbl.Columns.AddRange(columns);
            foreach (var item in ModelCrDrTra.CreditDebitList)
            {
                var row = tbl.NewRow();

                row["Company_Id"] = 1;
                row["Journal_VC_Id"] = Journal_VC_Id;
                row["Statementref"] = "Default";
                if (ModelCrDrTra.Type == "1")
                {
                    row["CrLedgerId"] = ModelCrDrTra.MstLedger_Id;
                    row["DrLedgerId"] = item.CrLedgerId;
                    row["Behaviour"] = "CR";
                }
                else
                {
                    row["CrLedgerId"] = item.CrLedgerId;
                    row["DrLedgerId"] = ModelCrDrTra.MstLedger_Id;
                    row["Behaviour"] = "DR";
                }
                row["Amount"] = item.Amount;

                tbl.Rows.Add(row);
            }
            DatabaseConfiguration.DbConfiguration.InitializeConnection(_userLog.ConnectionString);
            var builder = new EntityConnectionStringBuilder(DatabaseConfiguration.DbConfiguration.ConnectionString);
            var regularConnectionString = builder.ProviderConnectionString;
            using (SqlConnection connection = new SqlConnection(regularConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("", connection);
                command.CommandText = "sp_UpdateJournaltra";
                command.Parameters.Clear();
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@datatable", tbl).SqlDbType = SqlDbType.Structured;
                command.Parameters.AddWithValue("@Journal_VC_Id", Journal_VC_Id).SqlDbType = SqlDbType.BigInt;
                command.ExecuteNonQuery();
                connection.Close();
                bool result = true;

                return result;
            };
        }

        public JournalVoucherInfo Find(long Id)
        {
            JournalVoucherInfo modelJournalTra = new JournalVoucherInfo();

            try
            {
                JournalVoucherInfo modelJournal = new JournalVoucherInfo();
                JournalEntry tblJournal = Id > 0 ? _db.JournalEntries.Find(Id) : Id < 0 ? _db.JournalEntries.OrderByDescending(x => x.Date).FirstOrDefault() : null;

                if (tblJournal != null)
                {
                    modelJournal.Id = tblJournal.Id;
                    modelJournal.Journal_VC_No = tblJournal.Journal_VC_No;
                    modelJournal.Remarks = tblJournal.Remarks;
                    modelJournal.ValueDate = tblJournal.ValueDate;

                    if (tblJournal.Id != null && tblJournal.Id > 0)
                    {
                        //JournalEntryTra tblJournalEntryTra = _db.JournalEntryTras.Find(tblJournal.Id);
                        var list = _db.JournalEntryTras.Where(x => x.Journal_VC_Id == tblJournal.Id).ToList();
                        if (list != null)
                        {
                            List<ViewModel.Finance.CreditDebitList> lst = new List<CreditDebitList>();

                            modelJournal.Type = list[0].Behaviour;
                            if (list[0].Behaviour == "CR")
                            {
                                modelJournal.MstLedger_Id = list[0].CrLedgerId;
                            }
                            else
                            {
                                modelJournal.MstLedger_Id = list[0].DrLedgerId;
                            }
                            list.ForEach(x => lst.Add(new CreditDebitList
                            {
                                Id = x.Id,
                                CrLedgerId = x.CrLedgerId,
                                DrLedgerId = x.DrLedgerId,
                                Amount = x.Amount
                            }));
                            modelJournal.CreditDebitList = lst;
                        }

                    }
                }
                return modelJournal;
            }
            catch
            {

                return null;
            }
            // return modelJournalTra;
        }
    }
}

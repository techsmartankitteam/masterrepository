﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HDMEntity;
using ViewModel.Common;
using ViewModel.Users;
namespace DataLayer.Common
{
    public  class CommonDbLayer
    {
        #region  Property 
        INVENTORY_DBEntities _db;
        private ViewModel.Users.LoginInfo _userLog { get; set; }
           #endregion
        #region Constructor
        public CommonDbLayer()
        {
            _db = new INVENTORY_DBEntities();
            _userLog = new LoginInfo
            {
                CompanyId = 1,
                ConnectionString = "",
                FinancialId = 1,
                FinancialYear = "2015-2016",
                LoginId = 1,
                LoginName = "Ankit Singh",
                SecurityToken = Guid.NewGuid().ToString(),
                UserSession = Guid.NewGuid().ToString()
            };
        
        }
          [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
        public CommonDbLayer(String ConnectionString)
        {  
            _db = new INVENTORY_DBEntities();
              _db.Database.Connection.ConnectionString = ConnectionString;
              _userLog = new LoginInfo
              {
                  CompanyId = 1,
                  ConnectionString = ConnectionString,
                  FinancialId = 1,
                  FinancialYear = "2015-2016",
                  LoginId = 1,
                  LoginName = "Ankit Singh",
                  SecurityToken = Guid.NewGuid().ToString(),
                  UserSession = Guid.NewGuid().ToString()
              };
        }
        public CommonDbLayer(ViewModel.Users.LoginInfo Log)
        {
            this._userLog = Log;
            _db = new INVENTORY_DBEntities();
            _db.Database.Connection.ConnectionString =Log.ConnectionString;

        }
        #endregion
        public string UnitNameByItemId(long ItemId)
        {
            string name = string.Empty;
            if (ItemId != 0)
            {
                var item = _db.Stock_Item.SingleOrDefault(x => x.Id == ItemId && x.Company_Id==_userLog.CompanyId );
                name = _db.UnitMasters.SingleOrDefault(x => x.Id == item.Unit_Id && x.Company_Id == _userLog.CompanyId).Name ?? "";
            }
            return name;
        }
        public IEnumerable<ViewModel.Common.DDLBind> ListCityDDL(long ParentId = 0)
        {

            return _db.City_Master.AsNoTracking().AsParallel()
                 .Where(x => x.Company_Id == _userLog.CompanyId && (0 == ParentId || x.State_Id == ParentId) && x.Is_Active == true && x.Is_Deleted==false).OrderBy(m=>m.Name)
                 .Select(x => new ViewModel.Common.DDLBind { Id = x.Id, Name = x.Name });


        }

        public IEnumerable<ViewModel.Common.DDLBind> DDLCountryState(long Tag_Id, long ParentId = 0)
        {


            //  List<ViewModel.Master.CountryStateCityInfo> lst = new List<CountryStateCityInfo>();
            return _db.Country_State.AsNoTracking()
                .Where(m => m.Tag_Id == Tag_Id && (0 == ParentId || m.Parent_Id == ParentId) && m.Is_Visible == true)
                .AsParallel().OrderBy(m=>m.Name)
                .Select(x => new ViewModel.Common.DDLBind { Id = x.Id, Name = x.Name }).AsParallel();

        }
        public List<ViewModel.Common.DDLBind> DDLBind(string TableName, string searchText = "")
        {
            List<DDLBind> list = new List<DDLBind>();
            System.Globalization.CultureInfo _culture = new System.Globalization.CultureInfo("en-US");
            if (string.Compare(TableName.Trim(), "Ledger_Group", true, _culture) == 0)
            {
              //  list = String.IsNullOrWhiteSpace(searchText) != true ? _db.Ledger_Group.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId && x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList() : _db.Ledger_Group.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();
                list = _db.Ledger_Group.AsNoTracking()
               .Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId && (searchText.Trim().Length==0 || x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())))
              .OrderBy(m=>m.Name)
               .Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();
            }
            else if (string.Compare(TableName.Trim(), "Stock_Group", true, _culture) == 0)
            {
                list = _db.Stock_Group.AsNoTracking()
                 .Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId && (searchText.Trim().Length==0 || x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())))
                .OrderBy(m => m.Name)
                 .Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();
             //   list = String.IsNullOrWhiteSpace(searchText) != true ? _db.Stock_Group.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId && x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList() : _db.Stock_Group.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();

            }
            else if (string.Compare(TableName.Trim(), "Stock_Category", true, _culture) == 0)
            {
                //list = String.IsNullOrWhiteSpace(searchText) != true ? _db.Stock_Category.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId && x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList() : _db.Stock_Category.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();
                list = _db.Stock_Category.AsNoTracking()
               .Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId && (searchText.Trim().Length==0 || x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())))
               .OrderBy(m => m.Name)
               .Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();

            }
            else if (string.Compare(TableName.Trim(), "UnitMaster", true, _culture) == 0)
            {
               // list = String.IsNullOrWhiteSpace(searchText) != true ? _db.UnitMasters.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId && x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList() : _db.UnitMasters.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();
                list = _db.UnitMasters.AsNoTracking()
                        .Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId && (searchText.Trim().Length==0 || x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())))
                        .OrderBy(m => m.Name)
                        .Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();

            }
            else if (string.Compare(TableName.Trim(), "GoDown_Master", true, _culture) == 0)
            {
               // list = String.IsNullOrWhiteSpace(searchText) != true ? _db.GoDown_Master.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId && x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList() : _db.GoDown_Master.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();
                list = _db.GoDown_Master.AsNoTracking()
                        .Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId && (searchText.Trim().Length==0 || x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())))
                        .OrderBy(m => m.Name)
                        .Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();

            }
            else if (string.Compare(TableName.Trim(), "GoDown_Region", true, _culture) == 0)
            {
               // list = String.IsNullOrWhiteSpace(searchText) != true ? _db.GoDown_Region.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId && x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList() : _db.GoDown_Region.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();
                list = _db.GoDown_Region.AsNoTracking()
                        .Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId && (searchText.Trim().Length==0 || x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())))
                        .OrderBy(m => m.Name)
                        .Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();

            }
            else if (string.Compare(TableName.Trim(), "Stock_Item", true, _culture) == 0)
            {
                //list = String.IsNullOrWhiteSpace(searchText) != true ? _db.Stock_Item.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId && x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList() : _db.Stock_Item.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();
                list = _db.Stock_Item.AsNoTracking()
                        .Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == _userLog.CompanyId && (searchText.Trim().Length==0 || x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())))
                        .OrderBy(m => m.Name)
                        .Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();

            }
            else if (string.Compare(TableName.Trim(), "Customer", true, _culture) == 0)
            {
                //list = String.IsNullOrWhiteSpace(searchText) != true ? _db.Ledger_Master.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == 1 && x.Group_Id == 28 && x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList() : _db.Ledger_Master.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == 1 && x.Group_Id == 28).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();
                list = String.IsNullOrWhiteSpace(searchText) != true ? _db.Pr_BindLeder(_userLog.FinancialId).Where(x => x.Group_Name.ToUpper().StartsWith(searchText.Trim().ToUpper()) && x.Group_Name.ToLower() == "sundry creditors").OrderBy(m => m.Name).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList() : _db.Pr_BindLeder(_userLog.FinancialId).Where(x => x.Group_Name.ToLower() == "sundry debtors").OrderBy(m => m.Name).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();

            }
            else if (string.Compare(TableName.Trim(), "Supplier", true, _culture) == 0)
            {
                //list = String.IsNullOrWhiteSpace(searchText) != true ? _db.Ledger_Master.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == 1 && x.Group_Id == 27 && x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList() : _db.Ledger_Master.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == 1 && x.Group_Id == 27).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();
                list = String.IsNullOrWhiteSpace(searchText) != true ? _db.Pr_BindLeder(_userLog.FinancialId).Where(x => x.Group_Name.ToUpper().StartsWith(searchText.Trim().ToUpper()) && x.Group_Name.ToLower() == "sundry creditors").OrderBy(m => m.Name).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList() : _db.Pr_BindLeder(_userLog.FinancialId).Where(x => x.Group_Name.ToLower() == "sundry creditors").OrderBy(m => m.Name).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();

            }
            else if (string.Compare(TableName.Trim(), "CR", true, _culture) == 0)
            {

                list = String.IsNullOrWhiteSpace(searchText) != true ? _db.Pr_BindLeder(_userLog.FinancialId).Where(x => x.Group_Name.ToUpper().StartsWith(searchText.Trim().ToUpper()) && x.Nature.ToUpper() == "CR").OrderBy(m => m.Name).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList() : _db.Pr_BindLeder(_userLog.FinancialId).Where(x => x.Nature.ToUpper() == "CR").OrderBy(m => m.Name).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();

            }
            else if (string.Compare(TableName.Trim(), "DR", true, _culture) == 0)
            {

                list = String.IsNullOrWhiteSpace(searchText) != true ? _db.Pr_BindLeder(_userLog.FinancialId).Where(x => x.Group_Name.ToUpper().StartsWith(searchText.Trim().ToUpper()) && x.Nature.ToUpper() == "DR").OrderBy(m => m.Name).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList() : _db.Pr_BindLeder(_userLog.FinancialId).Where(x => x.Nature.ToUpper() == "DR").OrderBy(m => m.Name).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();

            }
            else if (string.Compare(TableName.Trim(), "ALL", true, _culture) == 0)
            {

                list = String.IsNullOrWhiteSpace(searchText) != true ? _db.Pr_BindLeder(_userLog.FinancialId).Where(x => x.Group_Name.ToUpper().StartsWith(searchText.Trim().ToUpper())).OrderBy(m => m.Name).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList() : _db.Pr_BindLeder(_userLog.FinancialId).OrderBy(m => m.Name).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();

            }
            else if (string.Compare(TableName.Trim(), "Employee", true, _culture) == 0)
            {
                //  list = String.IsNullOrWhiteSpace(searchText) != true ? _db.Stock_Item.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == 1 && x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList() : _db.Stock_Item.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == 1).Select(x => new DDLBind { Id = x.Id, Name = x.Name }).ToList();
                list.Add(new DDLBind { Id = 1, Name = "Ajay" });
                list.Add(new DDLBind { Id = 2, Name = "Vijay" });
                list.Add(new DDLBind { Id = 3, Name = "Sanjay" });

            }
            else if (string.Compare(TableName.Trim(), "Tax_Deduction_Master", true, _culture) == 0)
            {
                // list = String.IsNullOrWhiteSpace(searchText) != true ? _db.Tax_Deduction_Master.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == 1 && x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper())).Select(x => new DDLBind { Id = x.Id, Name = x.Name + "(" + x.Rate + "%)" }).ToList() : _db.Tax_Deduction_Master.Where(x => x.Is_Deleted == false && x.Is_Active == true && x.Company_Id == 1).Select(x => new DDLBind { Id = x.Id, Name = x.Name + "(" + x.Rate + "%)" }).ToList();
                list = _db.Tax_Deduction_Master.Where(x => x.Is_Active == true && x.Is_Deleted == false && x.Company_Id == _userLog.CompanyId && ("" == searchText || x.Name.ToUpper().StartsWith(searchText.Trim().ToUpper()))).OrderBy(m => m.Name).Select(t => new DDLBind { Value = t.Id + "_" + t.Type + "_" + t.Rate_Type + "_" + t.Rate, Name = t.Name }).ToList();

            }
            return list;
        }
    }
}

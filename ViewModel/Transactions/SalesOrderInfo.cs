﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViewModel.Common;
namespace ViewModel.Transactions
{
   public class SalesOrderInfo
    {
       public SalesOrderInfo()
       {
           OrderDetails = new List<SalesOrderInfo_Tra>();
           ItemList = new List<DDLBind>();
           SelsPersonList = new List<DDLBind>();
           CustomerList = new List<DDLBind>();
       }
        public long Id { get; set; }

        public long Customer_Id { get; set; }
        public string Order_No { get; set; }
        public long Sales_Person { get; set; }
        //public long Quation_Id { get; set; }
        public string Customer_Name { get; set; }
        public string Email { get; set; }
        public string Contact_No { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public System.DateTime Posting_Date { get; set; }
        public System.DateTime Order_Date { get; set; }
        public System.DateTime Document_Date { get; set; }
        public System.DateTime Request_Delivery_Date { get; set; }
        public string External_DocumentNo { get; set; }
        public string Remarks { get; set; }
        public bool Order_Status { get; set; }
        public string Refered_Type { get; set; }
        public long Refered_Id { get; set; }
        public bool Is_Refered { get; set; }
        public bool Is_DeliveryNote { get; set; }
        public virtual List<SalesOrderInfo_Tra> OrderDetails { get; set; }
        public virtual List<DDLBind> ItemList { get; set; }
        public virtual List<DDLBind> SelsPersonList { get; set; }
        public virtual List<DDLBind> CustomerList { get; set; }
        public string Image_Path { get; set; }
    }
   public class SalesOrdeList
   {
       public long Id { get; set; }
       public long Customer_Id { get; set; }
       public string Customer_Name { get; set; }
       public string Address { get; set; }
       public string Contact_No { get; set; }
       public string Email { get; set; }
       public string Order_No { get; set; }
       public long Sales_Person { get; set; }
       public string Sales_Person_Name { get; set; }
       public Nullable<decimal> Amount { get; set; }
       public string Order_Date { get; set; }
       public string Request_Delivery_Date { get; set; }
       public string Remarks { get; set; }
       public string Status { get; set; }

   }

   public class SalesOrderInfo_Tra

   {
       private decimal _OrderQty { get; set; }
       private decimal _Rate { get; set; }
       private decimal _Amount { get; set; }
      
       public SalesOrderInfo_Tra()
       {
           Status = true;
       }
       public long Id { get; set; }
       public string Order_No { get; set; }
       public long Item_Id { get; set; }
       public string Item_Name { get; set; }

       public Nullable<decimal> Ship_Quantity { get; set; }
       public Nullable<decimal> Order_Quantity { 
          get
       {
           return _OrderQty;
       }
           set
           {
               _OrderQty = value ?? 0;
           }
         }
       public Nullable<decimal> Invoice_Quantity { get; set; }
       public Nullable<decimal> Rate {
           
           get{

               return _Rate;
           } set{

               _Rate = value ?? 0;    
           }
       }
       public Nullable<decimal> Amount { 
           
                      get{
                          return _Amount;
                      }
           set {
               _Amount = value ?? 0;
           }
       }
       public string Unit_Name { get; set; }
       public int Decimal_Place { get; set; }
       public string Remarks { get; set; }
       public bool Status { get; set; }
     
   }
}

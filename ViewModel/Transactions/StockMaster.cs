﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ViewModel.Transactions
{
    public class StockMaster
    {
        public StockMaster()
        {
            StockMstDetails = new List<StockMasterDetail>();
        }
        public long Id { get; set; }
        public string Branch_Name { get; set; }
        public long Branch_Id { get; set; }
        public String Item_Code { get; set; }
        public string Name { get; set; }
        public long Item_Id { get; set; }
        public string Alias_Name { get; set; }
        public string GroupName { set; get; }
        public string CategoryName { set; get; }
        public string UnitName { set; get; }
        public string Qty { get; set; }
        public string Rate { get; set; }
        public string Selling_Rate { get; set; }

        public List<StockMasterDetail> StockMstDetails { set; get; }
    }
    public class StockMasterDetail
    {
        public long Id { get; set; }
        public string TagName { get; set; }
        public decimal Value { get; set; }
        public string Description { get; set; }
    }

    public class StockMasterItemDetail
    {
        public int ItemCode { get; set; }
        public int MinQunatity { get; set; }
        public int AvailableQuantity { get; set; }
        public string Status { get; set; }
        public string ImageUrl { get; set; }
    }
    public enum StockTag
    {
        All = 1,
        Repurchase = 2,
        OutOfStock = 3
    };
}

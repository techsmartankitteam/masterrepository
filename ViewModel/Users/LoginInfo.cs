﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ViewModel.Users
{
    public class LoginInfo
    {
        public long LoginId { get; set; }
        public String LoginName { get; set; }
        public String SecurityToken { get; set; }
        public String ConnectionString { get; set; }
        public long CompanyId { get; set; }
        public long FinancialId { get; set; }
        public String FinancialYear { get; set; }
        public String UserSession { get; set; }
    }
}

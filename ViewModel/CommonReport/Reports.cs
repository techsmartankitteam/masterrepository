﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ViewModel.CommonReport
{
    public class Reports
    {
    }
    public class UnitReport
    {
        public String Name { get; set; }
        public String AliasName { get; set; }
        public string Unit_Volume { get; set; }
        public String Remarks { get; set; }
    }
    public class Financial_Year
    {
        public String Name { get; set; }
        public string Remarks { get; set; }
        public DateTime FromPeriod { get; set; }
        public DateTime ToPeriod
        {
            get;
            set;
        }

    }
    public class TaxMaster
    {
        public long Id { get; set; }
        public long Company_Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Rate { get; set; }
      
    }
    public class Supplier_CustomerInfo
    {
       
        public long Id { get; set; }
        public String Title { get; set; }
        public String Name { get; set; }
        public string Code { get; set; }
        public string Alias_Name { get; set; }

        public long Group_Id { get; set; }

        public decimal Opening_Balance { get; set; }
        public bool MaintainRecord_BillbyBill { get; set; }
        public int CreditPeriodTime { get; set; }
        public string GroupName { set; get; }

        public String PANNumber { get; set; }
        public String AccountNumber { get; set; }
        public String Bank_Name { get; set; }
        public String IFSC_Code { get; set; }
        public String BranchName { get; set; }

        public String address { get; set; }
        public string CountryName { set; get; }
        public string StateName { set; get; }
        public string CityName { set; get; }
        public String Pin_Code { get; set; }
        //public Contact ContactInfo { get; set; }
       

        public String Phone_Number { get; set; }
        public String Mobile { get; set; }
        public String Email_Id { get; set; }

    }
    public class LedgerGroupCount
    {
        public string  Name { set; get; }
        public long Count { set; get; }
    }
   
    public class DayBookInfo
    {
        public long VC_Id { get; set; }
        public string VC_NO { get; set; }
        public string Name { get; set; }
        public string CR { get; set; }
        public string DR { get; set; }
        public decimal Amount { get; set; }
        public string Module { get; set; }
        public System.DateTime EntryOn { get; set; }

    }

    public class InventoryBookInfo
    {
        public DateTime Date_On { get; set; }
        public long Item_Id { get; set; }
        public string Item_Name { get; set; }
        public string Unit_Name { get; set; }
        public int Decimal_Place { get; set; }
        public decimal OpenQty { get; set; }
        public decimal ClosingQty { get; set; }
        public decimal Rate { get; set; }
    }
    public class OutStandingInfo
    {
        public long VC_Id { get; set; }
        public string VC_No { get; set; }
        public DateTime VC_Date { get; set; }
        public long Ledger_Id { get; set; }
        public string Ledger_Name { get; set; }
        public decimal   Bill_Amount { get; set; }
        public decimal Paid_Amount { get; set; }
    }

    public class OrdersInfo
    {
        public long VC_Id { get; set; }
        public long Ledger_Id { get; set; }
        public string Ledger_Name { get; set; }
        public string VC_No { get; set; }
        public System.DateTime VC_Date { get; set; }
        public long Sales_Person_Id { get; set; }
        public string Refered_Type { get; set; }
        public decimal Amount { get; set; }
        public bool Status { get; set; }
        public long Item_Id { get; set; }
        public string Item_Name { get; set; }
        public string Unit_Name { get; set; }
        public int Decimal_Place { get; set; }
        public decimal Qty { get; set; }
        public decimal Rate { get; set; }
    }
    public class AccountsBookInfo
    {
        public long VC_Id { get; set; }
        public string VC_No { get; set; }
        public long Ledger_Id { get; set; }
        public string Ledger_Name { get; set; }
        public System.DateTime VC_Date { get; set; }
        public string Refered_Type { get; set; }
        public decimal Amount { get; set; }
        public long Item_Id { get; set; }
        public string Item_Name { get; set; }
        public string Unit_Name { get; set; }
        public int Decimal_Place { get; set; }
        public decimal Qty { get; set; }
        public decimal Rate { get; set; }
        public string Serial_No { get; set; }
        public bool Is_Return { get; set; }
        public bool Is_Auto_Serial { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViewModel.Common;
namespace ViewModel.Finance
{

    public class JournalVoucherInfo
    {

        private DateTime _ValueDate { get; set; }
        private DateTime _Date { get; set; }
        public JournalVoucherInfo()
        {
            CrDrList = new List<CrDrList>();
            CustomerList = new List<DDLBind>();
            CreditDebitList = new List<CreditDebitList>();
            Date = DateTime.Now;
            ValueDate = DateTime.Now;
        }
        public long Id { get; set; }
        public string Type { get; set; }
        public long MstLedger_Id { get; set; }
        public long Credit_Id { get; set; }
        public long Debit_Id { get; set; }
        public System.DateTime? ValueDate
        {
            get
            {
                return _ValueDate;
            }
            set
            {
                _ValueDate = value ?? DateTime.Now;
            }
        }
        public System.DateTime? Date
        {
            get
            {
                return _Date;
            }
            set
            {
                _Date = value ?? DateTime.Now;
            }
        }
        public string Journal_VC_No { get; set; }
        public string Remarks { get; set; }

        public virtual List<DDLBind> CustomerList { get; set; }
        public virtual List<CrDrList> CrDrList { get; set; }
        public virtual List<CreditDebitList> CreditDebitList { get; set; }
    }
    public class JournalVoucherInfo_Tra
    {
        public long Id { get; set; }
        public long Company_Id { get; set; }
        public string Journal_VC_Id { get; set; }
        public string Statementref { get; set; }
        public long CrLedgerId { get; set; }
        public long DrLedgerId { get; set; }
        public decimal Amount { get; set; }
        public string Behaviour { get; set; }
    }
    public class CreditDebitList
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public long CrLedgerId { get; set; }
        public long DrLedgerId { get; set; }
        public decimal Amount { get; set; }

    }
    public class CrDrList
    {
        public long Id { get; set; }
        public long Journal_VC_Id { get; set; }
        public decimal CRAmount { get; set; }
        public decimal DRAmount { get; set; }
        public string Date { get; set; }
        public string Particulars { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ViewModel.NewsAndUpdates
{
  public  class Updates
    {
        public String UniqueId { get; set; }
      public String WebAddress { get; set; }
        public String ImageUrl { get; set; }
        public String Heading { get; set; }
        public String Message { get; set; }
        public string Icon { get; set; }
        public DateTime ReleaseOn { get; set; }

    }
}

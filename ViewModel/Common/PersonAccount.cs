﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace ViewModel.Common
{
   public class PersonAccountDetails
    {
       [Required(ErrorMessage = "Please Enter Pan No.")]
       [Display(Name = "Pan No.")]
        public String PANNumber { get; set; }
        public String AccountNumber { get; set; }
        public String Bank_Name { get; set; }
        public String IFSC_Code { get; set; }
        public String BranchName { get; set; }

    }
}

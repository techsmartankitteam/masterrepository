﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace ViewModel.Common
{
    public class FinancialYear
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Enter name")]
        [RegularExpression("[a-zA-Z ]*$", ErrorMessage = "Use letters only please")]
        [StringLength(50, ErrorMessage = "Name should not less Than 3 characters", MinimumLength = 3)]
        [Remote("IsNameExists", "FinancialYear", "Masters", AdditionalFields = "Id,Name", ErrorMessage = "Name Already Exists.")]
        public String Name { get; set; }
        public string Remarks { get; set; }
        [Required(ErrorMessage = "Select From-Period")]
        public string FromPeriod { get; set; }
        [Required(ErrorMessage = "Select To-Period")]
        public string ToPeriod { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace ViewModel.Ledger
{
    public class Supplier
    {
        public Supplier()
        {
            Address = new Common.Address();
            Account_Detail = new Common.PersonAccountDetails();
        }
        public long Id { get; set; }
        public String Title { get; set; }
        [Required(ErrorMessage = "Please fill Name")]
        [RegularExpression("[a-zA-Z ]*$", ErrorMessage = "Use letters only please")]
        [StringLength(50, ErrorMessage = "Name should not less Than 3 characters", MinimumLength = 3)]


        public String Name { get; set; }
        public string Tagname { get; set; }
        public string Code { get; set; }

        [RegularExpression("[a-zA-Z ]*$", ErrorMessage = "Use letters only please")]
        public string Alias_Name { get; set; }
        public ViewModel.Common.PersonAccountDetails Account_Detail { get; set; }
        public ViewModel.Common.Address Address { get; set; }
        [Required(ErrorMessage = "Select Group Name")]
        public long Group_Id { get; set; }
        public decimal Opening_Balance { get; set; }

        [Display(Name = "Maintain Record Bill by Bill")]
        [Required(ErrorMessage = "Please Select")]
        public bool MaintainRecord_BillbyBill { get; set; }
        public int CreditPeriodTime { get; set; }
        [Display(Name = "Group Name")]
       
        public string GroupName { set; get; }
        public string Image_Path { get; set; }

        [Display(Name = "Contact Person")]
        [RegularExpression("[a-zA-Z ]*$", ErrorMessage = "Use letters only please")]
        [StringLength(50, ErrorMessage = "Name should not less Than 3 characters", MinimumLength = 3)]
        public string Contact_Person { get; set; }
        [Display(Name = "Email")]

        public string Contact_Person_Email { get; set; }

        [Display(Name = "Contact No.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Invalid Contact No.")]
        [StringLength(10, ErrorMessage = "Contact No. should be 10 digits", MinimumLength = 10)]
        public string Contact_Person_ContactNo { get; set; }

    }

}

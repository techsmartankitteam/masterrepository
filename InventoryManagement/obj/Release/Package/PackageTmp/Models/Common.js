﻿//Process Start On Page Loading
$(document).on({
    mouseenter: function () {
        $(this).toggleClass('clickable');
    },
    mouseleave: function () {
    }
}, 'tbody tr');
$(function () {
    startTime();
    var lst = window.location.href + '/List';
    //For Filtetr Data 
    //$('#searchText').click(function () {
        
    //    var data = $('#SearchText').val();
    //    if (data != null)
    //        FilterData(data, lst, '#PageContent');
    //});
       //For Top menu 
    $(".dropdown-menu > li > a.trigger").on("click", function (e) {
        var current = $(this).next();
        var grandparent = $(this).parent().parent();
        if ($(this).hasClass('left-caret') || $(this).hasClass('right-caret'))
            $(this).toggleClass('right-caret left-caret');
        grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
        grandparent.find(".sub-menu:visible").not(current).hide();
        current.toggle();
        e.stopPropagation();
    });
    $(".dropdown-menu > li > a:not(.trigger)").on("click", function () {
        var root = $(this).closest('.dropdown');
        root.find('.left-caret').toggleClass('right-caret left-caret');
        root.find('.sub-menu:visible').hide();
    });
     // For Export Table To Excel 
    $(".hgt").on('change', function (event) {
        // CSV


      //  $('#gridcontent table').tableExport({ type: 'pdf', escape: 'false' }); 

        
      // $('#gridcontent table').tableExport({ type: 'excel', escape: 'false' }); 
        
      // $('#gridcontent table').tableExport({ type: 'pdf', escape: 'false' }); 
        
        //$('#gridcontent table').tableExport({ type: 'png', escape: 'false' }); 
        
              //$('#gridcontent table').tableExport({ type: 'csv', escape: 'false' }); 
        
              //$('#gridcontent table').tableExport({ type: 'powerpoint', escape: 'false' }); 
        
              //$('#gridcontent table').tableExport({ type: 'xml', escape: 'false' }); 
        
              $('#gridcontent table').tableExport({ type: 'doc', escape: 'false' }); 
        
              //$('#gridcontent table').tableExport({ type: 'txt', escape: 'false' }); 
        
        //var indxx = -1;
        //var tdcnt = 0;
        //var Heading = "";
        //debugger;
        ////    exportTableToCSV.apply(this, [$('#gridcontent>table'), 'Ledger.csv']);
        //if ($(this).find('option:selected').text() != "" && $(this).find('option:selected').text()=="Export") {

        //    var data = '<table>';
        //    $('#gridcontent table').find('thead th').each(function (ind, val) {

        //        tdcnt++;
        //        if ($(val).html().trim() == 'Action' || $(val).find('a').html() == 'Action')
        //        { indxx = ind; }
        //        else
        //            Heading += '<td>' + $(val).html() + '</td>';
        //    });
            
            
        //    data += '<tr><td  colspan=' + tdcnt + ' >' + $('.logo').html() + '</td></tr>';
        //    data += '<tr><td  colspan=' + tdcnt + ' >' + $('.panel-heading>.row>.col-md-9').html() + '</td></tr>';
        //  data += '<tr>' + Heading + '</tr>';
        //    $('#gridcontent table').find('tbody tr').each(function (ind, val) {
        //        data += '<tr>';
        //        $(val).find('td').each(function (inx, trs) {
        //            if (inx != indxx)
        //                data += '<td>' + $(trs).html() + '</td>';

        //        });
        //        data += '</tr>';
        //    });
        //    debugger;
        //    data += '</table>';


            //OpenOffice
            //window.open('data:application/vnd.oasis.opendocument.spreadsheet,<table>' + $('#gridcontent table').html() + '</table>');
            ////MS-Excel
        //    $("#gridcontent table").table2excel({
                
        //            // exclude CSS class
                
        //        exclude: ".noExl",
            
        //    name: "Worksheet Name",
       
        //    filename: "SomeFile" //do not include extension
            
        //});



          //  window.open('data:application/vnd.ms-excel,' + data);
            //CSV                                           ,<
            //window.open('data:application/csv,'+data);

      //  }
        // IF CSV, don't do event.preventDefault() or return false
        // We actually need this to be a typical hyperlink
    });

    //For Trigger Table Clickable Ecevts
    $('table tbody').on('hover', 'tr', function () {
        $(this).toggleClass('clickable');
    });
//Timmer Starting
    startTime();
});
 // For POPup Opens and Close 
    function OpenPopup(Popup) {
             if (Popup != undefined) {
            $('#myModalLabel').text(Popup);
        }
        else {
                 $('#myModalLabel').text("My Popup");
        }
             $('#modalinvtntify').modal({ backdrop: 'static' });
    }
    function ClosePopup() {
        $('#modalinvtntify').modal('hide');
    }
//For Timming   updation on Desktop
function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();

    // add a zero in front of numbers<10 m=checkTime(m);
    h = checkTime(h);
    m = checkTime(m);
    s = checkTime(s);
    var day_or_night = (h > 11) ? "PM" : "AM";

    //Convert to 12 hours system
    if (h > 12)
        h -= 12;

    //Add time to the headline and update every 500 milliseconds
    $('#time').html(h + ":" + m + " " + day_or_night);
    setTimeout(function () {
        startTime()
    }, 6000);
};
function checkTime(i) {


    if (i < 10) {
        return "0" + i.toString();
    }
    return i;
}
// Use for Refresh Button Click
function clearAll() {
    var Path = window.location.href + '/Index';
    $('#SearchText').val('');
    var data = $('#SearchText').val();
    if (data != null)
        FilterData(data, Path, '#PageContent');

}
 // Used For Filter the data bahelf of the Text 
function FilterData(Text, Url, ControlId) {
    $.ajax({ url: Url, data: { Search: Text }, success: function (data) { $(ControlId).html(data); }, error: function () { } });
}
// Used for Encoding ImageFile As URL 
function encodeImageFileAsURL(id) {

    var filesSelected = document.getElementById(id).files;
    if (filesSelected.length > 0) {
        var fileToLoad = filesSelected[0];

        var fileReader = new FileReader();

        fileReader.onload = function (fileLoadedEvent) {
            var srcData = fileLoadedEvent.target.result; // <--- data: base64

            var newImage = document.createElement('img');
            newImage.src = srcData;
            $("img[src*='data:image']").addClass("mangaeimg");

            // alert(srcData);

            document.getElementById("imgTest").innerHTML = newImage.outerHTML;
            //alert("Converted Base64 version is " + document.getElementById("imgTest").innerHTML);
            // console.log("Converted Base64 version is " + document.getElementById("imgTest").innerHTML);
            $('#ImageName').val(srcData);
        }
        fileReader.readAsDataURL(fileToLoad);
    }
}
 // For Google Translator
function googleTranslateElementInit() {
    new google.translate.TranslateElement(
               { pageLanguage: 'en' },
               'google_translate_element'
           );
    /*
    To remove the "powered by google",
    uncomment one of the following code blocks.
    NB: This breaks Google's Attribution Requirements:
    https://developers.google.com/translate/v2/attribution#attribution-and-logos
*/

    // Native (but only works in browsers that support query selector)
    if (typeof (document.querySelector) == 'function') {
        document.querySelector('.goog-logo-link').setAttribute('style', 'display: none');
        document.querySelector('.goog-te-gadget').setAttribute('style', 'font-size: 0');
    }

    // If you have jQuery - works cross-browser - uncomment this
    jQuery('.goog-logo-link').css('display', 'none');
    jQuery('.goog-te-gadget').css('font-size', '0');
}
function fn_ValidateForm(fm_Id) {


    //  $('#' + fm_Id).validate();
    //confirm("Do you want to save")==true?
    //:event.preventDefault()
    var res = JSON.stringify($('#' + fm_Id).validate({ ignore: "" }).submitted);
    console.log(res.hasOwnProperty());


}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Threading.Tasks;
namespace HotelManagementErp_Main.Helper
{
    /// <summary>
    /// Base Controller 
    /// </summary>
    public class BaseController : Controller
    {
        /// <summary>
        /// Controller Data Boinding
        /// </summary>
        /// <param name="filterContext"></param>
      protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            String Tagname = "Inventory";
            if (filterContext.HttpContext.Request.QueryString["Tagname"] != null)
                Tagname = filterContext.HttpContext.Request.QueryString["Tagname"].ToString();
            String LayoutTag = Tagname;
            ViewBag.layout = null;
            ViewBag.Tagname = LayoutTag;
            if(!Request.IsAjaxRequest())
                ViewBag.layout = "~/Areas/Inventory/Views/Shared/_ContentLayout.cshtml";
            //if (Session["dashboard"] == null)
            //{
            //    filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { controller = "LogIn", action = "FrontLogin", area = "" }));
            //}
            //else
            //{
            var _userLog = new ViewModel.Users.LoginInfo
            {
                CompanyId = 1,
                ConnectionString = "",
                FinancialId = 1,
                FinancialYear = "2015-2016",
                LoginId = 1,
                LoginName = "Ankit Singh",
                SecurityToken = Guid.NewGuid().ToString(),
                UserSession = Guid.NewGuid().ToString()
            };
           
           

                if (Session["dbConnection"] != null)
                {
           
                    _userLog.ConnectionString = Session["dbConnection"].ToString();
                }
           
                ViewBag.UserLog = _userLog;
            //}
         
            base.OnActionExecuting(filterContext);
        }
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }
        protected override void OnException(ExceptionContext filterContext)
        {
             log4net.ILog log = log4net.LogManager.GetLogger(typeof(InventoryBaseController));
             log.Error(filterContext.Controller.ToString()+" have error : "+filterContext.Exception.Message.ToString());
             
            base.OnException(filterContext);
        }

}        }
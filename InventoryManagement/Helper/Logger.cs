﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.IO;
using System.Linq;
using System.Web;
using log4net.Appender;
using log4net.Config;
using log4net.Repository.Hierarchy;

namespace InventoryManagement.Helper
{
    public class Log4NetManager
    {

        public static void InitializeLog4Net()
        {
            XmlConfigurator.ConfigureAndWatch(new FileInfo(System.AppDomain.CurrentDomain.BaseDirectory + @"\Log4Net.config"));

            Hierarchy hier = log4net.LogManager.GetRepository() as Hierarchy;

            if (hier != null)
            {
                AdoNetAppender adoAppender = (from appender in hier.GetAppenders()
                                              where appender.Name.Equals("DbAppender", StringComparison.InvariantCultureIgnoreCase)
                                              select appender).FirstOrDefault() as AdoNetAppender;

                if (adoAppender != null && adoAppender.ConnectionString.Contains("{auto}"))
                {
                    adoAppender.ConnectionString = ExtractConnectionStringFromEntityConnectionString(
                            GetEntitiyConnectionStringFromWebConfig());

                    //refresh settings of appender
                    adoAppender.ActivateOptions();

                }
            }

        }


        private static string GetEntitiyConnectionStringFromWebConfig()
        {
            return ConfigurationManager.ConnectionStrings["INVENTORY_DBEntities"].ConnectionString;

            //MemberEntities is like this in  Web.Config : http://pastebin.com/R9wCwRar


        }


        private static string ExtractConnectionStringFromEntityConnectionString(string entityConnectionString)
        {
            // create a entity connection string from the input
            EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder(entityConnectionString);

            // read the db connectionstring
            return entityBuilder.ProviderConnectionString;
        }
    }
}
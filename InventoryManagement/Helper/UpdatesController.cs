﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace InventoryManagement.Helper
{
    public class UpdatesController : ApiController
    {
        // GET api/updates
       [HttpGet]
        public List<ViewModel.NewsAndUpdates.Updates> GetUpdates()
        {

            List<ViewModel.NewsAndUpdates.Updates> _updates = new List<ViewModel.NewsAndUpdates.Updates>();
            _updates.Add(new ViewModel.NewsAndUpdates.Updates { WebAddress = "http://www.google.com", Heading = "Techsmart Education", Icon = "fa fa-cogs", ImageUrl = "http://i2.wp.com/www.ibpsexamadda.org.in/wp-content/uploads/2016/02/newsUpdates.jpg", Message = "TechSmart launches his new Product with Inventory management.Wnat more Click Heare.", UniqueId = Guid.NewGuid().ToString(), ReleaseOn = DateTime.Now.AddDays(-55) });
            _updates.Add(new ViewModel.NewsAndUpdates.Updates { WebAddress = "http://www.yahoo.com", Heading = "TechSmart Hotels", Icon = "fa fa-cogs", ImageUrl = "http://www.androidcentral.com/sites/androidcentral.com/files/styles/larger_wm_brw/public/article_images/2014/10/microsoft_next_lock_screen_lede.jpg", Message = "TechSmart launches his new Product with Inventory management.Wnat more Click Heare.", UniqueId = Guid.NewGuid().ToString(), ReleaseOn = DateTime.Now.AddDays(-45) });
            _updates.Add(new ViewModel.NewsAndUpdates.Updates { WebAddress = "http://www.facebook.com", Heading = "Ontime Employee Manager", Icon = "fa fa-cogs", ImageUrl = "http://i.imgur.com/UACT5.jpg", Message = "TechSmart launches his new Product with Inventory management.Wnat more Click Heare.", UniqueId = Guid.NewGuid().ToString(), ReleaseOn = DateTime.Now.AddDays(-35) });
            _updates.Add(new ViewModel.NewsAndUpdates.Updates { WebAddress = "http://www.LinkedId.com", Heading = "Techsmart Delhi Police", Icon = "fa fa-cogs", ImageUrl = "http://www.guidingtech.com/assets/postimages/2015/08/DSC_0255_result.png", Message = "TechSmart launches his new Product with Inventory management.Wnat more Click Heare.", UniqueId = Guid.NewGuid().ToString(), ReleaseOn = DateTime.Now.AddDays(-25) });
            _updates.Add(new ViewModel.NewsAndUpdates.Updates { WebAddress = "http://www.fortune-it.com", Heading = "Hospital Management", Icon = "fa fa-cogs", ImageUrl = "http://www.guidingtech.com/assets/postimages/2015/08/DSC_0220_result.png", Message = "TechSmart launches his new Product with Inventory management.Wnat more Click Heare.", UniqueId = Guid.NewGuid().ToString(), ReleaseOn = DateTime.Now.AddDays(-5) });
            return _updates;
        }

    }
}

﻿using HotelManagementErp_Main.Helper;
using ServiceLayer.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Transactions;

namespace InventoryManagement.Areas.Transactions.Controllers
{
    public class ReceiptNoteController : InventoryBaseController
    {
          ReceiptNoteServiceLayer _db;

        public ActionResult Index()
        {
            int Val = Convert.ToInt32(TempData["SaveStatus"]);
            if (Val != null && Val != 0)
                ViewBag.SaveStatus = Val;
            return View();
        }
        public ActionResult List(string search = "")
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new ReceiptNoteServiceLayer(_dashboardData);
         //   List<ReceiptNoteList> lst = new List<ReceiptNoteList>();
            var lst = _db.List(search);
            return View(lst);
        }
        public ActionResult Create(long Id = 0,long Order_Id=0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new ReceiptNoteServiceLayer(_dashboardData);
            ReceiptNoteInfo modelReceiptNote = new ReceiptNoteInfo();


            if (Id != 0 && Order_Id==0)
            {
                modelReceiptNote = _db.Find(Id);
                modelReceiptNote.PurchaseOrderList = _db.DDLBind(SuuplierId: modelReceiptNote.Supplier_Id, Id: Id);
            }
            else if(Id == 0 && Order_Id!=0)
            {
                PurchaseOrderInfo PurOrd = new PurchaseOrderServiceLayer(_dashboardData).Find(Order_Id);
                modelReceiptNote.Supplier_Name = PurOrd.Supplier_Name;
                modelReceiptNote.Supplier_Id = PurOrd.Supplier_Id;
                modelReceiptNote.Purchase_Order_Id = Order_Id;
                modelReceiptNote.Amount = PurOrd.Amount;
                modelReceiptNote.Purchase_Order_No = PurOrd.Order_No;
                modelReceiptNote.Refered_Type = "PURCHASEORDER";
                modelReceiptNote.Amount = PurOrd.Amount;
                modelReceiptNote.Receipt_Date = DateTime.Now.ToShortDateString();
                modelReceiptNote.Receipt_Note_No = _db.GEN_ReceiptNo();
                modelReceiptNote.PurchaseOrderList = _db.DDLBind(SuuplierId: modelReceiptNote.Supplier_Id, Id: Id);
            }
            else
            {
                modelReceiptNote = new ReceiptNoteInfo();
                modelReceiptNote.Receipt_Date = DateTime.Now.ToShortDateString();
                modelReceiptNote.Receipt_Note_No = _db.GEN_ReceiptNo();


            }
            BindDDL(modelReceiptNote);
            return View(modelReceiptNote);
        }
        //public ActionResult Create()
        //{
        //    _db = new ReceiptNoteServiceLayer();
        //    ReceiptNoteInfo data = new ReceiptNoteInfo();
        //    return View(BindDDL(data));
        //}

        [HttpPost]
        public ActionResult Create(ReceiptNoteInfo modelReceiptInfo)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new ReceiptNoteServiceLayer(_dashboardData);
           // ReceiptNoteInfo data = new ReceiptNoteInfo();
            if (ModelState.IsValid == true || ModelState.IsValid == false)
            {
                if (modelReceiptInfo.Id != 0)
                {
                   _db.Update(modelReceiptInfo);
                   TempData["SaveStatus"] = 2;
                }
                else
                {

                   _db.Create(modelReceiptInfo);
                   TempData["SaveStatus"] = 1;

                }

                var lst = _db.List();

                return View("List", lst);
            }
            BindDDL(modelReceiptInfo);
            return View(modelReceiptInfo);
        }

        private void BindDDL(ReceiptNoteInfo data)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            if (data != null)
            {
                data.SupplierList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("Supplier");
                if(data.Supplier_Id!=0)
                {
                    data.PurchaseOrderList = _db.DDLBind(data.Supplier_Id, data.Id);
                }
              //  data.PurchaseOrderList = new ServiceLayer.Common.CommonServiceLayer().DDLBind("Stock_Item");
               
            }
            
        }


        public ActionResult BindPurchaseOrderNO(int SupplierId=0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new ReceiptNoteServiceLayer(_dashboardData);
            var data = _db.DDLBind(SupplierId).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult ViewReceipt(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new ReceiptNoteServiceLayer(_dashboardData);
            ReceiptNoteInfo modelReceiptNote;


            if (Id != 0)
            {
                modelReceiptNote = _db.Find(Id);
                //modelReceiptNote = new ReceiptNoteInfo();

            }
            else
            {
                modelReceiptNote = new ReceiptNoteInfo();
                modelReceiptNote.Receipt_Date = DateTime.Now.ToShortDateString();
                
                modelReceiptNote.Order_Date = DateTime.Now.ToShortDateString();
                
                modelReceiptNote.Receipt_Note_No = _db.GEN_ReceiptNo();

            }

            return PartialView(modelReceiptNote);
        }
        public ActionResult ReceiptNoteDetailsList(int Id = 0, int OrderId = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            ReceiptNoteInfo modelReceiptNote = new ReceiptNoteInfo();
            _db = new ReceiptNoteServiceLayer(_dashboardData);

            if (OrderId != 0 || Id != 0)
            {

                modelReceiptNote.ReceiptDetails = _db.ReceiptNoteDetails(Id: Id, OrderId: OrderId);
                modelReceiptNote.Purchase_Order_Id = OrderId;
                modelReceiptNote.Id = Id;

                //  modelDeliveryNote.DeliveryDetails.Add(new DeliveryNoteInfo_Tra());
            }
            else
            {
                modelReceiptNote.ReceiptDetails.Add(new ReceiptNoteTraInfo());
            }

            BindDDL(modelReceiptNote);
            return View(modelReceiptNote);
        }
        public ActionResult ViewReceiptDetailsList(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            ReceiptNoteInfo modelReceiptNote =new ReceiptNoteInfo();
            _db = new ReceiptNoteServiceLayer(_dashboardData);

            if (Id != 0)
            {
                var Challan_No = _db.Find(Id).Receipt_Note_No;
                modelReceiptNote.ReceiptDetails = _db.ViewReceiptNoteDetails(Id);
                //modelReceiptNote.ReceiptDetails.Add(new ReceiptNoteTraInfo());
            }
            else
            {


                modelReceiptNote.ReceiptDetails.Add(new ReceiptNoteTraInfo());
            }


            return View(modelReceiptNote);
        }
        public ActionResult MasterDetails(int SuuplierId = 0, int OrderId = 0)
        {
            var _UserLog = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            
            _db = new ReceiptNoteServiceLayer(_UserLog);
            ReceiptNoteMasterInfo data = new ReceiptNoteMasterInfo();
            if(SuuplierId!=0 && OrderId!=0)
            {
                var objsupplier = new ServiceLayer.Masters.LedgerMasterServiceLayer(_UserLog).Find(SuuplierId);
                var objorder = new ServiceLayer.Transactions.PurchaseOrderServiceLayer(_UserLog).Find(OrderId);
                data = new ReceiptNoteMasterInfo { Name = objsupplier.Name, Contact_No = objsupplier.Contact_No, Email = objsupplier.Email_Id, Address = objsupplier.Address, Order_Date = objorder.Posting_Date.ToShortDateString() };
            }
            else
            {
                data = new ReceiptNoteMasterInfo { Address = "N/A", Contact_No = "N/A", Supplier_Code = "N/A", Email = "N/A", Name = "N/A", Order_Date = "N/A" };
            }
            return PartialView(data);
        }

        public ActionResult ApproveOrderList(int Id)
        {
            PurchaseOrderInfo info = new PurchaseOrderInfo();
            //var _dashboardData = ViewBag.LoginInfo as HDMEntity.DashBoard;
            //_db = new ReceiptNoteServiceLayer(_dashboardData.DbConnectionString);
            //info = _db.Find(Id);
            //info.OrderDetails = _db.OrderDetails(info.Id);
            //PurchaseOrderItem OrderdItem = new ServiceLayer.Transactions.PurchaseServiceLayer().GetPurchaseOrderItemList(info);
            //TempData["PurchaseOrderDetails"] = OrderdItem;

            // return View("Transactions/Purchase/Create", OrderdItem);
            // return View(@Url.Action("Create", "Purchase", new { area = "Transactions" }), OrderdItem);
            return RedirectToAction("Create", "Purchase", new { OrderedItem = 1 });
        }
    }
}

﻿using HotelManagementErp_Main.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Common;
using ViewModel.Transactions;
using ServiceLayer.Transactions;
namespace InventoryManagement.Areas.Transactions.Controllers
{
    public class PurchaseReturnController : InventoryBaseController
    {
        //
        // GET: /Transactions/PurchaseReturn/
        PurchaseReturnServiceLayer _db;
        public ActionResult Index()
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseReturnServiceLayer(_dashboardData);
            return View();
        }
        public ActionResult List(string search = "")
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseReturnServiceLayer(_dashboardData);
            var lst = _db.List(search);
            return View(lst);
        }
        public ActionResult Create(long Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseReturnServiceLayer(_dashboardData);
            PurchaseReturnInfo ModelPurRet;
            if (Id > 0)
            {
                ModelPurRet = _db.Find(Id);
            }
            else
            {
                ModelPurRet = new PurchaseReturnInfo();
                ModelPurRet.VC_No = _db.GEN_VC_No();
                ModelPurRet.Return_Date = DateTime.Today;
            }
            DDLBind(ModelPurRet);
            return View(ModelPurRet);
        }

        [HttpPost]
        public ActionResult Create(PurchaseReturnInfo ModelPurRet)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseReturnServiceLayer(_dashboardData);
            if (ModelState.IsValid || ModelState.IsValid == false)
            {
                if (ModelPurRet.Id > 0)
                {
                    _db.Update(ModelPurRet);

                }
                else
                {
                    _db.Create(ModelPurRet);
                }
                var lst = _db.List();
                return View("List", lst);
            }
            DDLBind(ModelPurRet);
            return View(ModelPurRet);
        }
        public ActionResult PurchaseReturnDetails(long Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseReturnServiceLayer(_dashboardData);
                        PurchaseReturnInfo ModelPurRet;


            if (Id > 0)
            {
                ModelPurRet = _db.Find(Id);
                ModelPurRet.ReturnDetails = _db.ReturnDetails(Id);

            }
            else
            {
                ModelPurRet = new PurchaseReturnInfo();
                ModelPurRet.ReturnDetails.Add(new PurchaseReturnInfo_Tra());
            }

            DDLBind(ModelPurRet);
            return View(ModelPurRet);
        }
        public ActionResult CheckSerialNo(string SerialNo, int Purchase_Id)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseReturnServiceLayer(_dashboardData);
            var result = _db.CheckSerialNo(SerialNo, Purchase_Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Added by anurag to check for return Serial numbers.
        /// </summary>
        /// <param name="SerialNo"></param>
        /// <returns></returns>
        public ActionResult CheckIsReturnedSerialNo(string SerialNo, int Item_Id, int Purchase_Id)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseReturnServiceLayer(_dashboardData);
            var result = _db.CheckIsReturnedSerialNo(SerialNo, Item_Id, Purchase_Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckValidQty(int Supplier_Id, int Purchase_Id, int Item_Id)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseReturnServiceLayer(_dashboardData);
            var result = _db.CheckValidQty(Supplier_Id, Purchase_Id, Item_Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetItemDetail(long Id)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseReturnServiceLayer(_dashboardData);
            var item = new ServiceLayer.InventoryMaster.StockServiceLayer(_dashboardData).Find(Id);
            return Json(item, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(long Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseReturnServiceLayer(_dashboardData);


            bool result = false;
            if (Id > 0)
            {
                result = _db.Delete(Id);

            }

            var lst = _db.List();
            return View("List", lst);
        }
        public ActionResult GetBillList(long Supplier_Id)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseReturnServiceLayer(_dashboardData);
            var l = _db.GetBillList(Supplier_Id);
            return Json(l, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Added by anurag
        /// </summary>
        /// <param name="Purchase_Id"></param>
        /// <returns></returns>
        public ActionResult GetStockItemForSupplier(long Purchase_Id)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseReturnServiceLayer(_dashboardData);
            var l = _db.GetStockItemForSupplier(Purchase_Id);
            return Json(l, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult ViewPurReturn(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseReturnServiceLayer(_dashboardData);

            PurchaseReturnInfo ModelPurRet = new PurchaseReturnInfo();
            ModelPurRet = _db.Find(Id);

            return PartialView(ModelPurRet);
        }

        public ActionResult ViewPurReturnDetailsList(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            PurchaseReturnInfo ModelPurRet = new PurchaseReturnInfo();
            _db = new PurchaseReturnServiceLayer(_dashboardData);

            if (Id != 0)
            {
                //var Challan_No = _db.Find(Id).Id;
                ModelPurRet.ReturnDetails = _db.ReturnDetails(Id);
            }
            else
            {


                ModelPurRet.ReturnDetails.Add(new PurchaseReturnInfo_Tra() { Quantity = 0.00m });
            }


            return View(ModelPurRet);
        }
        private void DDLBind(PurchaseReturnInfo ModelPurRet)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseReturnServiceLayer(_dashboardData);
            //  ModelPurRet.ItemList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData.DbConnectionString).DDLBind("Stock_Item");
            ModelPurRet.SupplierList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("Supplier");
            ModelPurRet.BillList = new List<DDLBind>();

        }


        /// <summary>
        /// Added by anurag dated-29-06-2016 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult GetItemRate(int Item_Id, int Purchase_Id)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseReturnServiceLayer(_dashboardData);
            //  var item = new ServiceLayer.InventoryMaster.StockServiceLayer(_dashboardData.DbConnectionString).Find(Id);
            var result = _db.GetItemRate(Item_Id,Purchase_Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}

﻿using HotelManagementErp_Main.Helper;
using ServiceLayer.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Ledger;
using ViewModel.Common;
namespace InventoryManagement.Areas.Transactions.Controllers
{
    public class SalesController : InventoryBaseController
    {
        SalesServiceLayer _db;
        // PurchaseServiceLayer _db;
        public ActionResult Index()
        {
            
            return View();
        }
        public ActionResult List(string search="")
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            
            _db = new SalesServiceLayer(_dashboardData);
            var lst = _db.List(search);

            return View(lst);
        }

        public ActionResult Create(long Id = 0, long Delivery_Note_Id = 0)
        {
            SalesMaster ModelSalMaster;
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;

            _db = new SalesServiceLayer(_dashboardData);
            var Challan_No = "";
            if (Id != 0 && Delivery_Note_Id == 0)
            {
                ModelSalMaster = _db.Find(Id);
                

                // Sales Purchase_tra code here
                Challan_No = ModelSalMaster.Challan_Number;

                ModelSalMaster.sales_tra.listpurchase_tra = _db.ItemDetails(Id);
              
                ModelSalMaster.sales_tra.Grand_Total = ModelSalMaster.Grand_Total;
              
                Challan_No = ModelSalMaster.sales_tra.listpurchase_tra.Count > 0 ? ModelSalMaster.sales_tra.listpurchase_tra[0].Challan_Number : null;

            }
            else if (Id == 0 && Delivery_Note_Id != 0)
                {
                    ModelSalMaster = new SalesMaster();
                    var tblDeliveryNote = new ServiceLayer.Transactions.DeliveryNoteServiceLayer(_dashboardData).Find(Delivery_Note_Id);
                    SalesOrderItem DeliveryDetails = _db.GetSalesOrdertemList(Delivery_Note_Id);
                    DeliveryDetails.listSalesOrderItem.ToList().ForEach(x => ModelSalMaster.sales_tra.listpurchase_tra.Add(new Purchase_Tra
                    {
                        Id = x.Id,
                        Item_Id = x.ItemId,
                        ItemName = x.ItemName,
                        Qty = x.Quantity,
                        Rate = x.Rate,
                        ProductCode = x.ProductCode,
                        SerialNo = x.SerialNo,
                        Unit_Name = x.UnitName ?? "",

                    }));
                ModelSalMaster.Is_Refered = true;
                ModelSalMaster.Refered_Id = Delivery_Note_Id;
                ModelSalMaster.Customer_Name = tblDeliveryNote.Name;
                ModelSalMaster.Contact_No = tblDeliveryNote.Contact_No;
                ModelSalMaster.Email_Id = tblDeliveryNote.Email;
                ModelSalMaster.Refered_Type = "DELIVERYNOTE";
                ModelSalMaster.Customer_Id = tblDeliveryNote.Customer_Id;
                ModelSalMaster.UnitList = new List<ViewModel.Common.DDLBind>();
                ModelSalMaster.Challan_Number = _db.GEN_ChallanNo();
                ModelSalMaster.sales_tra.taxdetails.listTaxDetails.Add(new ViewModel.Common.TaxDetails());
                ModelSalMaster.Purchase_Date = DateTime.Today;
             
                    //ModelPurMaster.purchase_tra.listpurchase_tra
                 }

            else
            {
                ModelSalMaster = new SalesMaster();
                ModelSalMaster.UnitList = new List<ViewModel.Common.DDLBind>();
                ModelSalMaster.Challan_Number = _db.GEN_ChallanNo();
                ModelSalMaster.sales_tra.taxdetails.listTaxDetails.Add(new ViewModel.Common.TaxDetails());
                ModelSalMaster.Purchase_Date = DateTime.Today;
                ModelSalMaster.Is_Refered = false;
                
            }
            

            if (Id > 0)
            {
                //ModelSalMaster.sales_tra.taxdetails.listTaxDetails = new ServiceLayer.Transactions.PurchaseServiceLayer().GetTaxList(Challan_No);
                ModelSalMaster.sales_tra.taxdetails.listTaxDetails = _db.GetTaxList(Id);
            }
            
            ModelSalMaster.sales_tra.taxdetails.Grand_Total = ModelSalMaster.sales_tra.Grand_Total;
            ModelSalMaster.sales_tra.Id = ModelSalMaster.Id;
            DDLBind(ModelSalMaster);
            return View(ModelSalMaster);
        }
        [HttpPost]
        public ActionResult Create(SalesMaster ModelSalMaster)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;

            _db = new SalesServiceLayer(_dashboardData);
            if (ModelState.IsValid == true)
            {
                if (ModelSalMaster.Id != 0)
                {
                    _db.Update(ModelSalMaster);
                }
                else
                {
                    _db.Create(ModelSalMaster);
                    

                }

                var lst = _db.List();

                return View("List", lst);
            }
            DDLBind(ModelSalMaster);
            return View(ModelSalMaster);
        }
        public ActionResult ViewDetails(int Id)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;

            _db = new SalesServiceLayer(_dashboardData); 
            var lst = _db.ViewDetails(Id);


            return View(lst);
        }
        public ActionResult GentChallanOfSales(int Id)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;

            _db = new SalesServiceLayer(_dashboardData);

            var data = _db.Gen_ChallanOfSales(Id);

            var lst = _db.List();

            return View("List", lst);
        }
        public ActionResult GetUnitName(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;

            _db = new SalesServiceLayer(_dashboardData);
            ServiceLayer.InventoryMaster.StockServiceLayer _stockSL = new ServiceLayer.InventoryMaster.StockServiceLayer(_dashboardData);
            ServiceLayer.Masters.UnitsServiceLayer _unitSL = new ServiceLayer.Masters.UnitsServiceLayer();
            var stock = _stockSL.Find(Id);
            var data = _unitSL.DDLBind().SingleOrDefault(x => x.Id == stock.Unit_Id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetStockItemDetails(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;

            _db = new SalesServiceLayer(_dashboardData);
            ServiceLayer.Transactions.StockMasterServiceLayer _objStockMaster = new StockMasterServiceLayer(_dashboardData);
            var data = _objStockMaster.StockMasterDDL(Id);



            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCustomer(string searchText = "")
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;

            _db = new SalesServiceLayer(_dashboardData);

            var data = new ServiceLayer.AccountMaster.CustomerServicesLayer(_dashboardData).DDLBind("CUSTOMER", searchText);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult ItemList(int Id = 0, int Gtotal=0)
        //{
        //    SalesMaster ModelSalMaster = new SalesMaster();
        //    _db = new SalesServiceLayer();
        //    ServiceLayer.InventoryMaster.StockServiceLayer _stockSL = new ServiceLayer.InventoryMaster.StockServiceLayer();
        //    if (Id != 0)
        //    {
        //        var Challan_No = _db.Find(Id).Challan_Number;

        //        ModelSalMaster.sales_tra.listpurchase_tra = _db.ItemDetails(Challan_No);
        //        ModelSalMaster.Grand_Total = Gtotal;
        //        ModelSalMaster.listSalesTax.Add(new SalesTax());
        //        ViewBag.ChalanNo = ModelSalMaster.ItemDetails[0].Challan_Number;
        //    }
        //    else
        //    {
        //        //ModelSalMaster.ItemDetails.Add(new Sales_Tra());
        //        ViewBag.ChalanNo = null;
        //    }
        //    ModelSalMaster.bindItemddl = new ServiceLayer.InventoryMaster.StockServiceLayer().ddlBindSaleItem();
        //    return View(ModelSalMaster);
        //}

        public ActionResult _TaxList(string ChallanNo, decimal GTotal)
        {
            SalesMaster model = new SalesMaster();
            model.listSalesTax.Add(new SalesTax());

            if (!string.IsNullOrEmpty(ChallanNo))
            {
                // model.listSalesTax = new ServiceLayer.InventoryMaster.StockServiceLayer().GetTaxList(ChallanNo);

            }
            return PartialView(model);

        }
        
        public ActionResult _ViewSalesItem(long Id = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;

            _db = new SalesServiceLayer(_dashboardData);
            SalesMaster modal = new SalesMaster();

            modal = _db.Find(Id);

          //  modal.ItemDetails = _db.ViewItemDetails(Sales_Id);
            return PartialView(modal);
        }
        public ActionResult ViewSalesDetais(long Sales_Id = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;

            _db = new SalesServiceLayer(_dashboardData);
            SalesMaster modal = new SalesMaster();

          

            modal.ItemDetails = _db.ViewItemDetails(Sales_Id);
            return PartialView(modal);
        }
        public ActionResult Delete(long Id=0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;

            _db = new SalesServiceLayer(_dashboardData); ;
            if(Id>0)
            {
                _db.Delete (Id);
            }
            var lst = _db.List();

            return View("List", lst);
        }
        private void DDLBind(SalesMaster ModelSalMaster)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;

            _db = new SalesServiceLayer(_dashboardData);
            if (ModelSalMaster != null)
            {
                ModelSalMaster.CustomerList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("Customer", "");
                ModelSalMaster.sales_tra.GoDownList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("GoDown_Master", "");
                ModelSalMaster.sales_tra.ledger_common.itemddlbind = new ServiceLayer.InventoryMaster.StockServiceLayer(_dashboardData).ddlBindSaleItem();
               
                ModelSalMaster.sales_tra.taxdetails.TaxDeductionList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("Tax_Deduction_Master", "");
              
            }
        }
        public JsonResult GetItemList(long GoDown_Id)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;

            _db = new SalesServiceLayer(_dashboardData);
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSerial_no(int Id, int Qty, string SrNo)
        {
            _db = new SalesServiceLayer(ViewBag.UserLog);
            var lst = _db.GetSerial_No(Id, Qty, SrNo);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }
    }
}

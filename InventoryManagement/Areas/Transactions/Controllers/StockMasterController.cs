﻿using HotelManagementErp_Main.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ServiceLayer.Transactions;
using ViewModel.Transactions;
namespace InventoryManagement.Areas.Transactions.Controllers
{
    public class StockMasterController : InventoryBaseController
    {
        //
        // GET: /Transactions/StockMaster/
        StockMasterServiceLayer _db;
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(string search="")
        {
            logger.Info("Stock Master Index Page Debugging Started");
            var _dashboardData = ViewBag.UserLog;
            _db = new StockMasterServiceLayer(_dashboardData);
            // List<ViewModel.Transactions.StockMaster> lst = new List<ViewModel.Transactions.StockMaster>();
            var lst = _db.List(search);

            return View(lst);
        }
        public ActionResult ItemAvailabltyDetails(int ItemId = 0)
        {
            logger.Info("Stock Master Index Page Debugging Started");
            var _dashboardData = ViewBag.UserLog;
            _db = new StockMasterServiceLayer(_dashboardData);
            var data = _db.Find(ItemId);

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MasterDetails(int ItemId = 0)
        {
            logger.Info("Stock Master Index Page Debugging Started");
            var _dashboardData = ViewBag.UserLog;
            _db = new StockMasterServiceLayer(_dashboardData);
            // var data = _db.Find(ItemId);
            MasterDetailsInfo modelMasterDetail = new MasterDetailsInfo();
            return View(modelMasterDetail);
        }

        public ActionResult ViewStockMasterdetail(long Id)
        {
            logger.Info("Stock Master Index Page Debugging Started");
            var _dashboardData = ViewBag.UserLog;
            _db = new StockMasterServiceLayer(_dashboardData);
            List<StockMasterDetail> stockMstDetails = new List<StockMasterDetail>();
            StockMaster StockMst = new StockMaster();
            //if (Id > 0)
            //{
            var stockMst1 = _db.FindStockMaster(Id);
            StockMst.StockMstDetails = stockMst1;
            //DdlBind(stock);
            // }
            return View(StockMst);
        }
        public ActionResult ViewStockMasterItemdetail(long Id)
        {
            logger.Info("Stock Master Index Page Debugging Started");
            var _dashboardData = ViewBag.UserLog;
            _db = new StockMasterServiceLayer(_dashboardData);
            List<StockMasterDetail> stockMstDetails = new List<StockMasterDetail>();
            StockMaster StockMst = new StockMaster();
            //if (Id > 0)
            //{
            var stockMst1 = _db.FindStockMaster(Id);
            StockMst.StockMstDetails = stockMst1;
            //DdlBind(stock);
            // }
            return View(StockMst);

        }
    }
}

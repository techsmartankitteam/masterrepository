﻿using HotelManagementErp_Main.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Ledger;
using ServiceLayer.Transactions;
using ViewModel.Transactions;
using ViewModel.Common;
namespace InventoryManagement.Areas.Transactions.Controllers
{
    public class PurchaseController : InventoryBaseController
    {
        //
        // GET: /Transactions/Purchase/
        PurchaseServiceLayer _db;
        public ActionResult Index()
        {
            
            return View();
        }
        public ActionResult List(String search="")
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseServiceLayer(_dashboardData);
            
            var lst = _db.List(search);
            return View(lst);
        }

        public ActionResult ViewDetails(int Id = 0, string Challan_No = "")
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseServiceLayer(_dashboardData);
            var lst = _db.ViewDetails(Id, Challan_No);
            return View(lst);
        }
         
        public ActionResult Create(int Id = 0,long Receipt_Note_Id=0 )
        {

            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseServiceLayer(_dashboardData);
            //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            PurchaseMaster ModelPurMaster=new PurchaseMaster ();
            ServiceLayer.InventoryMaster.StockServiceLayer _stockSL = new ServiceLayer.InventoryMaster.StockServiceLayer(_dashboardData);
            ModelPurMaster.purchase_tra.ledger_common = new Ledger_Common();
           Ledger_Common ddl = BindDDL(ModelPurMaster.purchase_tra.ledger_common); 
           //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            
            if (Id != 0 && Receipt_Note_Id==0)
            {
                ModelPurMaster = _db.Find(Id);

               // ModelPurMaster. = new ServiceLayer.Common.CommonServiceLayer().DDLBind("Supplier", "");
                //  ModelPurMaster.ItemDetails.Add(new Purchase_Tra());
                //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                long Challan_No = _db.Find(ModelPurMaster.Id).Id;
                ModelPurMaster.purchase_tra.listpurchase_tra = _db.ItemDetails(Challan_No);
                ModelPurMaster.purchase_tra.Grand_Total = ModelPurMaster.Grand_Total;
                //ModelPurMaster.listTax.Add(new ViewModel.Common.TaxDetails());
                ViewBag.ChalanNo =  ModelPurMaster.purchase_tra.listpurchase_tra.Count>0? ModelPurMaster.purchase_tra.listpurchase_tra[0].Challan_Number :null;
                //'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            }

            else if (Id == 0 && Receipt_Note_Id!=0)
            {
              //  PurchaseOrderItem PurchaseOrder = (PurchaseOrderItem)TempData["PurchaseOrderDetails"];
                if (Receipt_Note_Id != 0)
                {
                    ModelPurMaster.Is_Refered = true;
                    ModelPurMaster.Refered_Id = Receipt_Note_Id;
                    ModelPurMaster.Refered_Type = "RECEIPTNOTE";
                    PurchaseOrderItem OrderdItem = new ServiceLayer.Transactions.PurchaseServiceLayer(_dashboardData).GetPurchaseOrderItemList(Receipt_Note_Id); ;
                    OrderdItem.listPurchaseOrderItem.ForEach((x) => ModelPurMaster.purchase_tra.listpurchase_tra.Add(new Purchase_Tra {
                        Id = x.Id, 
                        Item_Id = x.ItemId,
                        ItemName = x.ItemName, 
                        Qty = x.Quantity,
                        Rate = x.Rate, 
                        ProductCode = x.ProductCode,
                        SerialNo = x.SerialNo, 
                        Unit_Name = x.UnitName ?? "", Is_Refered = true }));
                  //  PurchaseOrder.listPurchaseOrderItem.ForEach((x) => ModelPurMaster.purchase_tra.listpurchase_tra.Add(new Purchase_Tra { Id = x.Id, Item_Id = x.ItemId, ItemName = "", Qty = x.Quantity, Rate = x.Rate, ProductCode = x.ProductCode, SerialNo = x.SerialNo, Unit_Name = x.UnitName??"", Is_Refered = true }));
                    ModelPurMaster.purchase_tra.Grand_Total = OrderdItem.TotalAMount;
                    //ModelPurMaster.Supplier_Id = PurchaseOrder != null ? PurchaseOrder.SupplierId : ModelPurMaster.Supplier_Id;
                    //ModelPurMaster.orderId = PurchaseOrder != null ? PurchaseOrder.orderId : ModelPurMaster.orderId;
                    ModelPurMaster.Supplier_Id = OrderdItem.SupplierId;
                    ModelPurMaster.orderId = Receipt_Note_Id;
                    ModelPurMaster.Challan_Number = _db.GEN_ChallanNo();
                    ModelPurMaster.Purchase_Date = DateTime.Today;
                }

                
            }

            else
            {



                ModelPurMaster.UnitList = new List<ViewModel.Common.DDLBind>();
                ModelPurMaster.Challan_Number = _db.GEN_ChallanNo();
                ModelPurMaster.Purchase_Date = DateTime.Today;


                ViewBag.ChalanNo = null;
            }
            

            ModelPurMaster.purchase_tra.taxdetails.listTaxDetails.Add(new ViewModel.Common.TaxDetails());

            if (Id > 0)
            {
                ModelPurMaster.purchase_tra.taxdetails.listTaxDetails = _db.GetTaxList(Id); 
            }
                

            
            ModelPurMaster.purchase_tra.taxdetails.Grand_Total = ModelPurMaster.purchase_tra.Grand_Total;
            ModelPurMaster.purchase_tra.Id = ModelPurMaster.Id;
            DDLBind(ModelPurMaster);
            return View(ModelPurMaster);
        }
        [HttpPost]
        public ActionResult Create(PurchaseMaster ModelPurMaster)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseServiceLayer(_dashboardData);

            if (ModelState.IsValid == true)
            { 
                if (ModelPurMaster.Id != 0)
                {
                    _db.Update(ModelPurMaster);
                }
                else
                {
                    _db.Create(ModelPurMaster);

                }

                var lst = _db.List();

                return RedirectToAction("index");
            }
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            DDLBind(ModelPurMaster);
            return View(ModelPurMaster);
        }

        public ActionResult GetUnitName(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseServiceLayer(_dashboardData);

            ServiceLayer.InventoryMaster.StockServiceLayer _stockSL = new ServiceLayer.InventoryMaster.StockServiceLayer(_dashboardData);
            ServiceLayer.Masters.UnitsServiceLayer _unitSL = new ServiceLayer.Masters.UnitsServiceLayer(_dashboardData);
            //var stock = _stockSL.Find(Id);
            //var data = _unitSL.DDLBind().SingleOrDefault(x => x.Id == stock.Unit_Id);
            var data = _stockSL.GetUnitAndAmount(Id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSupplier(string searchText = "")
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseServiceLayer(_dashboardData);

            var data = new ServiceLayer.AccountMaster.CustomerServicesLayer(_dashboardData).DDLBind("SUPPLIER", searchText);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GentChallanOfPurchase(int Id)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseServiceLayer(_dashboardData);

            var data = _db.Gen_ChallanOfPurchse(Id);

            var lst = _db.List();

            return View("List", lst);
        }
        public ActionResult ItemList(PurchaseMaster modal)
        {
           // modal.ledger_common = new Ledger_Common();
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseServiceLayer(_dashboardData);
            ServiceLayer.InventoryMaster.StockServiceLayer _stockSL = new ServiceLayer.InventoryMaster.StockServiceLayer(_dashboardData);
            var ddl = BindDDL(modal.purchase_tra.ledger_common);
            if (modal.Id != 0)
            {
                var Challan_No = _db.Find(modal.Id).Id;
                modal.purchase_tra.listpurchase_tra = _db.ItemDetails(Challan_No);
                modal.purchase_tra.Grand_Total = modal.Grand_Total;
                //ModelPurMaster.listTax.Add(new ViewModel.Common.TaxDetails());
                ViewBag.ChalanNo = modal.purchase_tra.listpurchase_tra[0].Challan_Number;
            }
            else
            {
                PurchaseOrderItem PurchaseOrder = (PurchaseOrderItem)TempData["PurchaseOrderDetails"];
                if (PurchaseOrder != null)
                {

                    PurchaseOrder.listPurchaseOrderItem.ForEach((x) => modal.purchase_tra.listpurchase_tra.Add(new Purchase_Tra { Id = x.Id, Item_Id = x.ItemId, ItemName = ddl.bindDDl.SingleOrDefault(i => i.Id == x.ItemId).Name, Qty = x.Quantity, Rate = x.Rate, ProductCode = x.ProductCode, SerialNo = x.SerialNo }));
                    modal.purchase_tra.Grand_Total = PurchaseOrder.TotalAMount;
                }
                else
                {
                    //  ModelPurMaster.ItemDetails.Add(new Purchase_Tra());
                    ViewBag.ChalanNo = null;
                }
            }
            // BindDDL(ModelPurMaster);
            //ModelPurMaster.ItemList = _stockSL.DDLBind();
            return View(modal.purchase_tra);
        }
        public ActionResult Challan_Details(long Challan_No = 0)
        {

            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseServiceLayer(_dashboardData);
            PurchaseMaster ModelPurMaster = new PurchaseMaster();

            if (Challan_No!=0)
            {

                ModelPurMaster.ItemDetails = _db.ItemDetails(Challan_No);
            }

            return View("ViewDetails", ModelPurMaster);
        }

        private void DDLBind(PurchaseMaster ModelPurMaster)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseServiceLayer(_dashboardData);
            if (ModelPurMaster != null)
            {
                ModelPurMaster.SupplierList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("Supplier", "");
                ModelPurMaster.OrderList.Add(new DDLBind());
                ModelPurMaster.purchase_tra.taxdetails.TaxDeductionList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("Tax_Deduction_Master", "");
               // ModelPurMaster.purchase_tra.ledger_common.bindTax = new ServiceLayer.Masters.TaxServiceLayer(_dashboardData.DbConnectionString).DDLBind();
                ModelPurMaster.purchase_tra.ledger_common.bindDDl = new ServiceLayer.InventoryMaster.StockServiceLayer(_dashboardData).DDLBind();
                ModelPurMaster.purchase_tra.ledger_common.GoDownList = new ServiceLayer.Masters.GoDownServiceLayer(_dashboardData).DDLBind();
                ModelPurMaster.purchase_tra.ledger_common.itemddlbind = new ServiceLayer.InventoryMaster.StockServiceLayer(_dashboardData).DDLItemBind();
            }
        }
        private Ledger_Common BindDDL(Ledger_Common ledger_common)
        {
           // ledger_common = new Ledger_Common();
            var _dashboardData = ViewBag.UserLog;
            if (ledger_common != null)
            {
                ledger_common.bindTax = new ServiceLayer.Masters.TaxServiceLayer(_dashboardData).DDLBind();               
                ledger_common.bindDDl = new ServiceLayer.InventoryMaster.StockServiceLayer(_dashboardData ).DDLBind();
                ledger_common.GoDownList = new ServiceLayer.Masters.GoDownServiceLayer(_dashboardData).DDLBind();
                ledger_common.itemddlbind = new ServiceLayer.InventoryMaster.StockServiceLayer(_dashboardData).DDLItemBind();
               
            }
            return ledger_common;
        }

        public JsonResult PurchaseOrderDDL(long Supplier_Id)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseServiceLayer(_dashboardData);
            
            return Json( JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMaxid(int ItemId)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new PurchaseServiceLayer(_dashboardData);
            var maxid = new ServiceLayer.InventoryMaster.StockServiceLayer(_dashboardData).GEN_MAXId(ItemId);
            return Json(maxid, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TaxDeductionDetails(long ChallanNo, Purchase_Tra tra)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseServiceLayer(_dashboardData);
          

          tra.taxdetails.listTaxDetails.Add(new ViewModel.Common.TaxDetails());
            if (ChallanNo!=0)
            {
                tra.taxdetails.listTaxDetails = _db.GetTaxList(ChallanNo);

            }
            else
            {

                //objPurMaster.TaxList = new ServiceLayer.Common.CommonServiceLayer().DDLBind("Tax_Deduction_Master", "");

            }

            tra.taxdetails.TaxDeductionList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("Tax_Deduction_Master", "");
            tra.taxdetails.Grand_Total = tra.Grand_Total;

            return View(tra.taxdetails);
        }

        public ActionResult Delete(int Id=0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseServiceLayer(_dashboardData);
            if (Id > 0)
            {
                var i = _db.Delete(Id);
            }
            var lst = _db.List();
            return View(lst);
            
        }

        public ActionResult _ViewPurcaseItem(int Id, long ChalanNo = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseServiceLayer(_dashboardData);
            PurchaseMaster modal = new PurchaseMaster();
            modal = _db.Find(Id);
           
            modal.ItemDetails = _db.ItemDetails(ChalanNo);
            return PartialView(modal);
        }

        public JsonResult BindTax()
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseServiceLayer(_dashboardData);
            var list = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("Tax_Deduction_Master", "");
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BindGodown()
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            var list = new ServiceLayer.Masters.GoDownServiceLayer(info).DDLBind();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ServiceLayer.Transactions;
using ViewModel.Transactions;
using HotelManagementErp_Main.Helper;
namespace InventoryManagement.Areas.Transactions.Controllers
{
    public class DeliveryNoteController : InventoryBaseController
    {
        //
        // GET: /Transactions/DebitNote/
        DeliveryNoteServiceLayer _db;
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(string search = "")
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo ;
            _db = new DeliveryNoteServiceLayer(_dashboardData);

            var lst = _db.List(search);
            return View(lst);
        }
        public ActionResult Create(int Id = 0,long Sales_Order_Id=0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new DeliveryNoteServiceLayer(_dashboardData);
           DeliveryNoteInfo modelDeliveryNote;


            if (Id != 0 && Sales_Order_Id==0)
            {
                modelDeliveryNote = _db.Find(Id);
                modelDeliveryNote.SalesOrderList = _db.DDLBind(CustomerId: modelDeliveryNote.Customer_Id, Id:Id); 
            }
                else if(Id==0 && Sales_Order_Id!=0)
                {

                    modelDeliveryNote = new DeliveryNoteInfo();
                    var tblSalesOrder = new ServiceLayer.Transactions.SalesOrderServiceLayer(_dashboardData).Find(Sales_Order_Id);
                    modelDeliveryNote.Sales_Order_Id = Sales_Order_Id;
                    modelDeliveryNote.Sales_Order_No = tblSalesOrder.Order_No;
                    modelDeliveryNote.Refered_Type = "SALESORDER";
                    modelDeliveryNote.Customer_Id = tblSalesOrder.Customer_Id;
                    modelDeliveryNote.Name = tblSalesOrder.Customer_Name;
                    modelDeliveryNote.Amount = tblSalesOrder.Amount;
                    modelDeliveryNote.Contact_No = tblSalesOrder.Contact_No;
                
                modelDeliveryNote.Delivery_Date = DateTime.Now.ToShortDateString();
                modelDeliveryNote.Delivery_Note_No = _db.GEN_DebitNo();
            }

            else
            {
                modelDeliveryNote = new DeliveryNoteInfo();
                modelDeliveryNote.Delivery_Date =  DateTime.Now.ToShortDateString();
               modelDeliveryNote.Delivery_Note_No=_db.GEN_DebitNo();
               

            }

            return View(BindDDL(modelDeliveryNote));
        }
        [HttpPost]
        public ActionResult Create(DeliveryNoteInfo modelDeliveryNote)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new DeliveryNoteServiceLayer(_dashboardData);

            if (ModelState.IsValid == true || ModelState.IsValid == false)
            {
                if (modelDeliveryNote.Id != 0)
                {
                    _db.Update(modelDeliveryNote);
                    
                }
                else
                {

                   _db.Create(modelDeliveryNote);


                }

                var lst = _db.List();

                return View("List", lst);
            }
            return View(BindDDL(modelDeliveryNote));
        }
     
        public ActionResult DeliveryNoteDetailsList(int Id = 0,int OrderId=0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            DeliveryNoteInfo modelDeliveryNote=new DeliveryNoteInfo();
            _db = new DeliveryNoteServiceLayer(_dashboardData);
        
            if (OrderId != 0 || Id!=0)
            {
              
                modelDeliveryNote.DeliveryDetails = _db.DeliveryNoteDetails(Id: Id, OrderId: OrderId);
                modelDeliveryNote.Sales_Order_Id = OrderId;
                modelDeliveryNote.Id = Id;
             
              //  modelDeliveryNote.DeliveryDetails.Add(new DeliveryNoteInfo_Tra());
            }
            else
            {
                modelDeliveryNote.DeliveryDetails.Add(new DeliveryNoteInfo_Tra());
            }


            return View(BindDDL(modelDeliveryNote));
        }
        public ActionResult Delete(int Id)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new DeliveryNoteServiceLayer(_dashboardData);
            if (Id > 0)
            {
                var res = _db.Delete(Id);
            }
            var lst = _db.List();

            return View("List", lst);
        }
        public PartialViewResult ViewDelivery(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new DeliveryNoteServiceLayer(_dashboardData);
            DeliveryNoteInfo modelDeliveryNote;


            if (Id != 0)
            {
                modelDeliveryNote = _db.Find(Id);

               // modelDeliveryNote = new DeliveryNoteInfo();
            }
            else
            {
                modelDeliveryNote = new DeliveryNoteInfo();
                modelDeliveryNote.Delivery_Date = DateTime.Now.ToShortDateString();

               

                modelDeliveryNote.Delivery_Note_No = _db.GEN_DebitNo();

            }

            return PartialView(modelDeliveryNote);
        }
        public ActionResult ViewDeliverynoteDetailsList(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            DeliveryNoteInfo modelDeliveryNote = new DeliveryNoteInfo();
            _db = new DeliveryNoteServiceLayer(_dashboardData);

            if (Id != 0)
            {

                modelDeliveryNote.DeliveryDetails = _db.DeliveryNoteDetails(OrderId: 0, Id: Id);
                //modelReceiptNote.ReceiptDetails.Add(new ReceiptNoteTraInfo());
            }
            else
            {


                modelDeliveryNote.DeliveryDetails.Add(new DeliveryNoteInfo_Tra() {Quantity=0,Rate=0 });
            }


            return View(modelDeliveryNote);
        }
        public ActionResult MasterDetails(int CustomerId = 0,int OrderId=0)
        {
            var _UserLog = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            
            
           _db = new DeliveryNoteServiceLayer(_UserLog);
             DeliveryNoteMasterInfo modelMasterDetails = new DeliveryNoteMasterInfo();
             if (CustomerId != 0)
             {
                 var objcustomer = new ServiceLayer.Masters.LedgerMasterServiceLayer(_UserLog).Find(CustomerId);
                 var objorder = new ServiceLayer.Transactions.SalesOrderServiceLayer(_UserLog).Find(OrderId);
                 modelMasterDetails = new DeliveryNoteMasterInfo
                 {
                     Address = objcustomer.Address,
                     Contact_No = objcustomer.Contact_No,
                     Customer_Code = objcustomer.Code,
                     Email = objcustomer.Email_Id,
                     Name = objcustomer.Name,
                     Order_Date = objorder.Posting_Date.ToShortDateString()
                 };
             }
            else
             {
                 modelMasterDetails = new DeliveryNoteMasterInfo {Address="N/A",Contact_No="N/A",Customer_Code="N/A",Email="N/A",Name="N/A",Order_Date="N/A" };
             }
         
          
            return View(modelMasterDetails);
        }
        public ActionResult  BindSalesOrderNo(int CustomerId=0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new DeliveryNoteServiceLayer(_dashboardData);
            var data = _db.DDLBind(CustomerId: CustomerId);

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        private DeliveryNoteInfo BindDDL(DeliveryNoteInfo modelDeliveryNote)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            if (modelDeliveryNote != null)
            {
                modelDeliveryNote.ItemList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("Stock_Item");
                //modelDeliveryNote.SelsPersonList = new ServiceLayer.Common.CommonServiceLayer().DDLBind("Employee");
                modelDeliveryNote.CustomerList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("Customer");
               // modelDeliveryNote.SalesOrderList = new List<ViewModel.Common.DDLBind>();
            }
            return modelDeliveryNote;
        }

    }
}

﻿using HotelManagementErp_Main.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Transactions;
using ViewModel.Common;
using ServiceLayer.Transactions;
using ViewModel.Ledger;
namespace InventoryManagement.Areas.Transactions.Controllers
{
    public class PurchaseOrderController : InventoryBaseController
    {
        //
        // GET: /Transactions/PurchaseOrder/
        PurchaseOrderServiceLayer _db;
        public ActionResult Index()
        {
            int Val = Convert.ToInt32(TempData["SaveStatus"]);
            if (Val != null && Val != 0)
                ViewBag.SaveStatus = Val;
            return View();
        }
        public ActionResult List(string search = "")
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseOrderServiceLayer(_dashboardData);

            var lst = _db.List(search);
            return View(lst);
        }
        public ActionResult Create(long Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseOrderServiceLayer(_dashboardData);
            PurchaseOrderInfo modelPurchaseOrder;
            if (Id != 0)
            {
                modelPurchaseOrder = _db.Find(Id);
            }
            else
            {
                modelPurchaseOrder = new PurchaseOrderInfo();
                modelPurchaseOrder.Document_Date = DateTime.Today;
                modelPurchaseOrder.Request_Delivery_Date = DateTime.Today;
                modelPurchaseOrder.Order_Date = DateTime.Today;
                modelPurchaseOrder.Posting_Date = DateTime.Today;
                modelPurchaseOrder.Order_No = _db.GEN_OrderNo();
            }

            return View(BindDDL(modelPurchaseOrder));
        }

        private PurchaseOrderInfo FindPurchaseOrder(int Id, ViewModel.Users.LoginInfo _dashboardData)
        {
            _db = new PurchaseOrderServiceLayer(_dashboardData);
            PurchaseOrderInfo modelPurchaseOrder;


            if (Id != 0)
            {
                modelPurchaseOrder = _db.Find(Id);

            }
            else
            {
                modelPurchaseOrder = new PurchaseOrderInfo();
                modelPurchaseOrder.Posting_Date = DateTime.Now;
                modelPurchaseOrder.Request_Delivery_Date = DateTime.Now;
                modelPurchaseOrder.Order_Date = DateTime.Now;
                modelPurchaseOrder.Document_Date = DateTime.Now;
                modelPurchaseOrder.Order_No = _db.GEN_OrderNo();

            }
            return modelPurchaseOrder;
        }
        [HttpPost]
        public ActionResult Create(PurchaseOrderInfo modelPurchaseOrder)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseOrderServiceLayer(_dashboardData);

            //if (ModelState.IsValid == true || ModelState.IsValid == false)
            if (ModelState.IsValid)
            {
                if (modelPurchaseOrder.Id != 0)
                {
                    // _db.Update(modelPurchaseOrder); 
                    TempData["SaveStatus"] = 2;

                }
                else
                {

                    // _db.Create(modelPurchaseOrder); 
                    TempData["SaveStatus"] = 1;

                }

                // var lst = _db.List();
                // return View("List", lst);
                return RedirectToAction("Index");
            }
            else
            {
                BindDDL(modelPurchaseOrder);
                return View(modelPurchaseOrder);
            }
            return View();
        }
        public ActionResult OrderDetailsList(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            PurchaseOrderInfo modelPurchaseOrder = new PurchaseOrderInfo();
            _db = new PurchaseOrderServiceLayer(_dashboardData);

            if (Id != 0)
            {
                var Challan_No = _db.Find(Id).Id;
                modelPurchaseOrder.OrderDetails = _db.OrderDetails(Challan_No);
            }
            else
            {
                modelPurchaseOrder.OrderDetails.Add(new PurchaseOrderInfo_Tra());
            }


            return View(BindDDL(modelPurchaseOrder));
        }
        public ActionResult Delete(int Id)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseOrderServiceLayer(_dashboardData);
            var res = _db.Delete(Id);
            var lst = _db.List();

            return View("List", lst);
        }
        [HttpGet]
        public ActionResult PurchaseOrderByQuation(int QuationId = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseOrderServiceLayer(_dashboardData);
            PurchaseOrderInfo modelPurchaseOrder = new PurchaseOrderInfo();

            if (QuationId != 0)
            {

                modelPurchaseOrder = _db.FindByQuationId(QuationId);


            }


            return View(BindDDL(modelPurchaseOrder));
        }
        [HttpPost]
        public ActionResult PurchaseOrderByQuation(PurchaseOrderInfo modelPurchaseOrde)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseOrderServiceLayer(_dashboardData);

            bool result = false;
            if (modelPurchaseOrde != null)
            {

                result = _db.CreateByQuation(modelPurchaseOrde);
                return RedirectToAction("Index", "PurchaseQuation", new { area = "Transactions" });
                //var lst = _db.List();

                //return View("List", lst);
            }


            return View(BindDDL(modelPurchaseOrde));
        }
        public ActionResult OrderDetailsByQuationId(int QuationId = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            PurchaseOrderInfo modelPurchaseOrde = new PurchaseOrderInfo();
            _db = new PurchaseOrderServiceLayer(_dashboardData);
            if (QuationId != 0)
            {

                modelPurchaseOrde.OrderDetails = _db.OrderDetailsByQuationId(QuationId);
            }
            else
            {
                modelPurchaseOrde.OrderDetails.Add(new PurchaseOrderInfo_Tra());
            }


            return View(BindDDL(modelPurchaseOrde));
        }
        public ActionResult GetSupplierDetails(int SupplierId = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            ServiceLayer.AccountMaster.CustomerServicesLayer objcustomer = new ServiceLayer.AccountMaster.CustomerServicesLayer(_dashboardData);
            var data = objcustomer.FindCustomer(SupplierId, "Supplier");
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        private PurchaseOrderInfo BindDDL(PurchaseOrderInfo modelPurchaseOrder)
        {
            var _dashboardData = ViewBag.UserLog;
            if (modelPurchaseOrder != null)
            {
                modelPurchaseOrder.ItemList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("Stock_Item");
                modelPurchaseOrder.SelsPersonList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("Employee");
                modelPurchaseOrder.SupplierList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("Supplier");
            }
            return modelPurchaseOrder;
        }





        public PartialViewResult ViewPurodr(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseOrderServiceLayer(_dashboardData);
            PurchaseOrderInfo modelPurchaseOrder = _db.Find(Id);

            return PartialView(modelPurchaseOrder);
        }

        public ActionResult ViewOrderDetailsList(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            PurchaseOrderInfo modelPurchaseOrder = new PurchaseOrderInfo();
            _db = new PurchaseOrderServiceLayer(_dashboardData);

            if (Id != 0)
            {
                //var Challan_No = _db.Find(Id).Id;
                modelPurchaseOrder.OrderDetails = _db.OrderDetails(Id);
            }
            else
            {


                modelPurchaseOrder.OrderDetails.Add(new PurchaseOrderInfo_Tra() { Invoice_Quantity = 0, Rate = 0, Amount = 0 });
            }


            return View(modelPurchaseOrder);
        }
        public ActionResult ApproveOrderList(int Id)
        {
            var _dashboardData = ViewBag.UserLog;
            PurchaseOrderInfo info = new PurchaseOrderInfo();
            _db = new PurchaseOrderServiceLayer(_dashboardData);
            info = _db.Find(Id);
            info.OrderDetails = _db.OrderDetails(info.Id);
            PurchaseOrderItem OrderdItem = new ServiceLayer.Transactions.PurchaseServiceLayer(_dashboardData).GetPurchaseOrderItemList(info);
            TempData["PurchaseOrderDetails"] = OrderdItem;

            // return View("Transactions/Purchase/Create", OrderdItem);
            // return View(@Url.Action("Create", "Purchase", new { area = "Transactions" }), OrderdItem);
            return RedirectToAction("Create", "Purchase", new { OrderedItem = OrderdItem });
        }
    }
}

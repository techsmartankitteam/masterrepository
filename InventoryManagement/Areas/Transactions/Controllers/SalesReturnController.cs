﻿using HotelManagementErp_Main.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Transactions;
using ServiceLayer.Transactions;

namespace InventoryManagement.Areas.Transactions.Controllers
{
    public class SalesReturnController : InventoryBaseController
    {
        //
        // GET: /Transactions/SalesReturn/
        SalesReturnServiceLayer _db;
        public ActionResult Index()
        {
            return View();


        }
        public ActionResult List(string search = "")
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new SalesReturnServiceLayer(_dashboardData);

            var lst = _db.List(search);
            return View(lst);
        }
        public ActionResult Create(long Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new SalesReturnServiceLayer(_dashboardData);
            SalesReturnInfo ModelSalesRet;
            if (Id > 0)
            {
                ModelSalesRet = _db.Find(Id);
            }
            else
            {
                ModelSalesRet = new SalesReturnInfo();
                ModelSalesRet.VC_No = _db.GEN_VC_No();
                ModelSalesRet.Return_Date = DateTime.Today;
            }
            DDLBind(ModelSalesRet);
            return View(ModelSalesRet);
        }
        [HttpPost]
        public ActionResult Create(SalesReturnInfo ModelSalesRet)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new SalesReturnServiceLayer(_dashboardData);
            if (ModelState.IsValid || ModelState.IsValid == false)
            {
                if (ModelSalesRet.Id > 0)
                {
                    _db.Update(ModelSalesRet);
                }
                else
                {
                    _db.Create(ModelSalesRet);
                }
                var lst = _db.List();
                return View("List", lst);
            }
            DDLBind(ModelSalesRet);
            return View(ModelSalesRet);
        }
        public ActionResult SalesReturnDetails(long Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new SalesReturnServiceLayer(_dashboardData);
            SalesReturnInfo ModelSalesRet;


            if (Id > 0)
            {
                ModelSalesRet = _db.Find(Id);
                ModelSalesRet.ReturnDetails = _db.ReturnDetails(Id);

            }
            else
            {
                ModelSalesRet = new SalesReturnInfo();
                ModelSalesRet.ReturnDetails.Add(new SalesReturnInfo_Tra());
            }

            DDLBind(ModelSalesRet);
            return View(ModelSalesRet);
        }
        public PartialViewResult ViewSaleReturn(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new SalesReturnServiceLayer(_dashboardData);

            SalesReturnInfo ModelSalesRet = new SalesReturnInfo();
            if (Id != 0)
            {
                ModelSalesRet = _db.Find(Id);
            }


            return PartialView(ModelSalesRet);
        }
        public ActionResult GetItemDetail(long Id)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new SalesReturnServiceLayer(_dashboardData);
            var item = new ServiceLayer.InventoryMaster.StockServiceLayer(_dashboardData).Find(Id);
            return Json(item, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetBillList(long Customer_Id)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new SalesReturnServiceLayer(_dashboardData);
            var l = _db.GetBillList(Customer_Id);
            return Json(l, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CheckSerialNo(string SerialNo, int Sales_Id, int Item_Id)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new SalesReturnServiceLayer(_dashboardData);
            var result = _db.CheckSerialNo(SerialNo, Sales_Id, Item_Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ViewSalesReturnDetailsList(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            SalesReturnInfo ModelSalesRet = new SalesReturnInfo();
            _db = new SalesReturnServiceLayer(_dashboardData);

            if (Id != 0)
            {
                //var Challan_No = _db.Find(Id).Id;
                ModelSalesRet.ReturnDetails = _db.ReturnDetails(Id);
            }
            else
            {


                ModelSalesRet.ReturnDetails.Add(new SalesReturnInfo_Tra() { Quantity = 0.00m, Total_Amount = 0.00m });
            }


            return View(ModelSalesRet);
        }
        private void DDLBind(SalesReturnInfo ModelSalesRet)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new SalesReturnServiceLayer(_dashboardData);
            //  ModelSalesRet.ItemList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData.DbConnectionString).DDLBind("Stock_Item");
            ModelSalesRet.CustomerList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("Customer");

        }
        public ActionResult Delete(long Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new SalesReturnServiceLayer(_dashboardData);


            bool result = false;
            if (Id > 0)
            {
                result = _db.Delete(Id);

            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Added by anurag
        /// </summary>
        /// <param name="Purchase_Id"></param>
        /// <returns></returns>
        public ActionResult GetStockItemForSupplier(long Sales_Id)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new SalesReturnServiceLayer(_dashboardData);
            var l = _db.GetStockItemForSupplier(Sales_Id);
            return Json(l, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Added by anurag to check for return Serial numbers.
        /// </summary>
        /// <param name="SerialNo"></param>
        /// <returns></returns>
        public ActionResult CheckIsReturnedSerialNo(string SerialNo, int Item_Id, int Sales_Id)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new SalesReturnServiceLayer(_dashboardData);
            var result = _db.CheckIsReturnedSerialNo(SerialNo, Item_Id, Sales_Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckValidQty(int Customer_Id, int Sales_Id, int Item_Id)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new SalesReturnServiceLayer(_dashboardData);
            var result = _db.CheckValidQty(Customer_Id, Sales_Id, Item_Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Added by anurag dated-01-07-2016 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult GetItemRate(int Item_Id, int Sales_Id)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new SalesReturnServiceLayer(_dashboardData);
            //  var item = new ServiceLayer.InventoryMaster.StockServiceLayer(_dashboardData.DbConnectionString).Find(Id);
            var result = _db.GetItemRate(Item_Id, Sales_Id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}

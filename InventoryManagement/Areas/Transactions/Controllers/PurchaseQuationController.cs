﻿using HotelManagementErp_Main.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ServiceLayer.Transactions;
using ViewModel.Transactions;
namespace InventoryManagement.Areas.Transactions.Controllers
{
    public class PurchaseQuationController : InventoryBaseController
    {
        //
        // GET: /Transactions/PurchaseQuation/
        PurchaseQuationServiceLayer _db;
        public ActionResult Index()
        {
            int Val = Convert.ToInt32(TempData["SaveStatus"]);
            if (Val != null && Val != 0)
                ViewBag.SaveStatus = Val;
            return View();
        }

        public ActionResult List(string search = "")
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseQuationServiceLayer(_dashboardData);

            var lst = _db.List(search);
            return View(lst);
        }
        public ActionResult Create(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseQuationServiceLayer(_dashboardData);
            PurchaseQuationInfo modelQuation;


            if (Id != 0)
            {
                modelQuation = _db.Find(Id);

            }
            else
            {
                modelQuation = new PurchaseQuationInfo();
                modelQuation.Quation_Chalan_No = _db.GEN_ChallanNo();

            }
            modelQuation.CustomerList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("SUPPLIER", "").ToList();
            return View(modelQuation);
        }
        [HttpPost]
        public ActionResult Create(PurchaseQuationInfo modelQuation)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseQuationServiceLayer(_dashboardData);

            if (ModelState.IsValid == true || ModelState.IsValid == false)
            {
                if (modelQuation.Id != 0)
                {
                    _db.Update(modelQuation);
                    TempData["SaveStatus"] = 2;
                }
                else
                {

                    _db.Create(modelQuation);
                    TempData["SaveStatus"] = 1;
                }

                // var lst = _db.List();
                // return View("List", lst);
                return RedirectToAction("Index");
            }
            modelQuation.CustomerList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("SUPPLIER", "").ToList();
            return View(modelQuation);
        }

        public ActionResult QuationItemList(long Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            PurchaseQuationInfo modelQuation = new PurchaseQuationInfo();
            _db = new PurchaseQuationServiceLayer(_dashboardData);
            ServiceLayer.Common.CommonServiceLayer _objCommon = new ServiceLayer.Common.CommonServiceLayer(_dashboardData);
            if (Id != 0)
            {
                // var Challan_No = _db.Find(Id).Id;
                modelQuation.QuationItemList = _db.QuationDetails(Id);
            }
            else
            {
                modelQuation.QuationItemList.Add(new PurchaseQuationInfo_Tra() { Qty = 0 });
            }
            modelQuation.ItemList = new ServiceLayer.Common.CommonServiceLayer(_dashboardData).DDLBind("Stock_Item");

            return View(modelQuation);
        }
        public ActionResult Delete(long Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseQuationServiceLayer(_dashboardData);
            if (Id > 0)
            {
                _db.Delete(Id);
            }

            var lst = _db.List();

            return View("List", lst);
        }
        public PartialViewResult ViewPurchaseQuation(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseQuationServiceLayer(_dashboardData);
            PurchaseQuationInfo modelQuation;


            if (Id != 0)
            {
                modelQuation = _db.Find(Id);

            }
            else
            {
                modelQuation = new PurchaseQuationInfo();


                modelQuation.Quation_Chalan_No = _db.GEN_ChallanNo();

            }

            return PartialView(modelQuation);
        }
        public ActionResult ViewPurQuationDetailsList(long Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseQuationServiceLayer(_dashboardData);
            PurchaseQuationInfo modelQuation = new PurchaseQuationInfo();
            if (Id != 0)
            {

                modelQuation.QuationItemList = _db.QuationDetails(Id);
            }
            else
            {


                modelQuation.QuationItemList.Add(new PurchaseQuationInfo_Tra() { Qty = 0.00m, Final_Rate = 0 });
            }


            return View(modelQuation);
        }
        public ActionResult ApproveQuation(int QuationId = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new PurchaseQuationServiceLayer(_dashboardData);
            //  var res= _db.ApproveQuation(QuationId);
            return RedirectToAction("PurchaseOrderByQuation", "PurchaseOrder", new { QuationId = QuationId });
        }
        private PurchaseQuationInfo BindDDL(PurchaseQuationInfo modelQuation)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            if (modelQuation != null)
            {

                modelQuation.ItemList = new ServiceLayer.InventoryMaster.StockServiceLayer(_dashboardData).DDLBind();

            }
            return modelQuation;
        }
    }
}

﻿
using HotelManagementErp_Main.Helper;
using ServiceLayer.InventoryMaster;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Stock;

namespace InventoryManagement.Areas.InventoryMaster.Controllers
{
    public class StockItemController : InventoryBaseController
    {
        //
        // GET: /InventoryMaster/StockItem/
        StockServiceLayer objStockServiceLayer;
        public ActionResult Index()
        {
            int Val = Convert.ToInt32(TempData["SaveStatus"]);
            if (Val != null && Val != 0)
                ViewBag.SaveStatus = Val;
            return View();
        }
        public ActionResult List(string search = "")
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            objStockServiceLayer = new StockServiceLayer(_dashboardData);
            var data = objStockServiceLayer.GetStock(search);
            return View(data);
        }
        public ActionResult Create(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            objStockServiceLayer = new StockServiceLayer(_dashboardData);

            StockItem stock;

            if (Id != 0)
            {
                stock = new StockItem();
                stock = objStockServiceLayer.Find(Id);
                DdlBind(stock);
            }
            else
            {
                stock = new StockItem();
                stock.Item_Code = objStockServiceLayer.GEN_ItemCode();
                DdlBind(stock);

            }
            return View(stock);
        }
        private void DdlBind(StockItem stock)
        {

            var _dashboardData = ViewBag.UserLog;
            stock.UnitList = new ServiceLayer.Masters.UnitsServiceLayer(_dashboardData).DDLBind();
            stock.CategoryList = new StockCategoryServiceLayer(_dashboardData).DDLBind();
            stock.GroupList = new StockGroupServicelayer(_dashboardData).DDLlBind();
        }
        [HttpPost]
        public ActionResult Create(ViewModel.Stock.StockItem modelStockitem, HttpPostedFileBase file1)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            objStockServiceLayer = new StockServiceLayer(_dashboardData);
            if (ModelState.IsValid)
            {
                if (modelStockitem.Id != 0)
                {

                    if (file1 != null)
                    {
                        var allowedExtensions = new[] { ".Jpg", ".png", ".jpg", "jpeg" };

                        var ext = Path.GetExtension(file1.FileName);
                        if (allowedExtensions.Contains(ext))
                        {

                            string pic = System.IO.Path.GetFileName(file1.FileName);
                            string path = System.IO.Path.Combine(Server.MapPath("~/Themes/img/StockItemImg"), pic);

                            file1.SaveAs(path);

                            modelStockitem.Image_Path = "/Themes/img/StockItemImg/" + pic;

                        }
                    }
                    objStockServiceLayer.Update(modelStockitem);
                    TempData["SaveStatus"] = 2;
                }
                else
                {

                    if (file1 != null)
                    {
                        var allowedExtensions = new[] { ".Jpg", ".png", ".jpg", "jpeg" };

                        var ext = Path.GetExtension(file1.FileName);
                        if (allowedExtensions.Contains(ext))
                        {

                            string pic = System.IO.Path.GetFileName(file1.FileName);
                            string path = System.IO.Path.Combine(Server.MapPath("~/Themes/img/StockItemImg"), pic);

                            file1.SaveAs(path);

                            modelStockitem.Image_Path = "/Themes/img/StockItemImg/" + pic;

                        }
                    }
                    objStockServiceLayer.Create(modelStockitem);
                    TempData["SaveStatus"] = 1;
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            DdlBind(modelStockitem);
            return View(modelStockitem);
        }
        public ActionResult Delete(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            objStockServiceLayer = new StockServiceLayer(_dashboardData);
            bool result = false;
            if (Id > 0)
            {
                result = objStockServiceLayer.Delete(Id);
            }
            var data = objStockServiceLayer.GetStock().ToList();
            return PartialView("List", data);
        }
        public ActionResult IsNameExists(int Id, string Name)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;

            objStockServiceLayer = new StockServiceLayer(_dashboardData);

            return Json(!objStockServiceLayer.IsNameExists(Id, Name), JsonRequestBehavior.AllowGet);
        }


        public ActionResult ViewStockItem(int Id)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            objStockServiceLayer = new StockServiceLayer(_dashboardData);

            StockItem stock;

            if (Id != 0)
            {
                stock = new StockItem();
                stock = objStockServiceLayer.Find(Id);
                DdlBind(stock);
            }
            else
            {
                stock = new StockItem();
                stock.Item_Code = objStockServiceLayer.GEN_ItemCode();
                DdlBind(stock);

            }
            return PartialView(stock);
        }

        public ActionResult OpeningQty(decimal OpeningQty = 0, string Prefix = "", string Suffix = "", string Unit = "", string IsAuto = "NO", int StartFrom = 0, long Id = 0, decimal Rate = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            objStockServiceLayer = new StockServiceLayer(_dashboardData);
            List<DataDependonOpeningBal> obj = new List<DataDependonOpeningBal>();
            StockItem stock = new StockItem();
            DataDependonOpeningBal OpeningData;
            stock.OpeningBal.Clear();
            obj.Clear();
            if (Id == 0)
            {
                string SerialNO;
                if (IsAuto == "YES")
                {
                    for (int i = 0; i < OpeningQty; i++)
                    {
                        SerialNO = Prefix + StartFrom + Suffix;
                        OpeningData = new DataDependonOpeningBal();
                        OpeningData.SerialNo = SerialNO;
                        OpeningData.Purchase = SerialNO;
                        OpeningData.Sale = SerialNO;
                        OpeningData.Amount = Math.Round(1 * Rate, 2);
                        OpeningData.Quantity = Math.Round(Convert.ToDecimal(1), 2);
                        OpeningData.Rate = Rate;
                        OpeningData.Unit = Unit;
                        obj.Add(OpeningData);
                        StartFrom = StartFrom + 1;
                    }
                    stock.OpeningBal.AddRange(obj);
                }
                else if (IsAuto == "NO")
                {
                    OpeningData = new DataDependonOpeningBal();
                    OpeningData.SerialNo = Prefix + StartFrom + Suffix;
                    OpeningData.Sale = "xxxx";
                    OpeningData.Purchase = "xxxx";
                    OpeningData.Amount = Math.Round(OpeningQty * Rate, 2);
                    OpeningData.Quantity = Math.Round(Convert.ToDecimal(OpeningQty), 2);
                    OpeningData.Rate = Rate;
                    OpeningData.Unit = Unit;
                    obj.Add(OpeningData);
                    stock.OpeningBal.AddRange(obj);
                }
            }
            else
            {
                obj = objStockServiceLayer.OpeningBalData(Id);
                if (obj.Count > 0)
                {
                    stock.OpeningBal.AddRange(obj);
                }
            }
            return View(stock);
        }
    }
}

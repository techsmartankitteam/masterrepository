﻿using HotelManagementErp_Main.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Category;
using ViewModel.Common;
using ServiceLayer.InventoryMaster;
namespace InventoryManagement.Areas.InventoryMaster.Controllers
{
    public class StockGroupController : InventoryBaseController
    {
        //
        // GET: /InventoryMaster/StockGroup/
        StockGroupServicelayer _db;

        public ActionResult Index()
        {
            int val = Convert.ToInt32(TempData["SaveStatus"]);
            if (val != null && val != 0)
            {
                ViewBag.SaveStatus = val;

            }
            return View();
        }

        public ActionResult List(string search = "")
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;

            _db = new StockGroupServicelayer(_dashboardData);

            var lst = _db.List(search);

            return View(lst);


        }
        //[ChildActionOnly]
        public ActionResult Create(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new StockGroupServicelayer(_dashboardData);
            //ViewBag.House = new SelectList(house.List(), "Id", "House_Name");
            ViewBag.Grp = new SelectList(new ServiceLayer.AccountMaster.CustomerServicesLayer(_dashboardData).DDLBind("STOCKGROUP", ""), "Id", "Name");
            ViewModel.Category.StockGroup modelgroup;
            if (Id != 0)
            {
                modelgroup = _db.Find(Id);

            }
            else
            {
                modelgroup = new ViewModel.Category.StockGroup();
            }
            return View(modelgroup);
        }
        public ActionResult GetStockGroup(string searchText = "")
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new StockGroupServicelayer(_dashboardData);

            var data = new ServiceLayer.AccountMaster.CustomerServicesLayer(_dashboardData).DDLBind("STOCKGROUP", searchText);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Create(ViewModel.Category.StockGroup modelgroup)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new StockGroupServicelayer(_dashboardData);


            if (ModelState.IsValid)
            // if (ModelState.IsValid == false || ModelState.IsValid == true)
            {
                if (modelgroup.Group.Id != 0)
                {
                    _db.Update(modelgroup);
                    TempData["SaveStatus"] = 2;
                }
                else
                {
                    _db.Create(modelgroup);
                    TempData["SaveStatus"] = 2;
                }

                // var lst = _db.List();

                // return View("List",lst);
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Grp = new SelectList(new ServiceLayer.AccountMaster.CustomerServicesLayer(_dashboardData).DDLBind("STOCKGROUP", ""), "Id", "Name");
                return View(modelgroup);
            }

        }
        public ActionResult Delete(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new StockGroupServicelayer(_dashboardData);
            bool result = false;
            if (Id > 0)
            {
                result = _db.Delete(Id);
                TempData["SaveStatus"] = 3;
            }
            //  var lst = _db.List();
            //  return PartialView("List", lst);
            return RedirectToAction("Index");
        }
        public ActionResult IsNameExists(int Id, string Name)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new StockGroupServicelayer(_dashboardData);

            return Json(_db.IsNameExists(Id, Name), JsonRequestBehavior.AllowGet);
        }
    }
}

﻿using HotelManagementErp_Main.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Category;
using ViewModel.Common;
using ServiceLayer.InventoryMaster;
namespace InventoryManagement.Areas.InventoryMaster.Controllers
{
    public class CategoryController : InventoryBaseController
    {
        //
        // GET: /Masters/Category/
        StockCategoryServiceLayer _db;
        public ActionResult Index()
        {
            int val = Convert.ToInt32(TempData["SaveStatus"]);
            if (val != null && val != 0)
            {
                ViewBag.SaveStatus = val;
            }
            var _dashboardData = ViewBag.UserLog;
            _db = new StockCategoryServiceLayer(_dashboardData);

            return View();
        }
        public ActionResult List(string search = "")
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new StockCategoryServiceLayer(_dashboardData);
            var lst = _db.List(search);

            return View(lst);


        }
        public ActionResult Create(long Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new StockCategoryServiceLayer(_dashboardData);
            StockCategory Category;
            // ViewBag.Category = new SelectList(new ServiceLayer.AccountMaster.CustomerServicesLayer().DDLBind("STOCKCATEGORY", ""), "Id", "Name");
            if (Id != 0)
            {
                Category = _db.Find(Id);
            }
            else
            {
                Category = new StockCategory();
            }
            Category.Category.ListUnder = _db.DDLBind();
            return View(Category);
        }
        [HttpPost]
        public ActionResult Create(ViewModel.Category.StockCategory ModelCategory)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new StockCategoryServiceLayer(_dashboardData);
            if (ModelState.IsValid || ModelState.IsValid == false)
            {
                if (ModelCategory.Category.Id != 0)
                {
                    _db.Update(ModelCategory);
                    TempData["SaveStatus"] = 2;
                }
                else
                {
                    _db.Create(ModelCategory);
                    TempData["SaveStatus"] = 1;
                }


                // var lst = _db.List();
                // return View("List", lst);
                return RedirectToAction("Index");
            }
            else
            {
                //ViewBag.Category = new SelectList(new ServiceLayer.AccountMaster.CustomerServicesLayer().DDLBind("STOCKCATEGORY", ""), "Id", "Name");
                ModelCategory.Category.ListUnder = _db.DDLBind();
                return View(ModelCategory);
            }

        }
        public ActionResult Delete(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new StockCategoryServiceLayer(_dashboardData);
            bool result = false;
            if (Id > 0)
            {
                result = _db.Delete(Id);
                TempData["SaveStatus"] = 3;
            }
            //  var lst = _db.List();
            // return PartialView("List", lst);
            return RedirectToAction("Index");
        }
        public ActionResult IsNameExists(int Id, string Name)
        {
            var _dashboardData = ViewBag.UserLog;
            _db = new StockCategoryServiceLayer(_dashboardData);

            return Json(_db.IsNameExists(Id, Name), JsonRequestBehavior.AllowGet);
        }
    }
}

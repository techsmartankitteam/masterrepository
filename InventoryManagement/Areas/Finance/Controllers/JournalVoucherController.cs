﻿using HotelManagementErp_Main.Helper;
using System;
using System.Collections.Generic;
//using HotelManagementErp_Main.Helper;
//using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Finance;
using ViewModel.Common;
using ServiceLayer.Finance;
namespace InventoryManagement.Areas.Finance.Controllers
{
    public class JournalVoucherController : InventoryBaseController
    {
        //
        // GET: /Finance/JournalVoucher/
        JournalVoucherSL _db;

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List(string search = "")
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new JournalVoucherSL(info);
            var lst = _db.List(search);
            return View(lst);
        }
        [HttpGet]
        public ActionResult Create(long Id = 0)
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new JournalVoucherSL(info);
            JournalVoucherInfo data = new JournalVoucherInfo();
            //  SalesReturnInfo ModelSalesRet;
            if (Id > 0)
            {
                data = _db.Find(Id);
            }
            else
            {

                data.Journal_VC_No = _db.Journal_VC_No();
                // data.Return_Date = DateTime.Today;
            }
            DDLBind(data);
            return View(data);
        }
        [HttpPost]
        public ActionResult Create(JournalVoucherInfo ModelJournalVC)
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new JournalVoucherSL(info);
            if (ModelState.IsValid || ModelState.IsValid == false)
            {
                if (ModelJournalVC.Id > 0)
                {
                    _db.Update(ModelJournalVC);
                    TempData["SaveStatus"] = 2;
                }
                else
                {
                    _db.Create(ModelJournalVC);
                    TempData["SaveStatus"] = 1;
                }
                //var lst = _db.List();
                //return View("List", lst);
                return RedirectToAction("Index");
            }
            DDLBind(ModelJournalVC);
            return View(ModelJournalVC);
        }

        public ActionResult BindLedgers(long Sales_Id = 0)
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new JournalVoucherSL(info);
            var l = new ServiceLayer.Common.CommonServiceLayer(info).DDLBind("All");
            return Json(l, JsonRequestBehavior.AllowGet);

        }

        private void DDLBind(JournalVoucherInfo BindLedger)
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new JournalVoucherSL(info);
            BindLedger.CustomerList = new ServiceLayer.Common.CommonServiceLayer(info).DDLBind("All");

        }

        public ActionResult CreditDebitList(int Id = 0)
        {
            // var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            // _db = new JournalVoucherSL(info);
            //PurchaseOrderInfo modelPurchaseOrder = new PurchaseOrderInfo();

            //if (Id != 0)
            //{
            //    var Challan_No = _db.Find(Id).Id;
            //    modelPurchaseOrder.OrderDetails = _db.OrderDetails(Challan_No);
            //}
            //else
            //{
            //    modelPurchaseOrder.OrderDetails.Add(new PurchaseOrderInfo_Tra());
            //}


            // return View(BindDDL(modelPurchaseOrder));
            return View();
        }
    }
}

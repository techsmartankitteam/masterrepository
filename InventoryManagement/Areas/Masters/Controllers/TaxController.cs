﻿using HotelManagementErp_Main.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Common;
using ServiceLayer.Masters;
namespace InventoryManagement.Areas.Masters.Controllers
{
    public class TaxController : InventoryBaseController
    {
        //
        // GET: /Masters/TaxMaster/
        TaxServiceLayer _db;
        public ActionResult Index()
        {
            int val = Convert.ToInt32(TempData["SavedStatus"]);
            if (val != null && val != 0)
            {
                ViewBag.SaveStatus = val;
            }
            return View();
        }
        public ActionResult List(string Search = "")
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new TaxServiceLayer(info);
            var lst = _db.List(Search);
            return View(lst);
        }
        public ActionResult Create(int Id = 0)
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new TaxServiceLayer(info);
            Tax tax;
            if (Id != 0)
            {
                tax = _db.Find(Id);
            }
            else
            {
                tax = new Tax();
                tax.Code = _db.GEN_TaxCode();
            }
            return View(tax);
        }
        [HttpPost]
        public ActionResult Create(Tax modeltax)
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new TaxServiceLayer(info);
            var Status = false;
            if (ModelState.IsValid || ModelState.IsValid == false)
            {
                if (modeltax.Id != 0)
                {
                    Status = _db.Update(modeltax);
                    TempData["SavedStatus"] = 2;
                }
                else
                {

                    Status = _db.Create(modeltax);
                    TempData["SavedStatus"] = 1;
                }
                if (Status)
                    return View("Index");
                else
                    return View(modeltax);
            }
            return View(modeltax);
        }
        public ActionResult GetTaxRate(int Id = 0)
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new TaxServiceLayer(info);
            var result = 0.0m;
            if (Id != 0)
            {
                var tax = _db.Find(Id);
                result = tax != null ? tax.Rate : 0.00m;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int Id = 0)
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new TaxServiceLayer(info);
            bool result = false;
            if (Id > 0)
            {
                result = _db.Delete(Id);
            }
            var lst = _db.List();
            return PartialView("List", lst);
        }
        public ActionResult IsNameExists(int Id, string Name)
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new TaxServiceLayer(info);

            return Json(!_db.IsNameExists(Id, Name), JsonRequestBehavior.AllowGet);
        }
    }
}

﻿using HotelManagementErp_Main.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Common;
using ServiceLayer.Masters;
namespace InventoryManagement.Areas.Masters.Controllers
{
    public class FinancialYearController : InventoryBaseController
    {
        //
        // GET: /Masters/FinancialYear/
        FinancialYearServiceLayer _db;
        public ActionResult Index()
        {
            int val = Convert.ToInt32(TempData["SavedStatus"]);
            if (val != null && val != 0)
            {
                ViewBag.SaveStatus = val;
            }
            return View();
        }

        public ActionResult List(string search = "")
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new FinancialYearServiceLayer(info);
            var lst = _db.List(search);
            return View(lst);
        }
        public ActionResult Create(int Id = 0)
        {
            FinancialYear Fnclyear;
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new FinancialYearServiceLayer(info);
            if (Id != 0)
            {
                Fnclyear = _db.Find(Id);
            }
            else
            {
                Fnclyear = new FinancialYear();
            }
            return View(Fnclyear);
        }
        [HttpPost]
        public ActionResult Create(ViewModel.Common.FinancialYear modelFincial)
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new FinancialYearServiceLayer(info);

            if (ModelState.IsValid)
            {
                if (modelFincial.Id != 0)
                {
                    _db.Update(modelFincial);
                    TempData["SavedStatus"] = 2;
                }
                else
                {
                    _db.Create(modelFincial);
                    TempData["SavedStatus"] = 1;
                }
                // var lst = _db.List();
                return View("Index");
            }
            return View(modelFincial);
        }
        public ActionResult IsNameExists(int Id, string Name)
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new FinancialYearServiceLayer(info);
            return Json(!_db.IsNameExists(Id, Name), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int Id = 0)
        {
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new FinancialYearServiceLayer(info);
            bool result = false;
            if (Id > 0)
            {
                result = _db.Delete(Id);
                TempData["SavedStatus"] = 3;
            }
            // var lst = _db.List();
            // return View("List", lst);

            return View("Index");
        }
    }
}

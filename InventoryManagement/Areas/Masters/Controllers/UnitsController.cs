﻿using HotelManagementErp_Main.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Common;
using ServiceLayer.Masters;
namespace InventoryManagement.Areas.Masters.Controllers
{
    public class UnitsController : InventoryBaseController
    {
        //
        // GET: /Masters/Units/
        UnitsServiceLayer _db;
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ActionResult Index()
        {
        logger.Info("Unit Master Index Page Debugging Started");
            return View();
        }
        public ActionResult List(string search = "")
        {
            logger.Info("Unit Master List View Page Debugging Started");
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new UnitsServiceLayer(info);
            var lst = _db.List(search);

            return View(lst);
        }
        public ActionResult Create(int Id = 0)
        {
            logger.Info("Unit Master Create Get Method Debugging Started");
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new UnitsServiceLayer(info);
            Unit unit;

            if (Id != 0)
            {
                unit = _db.Find(Id);
            }
            else
            {
                unit = new Unit();
            }
            return View(unit);
        }
        [HttpPost]
        public ActionResult Create(Unit  unit)
        {
            logger.Info("Unit Master Create POST Method Debugging Started");
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new UnitsServiceLayer(info);
            if (ModelState.IsValid)
            {
                if (unit.Id!=0)
                {
                    _db.Update(unit);
                }
                else
                {
                   _db.Create(unit);
                }
                
                var lst = _db.List();

                return View("Index");
            }

            else
                return View(unit);
        }

        public ActionResult IsNameExists(int Id, string Name)
        {
            logger.InfoFormat("Unit Master IsNameExits  Method with 2 Paremeter Id #{0} and Name#{1} Debugging Started",Id,Name);
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new UnitsServiceLayer(info);

            return Json(!_db.IsNameExists(Id, Name), JsonRequestBehavior.AllowGet);
        }
        public ActionResult SubUnit(long Id = 0)
        {
            logger.InfoFormat("Unit Master Subunite Get  Method with 1 Paremeter Id #{0}  Debugging Started", Id);
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new UnitsServiceLayer(info);
            Unit unit=new Unit();
            if(Id!=0)
            {
                var lst=_db.FindSubUnit(Id);
                if (lst.Count >0 )
                {
                    unit.SubUnitList = lst;
                }
                else
                unit.SubUnitList.Add(new SubUnit());
                    
            }
            else
            {
                unit.SubUnitList.Add(new SubUnit());
                
            }
            return PartialView(unit);
        }
        public ActionResult Delete(int Id=0)
        {
            logger.InfoFormat("Unit Master Delete  Method calling with Id #{0} Parameter", Id);
            var info = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new UnitsServiceLayer(info); ;
            bool result = false;
            if (Id > 0)
            {
                result = _db.Delete(Id);
            }
            var lst = _db.List();
            return PartialView("List",lst);
        }

    }
}

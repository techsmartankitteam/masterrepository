﻿using HotelManagementErp_Main.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel;
using ViewModel.Common;
using ServiceLayer.Masters;
using ServiceLayer.Reporthelper.CSVHelper;
using System.Text;
using System.IO;
namespace InventoryManagement.Areas.Masters.Controllers
{
    public class LedgerGroupController : InventoryBaseController
    {
        //
        // GET: /Masters/Ledger/
        LedgerGroupServiceLayer _db;
        public ActionResult Index()
        {
            int val = Convert.ToInt32(TempData["SaveStatus"]);
            if (val != null && val != 0)
            {
                ViewBag.SaveStatus = val;
            }
            return View();
        }
        private Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
        public ActionResult List(string search = "")
        {

            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new LedgerGroupServiceLayer(_dashboardData);
            var lst = _db.List(search);

            return View(lst);

        }
        public ActionResult Create(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new LedgerGroupServiceLayer(_dashboardData);
            ViewModel.LadgerGroup.LedgerGroup Lggrp;

            if (Id != 0)
            {
                Lggrp = _db.Find(Id);
                Lggrp.Ledger.ListUnder = _db.DDLBind("LEDGERGROUP", "");
            }
            else
            {
                Lggrp = new ViewModel.LadgerGroup.LedgerGroup();
                Lggrp.Ledger.ListUnder = _db.DDLBind("LEDGERGROUP", "");
            }
            return View(Lggrp);
        }
        [HttpPost]
        public ActionResult Create(ViewModel.LadgerGroup.LedgerGroup modelLedggrp)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new LedgerGroupServiceLayer(_dashboardData);
            if (ModelState.IsValid)
            {
                if (modelLedggrp.Ledger.Id != 0)
                {
                    _db.Update(modelLedggrp);
                    TempData["SaveStatus"] = 2;

                }
                else
                {
                    _db.create(modelLedggrp);
                    TempData["SaveStatus"] = 1;
                }
                //  var lst = _db.List();
                // return View("Index");
                return RedirectToAction("Index");
            }
            else
            {
                modelLedggrp.Ledger.ListUnder = _db.DDLBind("LEDGERGROUP", "");
                return View(modelLedggrp);
            }
        }
        public ActionResult IsNameExists(ViewModel.LadgerGroup.LedgerGroup model)
        {

            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new LedgerGroupServiceLayer(_dashboardData);
            return Json(!_db.IsNameExists(model.Ledger.Id, model.Ledger.Name), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new LedgerGroupServiceLayer(_dashboardData);
            bool result = false;
            if (Id > 0)
            {
                result = _db.Delete(Id);
                TempData["SaveStatus"] = 3;
            }
            //  var lst = _db.List();
            // return PartialView("List", lst);
            return RedirectToAction("Index");
        }

        public ActionResult GetLedger(string searchText = "")
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new LedgerGroupServiceLayer(_dashboardData);
            var data = _db.DDLBind("LEDGERGROUP", searchText).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}

﻿using HotelManagementErp_Main.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Common;
using ServiceLayer.Masters;
namespace InventoryManagement.Areas.Masters.Controllers
{
    public class RegionController : InventoryBaseController
    {
        //
        // GET: /Masters/Region/
        RegionServiceLayer _db;
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List(string search = "")
        {
            var info = ViewBag.UserLog;
            _db = new RegionServiceLayer(info);
            var lst = _db.List(search);
            return View(lst);
        }
        public ActionResult Create(int Id = 0)
        {
            var info = ViewBag.UserLog;
            _db = new RegionServiceLayer(info);

            RegionInfo modelRegionInfo;
            if (Id != 0)
            {
                modelRegionInfo = _db.Find(Id);
            }
            else
            {
                modelRegionInfo = new RegionInfo();
              
            }
            modelRegionInfo.GoDownList = new ServiceLayer.Common.CommonServiceLayer(info).DDLBind("GoDown_Master","");
            modelRegionInfo.UnderList = new ServiceLayer.Common.CommonServiceLayer(info).DDLBind("GoDown_Region","");
            return View(modelRegionInfo);
        }
        [HttpPost]
        public ActionResult Create(RegionInfo modelRegionInfo)
        {
            var info = ViewBag.UserLog;
            _db = new RegionServiceLayer(info);
            if (ModelState.IsValid)
            {
                if (modelRegionInfo.Id != 0)
                {
                    _db.Update(modelRegionInfo);
                }
                else
                {

                    _db.Create(modelRegionInfo);
                }
                var lst = _db.List();
                return View("Index");
            }
            modelRegionInfo.GoDownList = new ServiceLayer.Common.CommonServiceLayer(info).DDLBind("GoDown");
            return View(modelRegionInfo);
        }
        public ActionResult Delete(int Id=0)
        {
            var info = ViewBag.UserLog;
            _db = new RegionServiceLayer(info);
            bool result = false;
            if (Id > 0)
            {
                result = _db.Delete(Id);
            }
            var lst = _db.List();
            return View("List", lst);
        }
    }
}

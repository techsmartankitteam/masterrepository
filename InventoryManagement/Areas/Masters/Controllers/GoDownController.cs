﻿using HotelManagementErp_Main.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Common;
using ServiceLayer.Masters;
using ServiceLayer.Common;
namespace InventoryManagement.Areas.Masters.Controllers
{
    public class GoDownController : InventoryBaseController
    {
        //
        // GET: /Masters/GoDown/
        GoDownServiceLayer _db;
        public ActionResult Index()
        {
            int val = Convert.ToInt32(TempData["SaveStatus"]);
            if (val != null && val != 0)
            {
                ViewBag.SaveStatus = val;
            }
            return View();
        }
        public ActionResult List(string search = "")
        {

            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new GoDownServiceLayer(_dashboardData);
            var lst = _db.List(search);
            return View(lst);
        }
        public ActionResult Create(int Id = 0)
        {

            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new GoDownServiceLayer(_dashboardData);
            GoDown_Info modelGoDown;
            if (Id != 0)
            {
                modelGoDown = _db.Find(Id);

            }
            else
            {
                modelGoDown = new GoDown_Info();
                modelGoDown.Code = _db.GEN_GodownCode();
            }
            ddlbind(modelGoDown);
            return View(modelGoDown);
        }
        [HttpPost]
        public ActionResult Create(GoDown_Info modelGoDown)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new GoDownServiceLayer(_dashboardData);
            if (ModelState.IsValid)
            {
                if (modelGoDown.Id != 0)
                {
                    _db.Update(modelGoDown);
                    TempData["SaveStatus"] = 2;
                }
                else
                {

                    _db.Create(modelGoDown);
                    TempData["SaveStatus"] = 1;
                }
                // var lst = _db.List();
                // return View("Index");
                return RedirectToAction("Index");
            }
            ddlbind(modelGoDown);
            return View(modelGoDown);
        }
        public ActionResult Delete(int Id = 0)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new GoDownServiceLayer(_dashboardData);
            bool result = false;
            if (Id > 0)
            {
                // result = _db.Delete(Id);
                TempData["SaveStatus"] = 3;
            }
            //  var Godwnlist = _db.List();
            // return View("List", Godwnlist);
            return RedirectToAction("Index");
        }
        public void ddlbind(GoDown_Info modelGoDown)
        {
            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new GoDownServiceLayer(_dashboardData);
            CommonServiceLayer _objCommon = new CommonServiceLayer(_dashboardData.ConnectionString);
            modelGoDown.GoDownList = _objCommon.DDLBind("GoDown_Master", "").ToList();
            if (modelGoDown.Id != 0)
            {
                modelGoDown.CountryList = _objCommon.DDLCountryState(1, 0);
                modelGoDown.StateList = _objCommon.DDLCountryState(2, modelGoDown.Country_Id);
                modelGoDown.CityList = _objCommon.ListCityDDL(modelGoDown.State_Id).ToList();


            }
            else
            {
                modelGoDown.CountryList = _objCommon.DDLCountryState(1, 0);
                modelGoDown.StateList = new List<DDLBind>();
                modelGoDown.CityList = new List<DDLBind>();
            }

        }
        public ActionResult IsNameExists(int Id, string Name)
        {

            var _dashboardData = ViewBag.UserLog as ViewModel.Users.LoginInfo;
            _db = new GoDownServiceLayer(_dashboardData);

            return Json(!_db.IsNameExists(Id, Name), JsonRequestBehavior.AllowGet);
        }
    }
}

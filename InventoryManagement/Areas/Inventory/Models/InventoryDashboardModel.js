﻿function Generate() {
    var Para1 = $('#Report option:selected').val();
    var group = "";
    var frmdt = "";
    var todt = "";
    if ($('#GroupBy').is(':visible')) {
        group = $('#GroupBy option:selected').val();

    }
    if ($('#date').is(':visible')) {
        frmdt = $('#frmdt').val();
        todt = $('#todt').val();
    }

    // alert(Para1); alert(group); alert(frmdt); alert(todt);
    var url = '@Url.Action("Content", "InventoryReport")?para=' + Para1 + '&group=' + group + '&FromDate=' + frmdt + '&ToDate=' + todt;
    //  alert(url);
    $("#frm").attr("src", url);

}
$(document).ready(function () {
    $('#frmdt').datepicker({ dateFormat: 'dd/mm/yy' });
    $('#todt').datepicker({ dateFormat: 'dd/mm/yy' });
    Toggle();

    $('#Report').change(function () {
        Toggle();
    });
});

function Toggle() {
    if ($('#Report').val() == "Supplier" || $('#Report').val() == "Unit") {
        $('#GroupBy').hide();
        $('#lbl').hide();
    }
    else {
        $('#GroupBy').show();
        $('#lbl').show();
    }

    if ($('#Report').val() == "Return" || $('#Report').val() == "Purchase") {
        $('#date').show();
    }
    else {

        $('#date').hide();
    }
}

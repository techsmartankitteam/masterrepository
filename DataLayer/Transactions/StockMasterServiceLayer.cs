﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Transactions;
using ViewModel.Transactions;
namespace ServiceLayer.Transactions
{
    public class StockMasterServiceLayer
    {
        StockMasterDbLayer _db;
         
        public StockMasterServiceLayer()
        {
            _db = new StockMasterDbLayer();
        }
            [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
        public StockMasterServiceLayer(string ConnectionString)
        {
            _db = new StockMasterDbLayer(ConnectionString);
        }
            public StockMasterServiceLayer(ViewModel.Users.LoginInfo Log)
            {
               _db=new StockMasterDbLayer(Log);
            }
        public List<ViewModel.Transactions.StockMaster> List(String Terms="")
        {
            return _db.List(Terms);
        }
        public List<ViewModel.Ledger.StockMasterDDL> StockMasterDDL(int Id = 0)
        {
            return _db.StockMasterDDL(Id);
        }
        public ViewModel.Transactions.StockMaster Find(int ItemId)
        {
            return _db.Find(ItemId);
        }

        public List<StockMasterDetail> FindStockMaster(long ItemId)
        {
            return _db.FindStockMaster(ItemId);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViewModel.Common;
using ViewModel.Transactions;
using DataLayer.Transactions;
namespace ServiceLayer.Transactions
{
    public class PurchaseReturnServiceLayer
    {
        PurchaseReturnDbLayer _db;

        public PurchaseReturnServiceLayer()
        {
            _db = new PurchaseReturnDbLayer();
        
        }
                [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
        public PurchaseReturnServiceLayer(string ConnectionString)
        {
            _db = new PurchaseReturnDbLayer(ConnectionString);
        }
                public PurchaseReturnServiceLayer(ViewModel.Users.LoginInfo Log)
                {
                    _db = new PurchaseReturnDbLayer(Log);
                }
        public List<PurchaseReturnList> List(string search = "")
        {
            return _db.List(search);
        }
        public bool Create(PurchaseReturnInfo ModelPurRet)
        {
            return _db.Create(ModelPurRet);
        }
        public bool Update(PurchaseReturnInfo ModelPurRet)
        {
            return _db.Update(ModelPurRet);
        }
        public List<PurchaseReturnInfo_Tra> ReturnDetails(long Id)
        {
            return _db.ReturnDetails(Id);
        }
        public PurchaseReturnInfo Find(long Id)
        {
            return _db.Find(Id);
        }
        public List<DDLBind> GetBillList(long Supplier_Id)
        {
            return _db.GetBillList(Supplier_Id);
        }
        public String GEN_VC_No()
        {
            return _db.GEN_VC_No();
        }
        public bool CheckSerialNo(string SerialNo, int Purchase_Id)
        {
            return _db.CheckSerialNo(SerialNo, Purchase_Id);
        }





        public bool Delete(long Id)
        {
            return _db.Delete(Id);
        }

        /// <summary>
        /// Added By anurag dated-27-06-2016
        /// </summary>
        /// <param name="Supplier_Id"></param>
        /// <returns></returns>
        public List<DDLBind> GetStockItemForSupplier(long Purchase_Id)
        {
            return _db.GetStockItemForSupplier(Purchase_Id);
        }

        public bool CheckIsReturnedSerialNo(string SerialNo, int Item_Id, int Purchase_Id)
        {
            return _db.CheckIsReturnedSerialNo(SerialNo, Item_Id, Purchase_Id);
        }
        public string CheckValidQty(int Supplier_Id, int Purchase_Id, int Item_Id)
        {
            return _db.CheckValidQty(Supplier_Id, Purchase_Id, Item_Id);
        }

        /// <summary>
        /// Added by anurag dated 29-06-2016
        /// </summary>
        /// <param name="Supplier_Id"></param>
        /// <param name="Purchase_Id"></param>
        /// <param name="Item_Id"></param>
        /// <returns></returns>
        public string GetItemRate(int Item_Id, int Purchase_Id)
        {
            return _db.GetItemRate(Item_Id, Purchase_Id);
        }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Transactions;
using ViewModel.Transactions;
namespace ServiceLayer.Transactions
{
    public class PurchaseOrderServiceLayer
    {
       PurchaseOrderDbLayer _db;
       private string ConnectionString;
       ServiceLayer.Transactions.PurchaseQuationServiceLayer _QuotationDb;
       
       public PurchaseOrderServiceLayer()
       {
           _db = new PurchaseOrderDbLayer();
           _QuotationDb=new PurchaseQuationServiceLayer();
       }
       [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
       public PurchaseOrderServiceLayer(string ConnectionString)
       {
        _db = new PurchaseOrderDbLayer(ConnectionString);
         this.ConnectionString = ConnectionString;
         _QuotationDb=new PurchaseQuationServiceLayer();
       }
       public PurchaseOrderServiceLayer(ViewModel.Users.LoginInfo Log)
       {
       _db=new PurchaseOrderDbLayer(Log);
       
       this.ConnectionString=Log.ConnectionString;
       _QuotationDb=new PurchaseQuationServiceLayer(Log);
       }
       public  List<ViewModel.Transactions.PurchaseOrdeList> List(String Terms="")
       {
           return _db.List(Terms);

       }
       public PurchaseOrderInfo FindByQuationId(long QuationId)
       {
   
           return _db.FindByQuationId(QuationId);
       }
       public List<ViewModel.Transactions.PurchaseOrderInfo_Tra> OrderDetailsByQuationId(int QuationId)
       {
           return _db.OrderDetailsByQuationId(QuationId);
       }
       public bool Create(PurchaseOrderInfo modelPurchaseOrder)
       {
           return _db.Create(modelPurchaseOrder);
       }
       public bool CreateByQuation(PurchaseOrderInfo modelPurchaseOrder)
       {
           bool result = false;

           result = _db.Create(modelPurchaseOrder);
           if (result == true)
           {
               result = _QuotationDb.ApproveQuation(modelPurchaseOrder.Quation_Id);
           }
           return result;
       }
       public bool Update(PurchaseOrderInfo modelPurchaseOrder)
       {
           return _db.Update(modelPurchaseOrder);
       }
       public List<ViewModel.Transactions.PurchaseOrderInfo_Tra> OrderDetails(long Id)
       {
           return _db.OrderDetails(Id);
       }
       public String GEN_OrderNo()
       {
           return _db.GEN_OrderNo();
       }
       public PurchaseOrderInfo Find(long Id)
       {
           return _db.Find(Id);
       }
       public bool Delete(long Id)
       {
           return _db.Delete(Id);
       }
    }
}

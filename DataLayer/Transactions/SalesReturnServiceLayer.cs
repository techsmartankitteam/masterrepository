﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Transactions;
using ViewModel.Transactions;
using ViewModel.Common;
namespace ServiceLayer.Transactions
{
   public class SalesReturnServiceLayer
    {
       SalesReturnDbLayer _db;
        
       public SalesReturnServiceLayer()
       {
           _db = new SalesReturnDbLayer();
       }

          [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
       public SalesReturnServiceLayer(string ConnectionString)
           
       {
           _db = new SalesReturnDbLayer(ConnectionString);
       }
       public SalesReturnServiceLayer(ViewModel.Users.LoginInfo Log)
       {
       _db=new SalesReturnDbLayer(Log);
       }
       public List<SalesReturnList> List(string search="")
       {
           return _db.List(search);
       }
       public bool Create(SalesReturnInfo ModelSales)
       {
           return _db.Create(ModelSales);
          
       }
       public bool Update(SalesReturnInfo ModelSales)
       {
           return _db.Update(ModelSales);

       }
    public string      GEN_VC_No()
       {
           return _db.GEN_VC_No();
       }
    public List<SalesReturnInfo_Tra> ReturnDetails(long Id)
    {
        return _db.ReturnDetais(Id);
    }
    public bool CheckSerialNo(string SerialNo, int Sales_Id, int Item_Id)
    {
        return _db.CheckSerialNo(SerialNo, Sales_Id, Item_Id);
    }
       public SalesReturnInfo Find(long Id)
       {
           return _db.Find(Id);
       }
       public bool Delete(long Id)
       {
           return _db.Delete(Id);
       }

       public List<DDLBind> GetBillList(long Customer_Id)
       {
           return _db.GetBillList(Customer_Id);
       }

       /// <summary>
       /// Added By anurag dated-27-06-2016
       /// </summary>
       /// <param name="Supplier_Id"></param>
       /// <returns></returns>
       public List<DDLBind> GetStockItemForSupplier(long Sales_Id)
       {
           return _db.GetStockItemForSupplier(Sales_Id);
       }

       ////Added by anurag
       public bool CheckIsReturnedSerialNo(string SerialNo, int Item_Id, int Sales_Id)
       {
           return _db.CheckIsReturnedSerialNo(SerialNo, Item_Id, Sales_Id);
       }
       public string CheckValidQty(int Customer_Id, int Sales_Id, int Item_Id)
       {
           return _db.CheckValidQty(Customer_Id, Sales_Id, Item_Id);
       }

       public string GetItemRate(int Item_Id, int Sales_Id)
       {
           return _db.GetItemRate(Item_Id, Sales_Id);
       }
    }
}

﻿using DataLayer.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViewModel.Transactions;

namespace ServiceLayer.Transactions
{
   public class ReceiptNoteServiceLayer
    {
       ReceiptNoteDbLayer _db;

       public ReceiptNoteServiceLayer()
       {
           _db = new ReceiptNoteDbLayer();
       }
       [Obsolete("Please Use UserInfo Constructor intead of String Parameter",true)]
       public ReceiptNoteServiceLayer(string ConnectionString)
       {
        _db = new ReceiptNoteDbLayer(ConnectionString);
       }
       public ReceiptNoteServiceLayer(ViewModel.Users.LoginInfo Log)
       {
           _db = new ReceiptNoteDbLayer(Log);
       }
       public List<ReceiptNoteList> List(String Term="")
       {
           return _db.List(Term);
       }
       public bool Create(ReceiptNoteInfo modelReceiptNote)
       {
           return _db.Create(modelReceiptNote);
       }
       public bool Update(ReceiptNoteInfo modelReceiptNote)
       {
           return _db.Update(modelReceiptNote);
       }
       public List<ReceiptNoteTraInfo> ReceiptNoteDetails(int Id, int OrderId)
       {
           return _db.ReceiptNoteDetails(Id, OrderId);
       }
       public List<ReceiptNoteTraInfo> ViewReceiptNoteDetails(long Id)
       {
           return _db.ViewReceiptNoteDetails(Id);
       }
       public List<ViewModel.Common.DDLBind> DDLBind(long SuuplierId = 0,long Id=0)
       {
           return _db.DDLBind(SuuplierId, Id).ToList();
       }
       public ReceiptNoteInfo Find(long Id)
       {
           return _db.Find(Id);
       }
       public String GEN_ReceiptNo()
       {
           return _db.GEN_ReceiptNo();
       }
       public bool Delete(int Id)
       {
           return _db.Delete(Id);
       }
      
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Transactions;
using ViewModel.Transactions;
namespace ServiceLayer.Transactions
{
   public class PurchaseQuationServiceLayer
    {
        PurchaseQuationDbLayer _db;

        public PurchaseQuationServiceLayer()
       {
           _db = new PurchaseQuationDbLayer();
       }
       [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
        public PurchaseQuationServiceLayer(string ConnectionString){

            _db = new PurchaseQuationDbLayer(ConnectionString);
       }
        public PurchaseQuationServiceLayer(ViewModel.Users.LoginInfo Log)
        {
    _db=new PurchaseQuationDbLayer(Log);
        }
       public  List<ViewModel.Transactions.PurchaseQuationList> List(String Terms="")
       {
           return _db.List(Terms);
       }
       public bool Create(PurchaseQuationInfo modelQuation)
       {
           return _db.Create(modelQuation);
       }
       public PurchaseQuationInfo Find(long Id)
       {
           return _db.Find(Id);
       }
       public bool Update(PurchaseQuationInfo modelQuation)
       {
           return _db.Update(modelQuation);
       }
       public List<ViewModel.Transactions.PurchaseQuationInfo_Tra> QuationDetails(long Quation_Id)
       {
           return _db.QuationDetails(Quation_Id);

       }
       public String GEN_ChallanNo()
       {
           return _db.GEN_ChallanNo();
       }
       public bool ApproveQuation(long QuationId)
       {
          return _db.ApproveQuation(QuationId);
       }
       public bool Delete(long Id)
       {
           return _db.Delete(Id);
       }
    }
}

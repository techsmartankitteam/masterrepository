﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Transactions;
using ViewModel.Transactions;
namespace ServiceLayer.Transactions
{
    public class SalesOrderServiceLayer : QuationServiceLayer
    {
       SalesOrderDbLayer _db;
        
       public SalesOrderServiceLayer():base()
       {
           _db = new SalesOrderDbLayer();
       }
         [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
       public SalesOrderServiceLayer(string ConnectionString) :base(ConnectionString)
       {
        _db = new SalesOrderDbLayer(ConnectionString);
       }
       public SalesOrderServiceLayer(ViewModel.Users.LoginInfo Log):base(Log)
       { 
       _db=new SalesOrderDbLayer(Log);
       }
       public List<SalesOrdeList> List(string Terms="")
       {
           return _db.List(Terms);
       }
       public bool Create(SalesOrderInfo modelSalesOrder)
       {
           return _db.Create(modelSalesOrder);
       }
       public bool Update(SalesOrderInfo modelSalesOrder)
       {
           return _db.Update(modelSalesOrder);
       }
       public SalesOrderInfo Find(long Id)
       {
           return _db.Find(Id);
       }
       public SalesOrderInfo FindByQuationId(long QuationId)
       {
          
          
                return _db.FindByQuationId(QuationId);
       }
       public List<ViewModel.Transactions.SalesOrderInfo_Tra> OrderDetailsByQuationId(int QuationId)
       {
           return _db.OrderDetailsByQuationId(QuationId);
       }
       public List<ViewModel.Transactions.SalesOrderInfo_Tra> OrderDetails(long OrderNo)
       {
           return _db.OrderDetails(OrderNo);
       }
       public bool Delete(int Id)
       {
           return _db.Delete(Id);
       }
       public String GEN_OrderNo()
       {
           return _db.GEN_OrderNo();
       }
       public bool CreateByQuation(SalesOrderInfo modelSalesOrder)
       {
           bool result = false;
           result = _db.Create(modelSalesOrder); 
           if(result==true)
           {
               result =base.ApproveQuation(modelSalesOrder.Refered_Id);
           }
           return result;
       }
       
       
      
       //public SalesOrderInfo FindByQuationId(int Quation_Id)
       //{
       //    return _db.FindByQuationId(Quation_Id);
       //}
    }
}

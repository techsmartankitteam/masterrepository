﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using HDMEntity;
using DataLayer;
using DataLayer.Transactions;
using ViewModel.Ledger;
namespace ServiceLayer.Transactions
{
    public class SalesServiceLayer
    {
        SalesDbLayer _db;
 
   Masters.UnitsServiceLayer _unitDb;
        public SalesServiceLayer()
        {
            _db = new SalesDbLayer();
            _unitDb=new Masters.UnitsServiceLayer();
        }
           [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
        public SalesServiceLayer(string ConnectionString)
        {
            _db = new SalesDbLayer(ConnectionString);
            _unitDb=new Masters.UnitsServiceLayer();
        }
        public SalesServiceLayer(ViewModel.Users.LoginInfo Log)
        {
            _db = new SalesDbLayer(Log);
            _unitDb=new Masters.UnitsServiceLayer(Log);
        }
        public List<PurchaseList> List(String Terms="")
        {
            return _db.List(Terms);
        }
        public ViewModel.Ledger.SalesMaster Find(long Id = 0)
        {
            return _db.Find(Id);
        }
        public List<ViewModel.Ledger.Purchase_Tra> ItemDetails(long SalesId)
        {
            return _db.ItemDetails(SalesId);
        }
        public List<ViewModel.Ledger.Sales_Tra> ViewItemDetails(long SalesId)
        {
            return _db.ViewItemDetails(SalesId);
        }
        public bool Create(ViewModel.Ledger.SalesMaster ModelSalMaster)
        {
            ModelSalMaster.Grand_Total = GetGrandTotal(ModelSalMaster.sales_tra.listpurchase_tra);
            return _db.Create(ModelSalMaster);
        }
        public bool Update(ViewModel.Ledger.SalesMaster ModelSalMaster)
        {
            ModelSalMaster.Grand_Total = GetGrandTotal(ModelSalMaster.sales_tra.listpurchase_tra);
            return _db.Update(ModelSalMaster);
        }
        public String GEN_ChallanNo()
        {
            return _db.GEN_ChallanNo();
        }
        public decimal GetGrandTotal(List<ViewModel.Ledger.Purchase_Tra> salTra)
        {
            decimal GrTotal = 00.0m;
            try
            {
                foreach (var item in salTra.Where(x => x.Status == true))
                {
                    GrTotal = GrTotal + (item.Qty * item.Rate);
                }
            }
            catch
            {

                GrTotal = 00.0m;
            }
            return GrTotal;
        }
        public List<Challan_Details> ViewDetails(int Id)
        {
            return _db.ViewDetails(Id);
        }
        public String GetUnitName(int Id)
        {
            
            string UnitName = "";
            if (Id != 0)
                UnitName = _unitDb.Find(Id).Name;
            return UnitName;
        }


        public bool Gen_ChallanOfSales(int Id)
        {
            return _db.Gen_ChallanOfSales(Id);
        }
        

        public SalesOrderItem GetSalesOrdertemList(long Delivery_Note_Id = 0)
        {
            return _db.GetSalesOrdertemList(Delivery_Note_Id);
        }
        public List<ViewModel.Common.TaxDetails> GetTaxList(long Sales_Id)
        {
            return _db.GetTaxList(Sales_Id);
        }
        public bool Delete(long ID)
        {
            return _db.Delete(ID);
        }
        public List<string> GetSerial_No(int Id, int Qty, string SrNo)
        {
            return _db.GetSerial_No(Id, Qty, SrNo);
        }
    }
}

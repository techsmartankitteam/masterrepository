﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Masters;
namespace ServiceLayer.Masters
{
   public class RegionServiceLayer
    {
       RegionDbLayer _db;
         
       public RegionServiceLayer()
       {
           _db = new RegionDbLayer();
       }
         [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
       public RegionServiceLayer(string ConnectionString)
       {
  _db = new RegionDbLayer(ConnectionString  );
       }
         public RegionServiceLayer(ViewModel.Users.LoginInfo Log)
         {
         _db=new RegionDbLayer(Log);
         }
       public List<ViewModel.Common.RegionInfo> List(String Terms="")
       {
           return _db.List(Terms);
       }
       public bool Create(ViewModel.Common.RegionInfo modelRegionInfo)
       {
           return _db.Create(modelRegionInfo);
       }
       public bool Update(ViewModel.Common.RegionInfo modelRegionInfo)
       {
           return _db.Update(modelRegionInfo);
       }
       public ViewModel.Common.RegionInfo Find(int Id)
       {
           return _db.Find(Id);
       }
       public bool Delete(int Id)
       {
           return _db.Delete(Id);
       }
    }
}

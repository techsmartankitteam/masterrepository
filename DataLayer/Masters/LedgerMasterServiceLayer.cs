﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Masters;
using ViewModel.Common;
using ServiceLayer.Common;
namespace ServiceLayer.Masters
{
  public  class LedgerMasterServiceLayer:CommonServiceLayer
    {

      LedgerMasterDbLayer _db;
       
      public LedgerMasterServiceLayer():base()
      {
          _db = new LedgerMasterDbLayer();
      }
         [Obsolete("Please use LoginInfo parameter intead of String in this Contructor", true)]
      public LedgerMasterServiceLayer(string ConnectionString)
          : base(ConnectionString)
      {
          _db = new LedgerMasterDbLayer(ConnectionString);
      }
         public LedgerMasterServiceLayer(ViewModel.Users.LoginInfo _log):base(_log)
       {
           _db = new LedgerMasterDbLayer(_log);
       }
      public List<ViewModel.Common.List_Common> List(String Term="")
      {
          return _db.List(Term);
          
      }
      public bool Create(LedgerMasterInfo modelledmaster)
      {
          

          return _db.Create(modelledmaster);
      }
      public bool Update(LedgerMasterInfo modelledmaster)
      {
          return _db.Update(modelledmaster);
      }
      public LedgerMasterInfo Find(int Id)
      {
          return _db.Find(Id);
      }
      public bool Delete(int Id)
      {
          return _db.Delete(Id);
      }
      public bool IsNameExists(int Id, string Name)
      {
          return _db.IsNameExists(Id, Name);
      }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Finance;
using ViewModel.Finance;
using ViewModel.Common;
using ViewModel.Users;

namespace ServiceLayer.Finance
{

    public class JournalVoucherSL
    {
        JournalVoucherDbLayer _db;
        
        public JournalVoucherSL()
        {
            _db = new JournalVoucherDbLayer();
        }
        public JournalVoucherSL(LoginInfo _log)
        {
            _db = new JournalVoucherDbLayer(_log);
        }
        public string Journal_VC_No()
        {
            return _db.GEN_JournalVC_No();
        }
        public JournalVoucherInfo Find(long Id)
        {
            return _db.Find(Id);
        }

        public bool Create(JournalVoucherInfo ModelSales)
        {
            return _db.Create(ModelSales);
        }
        public bool Update(JournalVoucherInfo ModelSales)
        {
            return _db.Update(ModelSales);
        }
        public List<CrDrList> List(string search = "")
        {
            return _db.List(search);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViewModel.CommonReport;
using ViewModel.Users;
using DataLayer.Report;
namespace ServiceLayer.CommonReport
{
  public  class ReportServiceLayer
    {
      ReportDbLayer _db;
      public ReportServiceLayer()
      {
          _db = new ReportDbLayer();
      }
      public ReportServiceLayer(LoginInfo _Log)
      {
          _db = new ReportDbLayer(_Log);      
      }

      public List<DayBookInfo> DayBookList(DateTime From,DateTime To,string SearchText)
      {
          return _db.DayBookList(From,To,SearchText);
      }
      public List<InventoryBookInfo> InventoryBookList(DateTime From, DateTime To, string SearchText)
      {
          return _db.InventoryBookList(From,To,SearchText);
      }
      public List<OutStandingInfo> PayabalesBill(DateTime From, DateTime To, string SearchText)
      {
          return _db.PayabalesBill(From, To, SearchText);
      }
      public List<OutStandingInfo> ReceivablesBill(DateTime From, DateTime To, string SearchText)
      {
          return _db.ReceivablesBill(From, To, SearchText);
      }
      public List<OrdersInfo> OrdersList(string Tag, DateTime From, DateTime To, string SearchText)
      {
          return _db.OrdersList(Tag, From, To, SearchText);
      }
      public List<AccountsBookInfo> AccountsBookList(string Tag, DateTime From, DateTime To, string SearchText)
      {
          return _db.AccountsBookList(Tag, From, To, SearchText);
      }
    }
}
